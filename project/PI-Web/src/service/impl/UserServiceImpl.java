package service.impl;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


import orm.classes.AuthorityDAO;
import orm.classes.UserDAO;
import classes.Authority;
import classes.UserCriteria;
import service.UserService;
import utils.JavaUtils;
import utils.MailUtils;


/**
 * @author andre
 *
 */
public class UserServiceImpl implements UserService {

	protected final Log logger = LogFactory.getLog(this.getClass());

	private PasswordEncoder passwordEncoder;

	private SaltSource saltSource;

	public UserServiceImpl() {
		this.logger.debug(("UserServiceImpl created -hash : " + this.hashCode()));
	}

	@Override
	@Transactional(readOnly = false,  rollbackFor = Exception.class)
	public boolean activateAccount(String username, String key) {
		classes.User user = getUser(username);
		if (user != null && user.getActivationKey() != null && user.getActivationKey().equals(key)){
			if (!user.getActive()){
				user.setActive(true); 
				try{
					UserDAO.save(user);
					UserDAO.evict(user);
				}catch(Exception e){
					String msg = "Could not update active flag for user '"+username +"' :" + e.toString();
					this.logger.error(msg, e);
					return false;
				}
			}
			return true;
		}
		return false;
	}


	@Transactional(readOnly = true)
	public classes.User getUser(String username) {
		this.logger.debug(("entering method getUser"));
		classes.User user = null;
		try {
			UserCriteria userCriteria = new UserCriteria();
			userCriteria.username.eq(username);
			userCriteria.setCacheable(false);
			user = userCriteria.uniqueUser();
		} catch (Exception e) {
			String msg = "Could not find the user " + e.toString();
			this.logger.error(msg, e);
		}
		return user;
	}

	@Transactional(readOnly = true)
	public boolean checkUser(String username){
		return getUser(username) != null;
	}

	@Transactional(readOnly = true)
	public boolean checkPassword(String username, String password){
		classes.User user = getUser(username);

		UserDetails userDetails = new User(username, password, true, true, true, true, new ArrayList<GrantedAuthority>());
		Object salt = saltSource.getSalt(userDetails); 

		this.logger.info((password+"->"+passwordEncoder.encodePassword(password, salt)+
				"\nretrieved->"+user.getPassword()+"\nusername->"+username+"\nUser.id->"+user.getId()));
		if (passwordEncoder.encodePassword(password, salt).equals(user.getPassword())){
			return true;			
		}

		return false;
	}

	@Transactional(readOnly = false,  rollbackFor = Exception.class)
	public classes.User addUser(classes.User user){
		this.logger.debug(("entering method addUser"));

		UserDetails userDetails = new User(user.getUsername(), user.getPassword(), true, true, true, true, new ArrayList<GrantedAuthority>());
		Object salt = saltSource.getSalt(userDetails); 

		classes.User newUser = UserDAO.createUser();

		newUser.setUsername(user.getUsername());
		newUser.setPassword(passwordEncoder.encodePassword(user.getPassword(), salt)); 
		newUser.setEmail(user.getEmail());
		newUser.setName(user.getName());
		newUser.setActive(true);
		newUser.setActivationKey(JavaUtils.generateRandomKey(26));

		Authority auth = AuthorityDAO.createAuthority();
		auth.setAuthority("ROLE_USER");
		newUser.authority.add(auth);

		try {
			UserDAO.save(newUser);

		} catch (Exception e) {
			String msg = "Could not save the user " + e.toString();
			this.logger.error(msg, e);
			return null;
		}

		// Send mail with activation key
		MailUtils.sendActivationEmail(newUser.getActivationKey(),newUser.getEmail(), newUser.getUsername());

		return user;
	}

	@Transactional(readOnly = false,  rollbackFor = Exception.class)
	public classes.User updateUser(classes.User newUser){
		this.logger.debug(("entering method updateUser"));

		classes.User persistedUser = null;
		try {
			persistedUser = getUser(newUser.getUsername());

			if (!persistedUser.getEmail().equalsIgnoreCase(newUser.getEmail())){

				persistedUser.setActivationKey(JavaUtils.generateRandomKey(26));
				persistedUser.setActive(false);

				// Send mail with activation key
				MailUtils.sendActivationEmail(persistedUser.getActivationKey(),newUser.getEmail(), persistedUser.getUsername());			
			}

			if (newUser.getPassword().length()>0){

				UserDetails userDetails = new User(newUser.getUsername(), newUser.getPassword(), true, true, true, true, new ArrayList<GrantedAuthority>());
				Object salt = saltSource.getSalt(userDetails); 

				persistedUser.setPassword(passwordEncoder.encodePassword(newUser.getPassword(), salt));
			}

			persistedUser.setEmail(newUser.getEmail());
			persistedUser.setName(newUser.getName());
			persistedUser.setNif(newUser.getNif());

			UserDAO.save(persistedUser);
			UserDAO.evict(newUser);

		} catch (Exception e) {
			String msg = "Could not update the user " + e.toString();
			this.logger.error(msg, e);
			return null;
		}
		return persistedUser;
	}

	@Override
	public boolean passwordRecovery(String email) {
		this.logger.debug(("entering method getUser"));
		try {
			UserCriteria userCriteria = new UserCriteria();
			userCriteria.email.eq(email);
			classes.User[] users = userCriteria.listUser();

			if (users != null && users.length >0){
				for (classes.User user : users){

					String newPassword = JavaUtils.generateRandomKey(8);
					UserDetails userDetails = new User(user.getUsername(), newPassword, true, true, true, true, new ArrayList<GrantedAuthority>());
					Object salt = saltSource.getSalt(userDetails); 
					user.setPassword(passwordEncoder.encodePassword(newPassword, salt));
					UserDAO.save(user);
					UserDAO.evict(user);

					// Send mail with newPassword
					MailUtils.sendRecoveryEmail(newPassword,email, user.getUsername());
				}
			}else{
				return false;
			}
		} catch (Exception e) {
			String msg = "Could not find the user " + e.toString();
			this.logger.error(msg, e);
			return false;
		}
		return true;
	}


	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}

	public void setSaltSource(SaltSource saltSource) {
		this.saltSource = saltSource;
	}

	@Transactional(readOnly = true)
	public classes.User[] getUsersList(){
		try{
			UserCriteria userCriteria = new UserCriteria();
			return userCriteria.listUser();
		}catch(Exception e){
			logger.error("Error while fetching components list: "+e);
			return null;
		}
	}

	@Transactional(propagation =Propagation.REQUIRES_NEW, readOnly = false,  rollbackFor = Exception.class)
	public boolean desactivateUser(classes.User u){
		this.logger.debug(("entering method updateUser"));
		try {
			UserDAO.refresh(u);
			u.setActive(false);
			UserDAO.save(u);
		}catch (Exception e) {
			String msg = "Could not update the user " + e.toString();
			this.logger.error(msg, e);
			return false;
		}
		return true;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = Exception.class)
	public boolean activateUser(classes.User u){
		this.logger.debug(("entering method updateUser"));
		try {
    		UserDAO.refresh(u);
			u.setActive(true);
			UserDAO.save(u);
		}catch (Exception e) {
			String msg = "Could not update the user " + e.toString();
			this.logger.error(msg, e);
			return false;
		}
		return true;
	}


	@Override
	public void sendSupportMessage(String email, String text) {
		MailUtils.sendSupportRequestEmail(text, email);
	}

}
