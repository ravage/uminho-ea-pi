package service.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import orm.classes.ApplicationDAO;
import orm.classes.ServiceDAO;

import service.ServiceService;
import utils.FacesUtils;
import utils.MailUtils;
import utils.WebRequestsUtils;

import classes.Application;
import classes.Bundle;
import classes.BundleCriteria;
import classes.ComponentCriteria;
import classes.Service;
import classes.ServiceCriteria;
import classes.Statistic;
import classes.StatisticCriteria;
import classes.Worker;

public class ServiceServiceImpl implements ServiceService{

	protected final Log logger = LogFactory.getLog(this.getClass());

	@Transactional(readOnly=true, rollbackFor = Exception.class)
	public Collection<Bundle> getBundles(){
		this.logger.debug(("entering method getBundle"));
		Collection<Bundle> bundleList = null;
		try {
			BundleCriteria bundleCriteria = new BundleCriteria();
			bundleList = new LinkedList<Bundle>(Arrays.asList(bundleCriteria.listBundle()));

		} catch (Exception e) {
			String msg = "Could not find the bundles " + e.toString();
			this.logger.error(msg, e);
		}		
		return bundleList;		
	}

	@Transactional(readOnly=true, rollbackFor = Exception.class)
	public Service getServiceForUser(String username){
		this.logger.debug(("entering method getServiceForUser"));

		try {

			ServiceCriteria serviceCriteria = new ServiceCriteria();
			serviceCriteria.createUserCriteria().username.eq(username);
			serviceCriteria.setMaxResults(1);
			return serviceCriteria.uniqueService();

		} catch (Exception e) {
			this.logger.error("Failed to get user Service: "+e);
			return null;
		}		
	}

	@Transactional(readOnly=true, rollbackFor = Exception.class)
	public Service getServiceForId(long id){
		this.logger.debug(("entering method getServiceForId"));

		try {

			ServiceCriteria serviceCriteria = new ServiceCriteria();
			serviceCriteria.id.eq(id);
			serviceCriteria.setMaxResults(1);
			return serviceCriteria.uniqueService();

		} catch (Exception e) {
			this.logger.error("Failed to get Service: "+e);
			return null;
		}		
	}

	@Transactional(readOnly=false, rollbackFor = Exception.class)
	public boolean createService(Service service) {

		Properties p = FacesUtils.getProperties();
		try{			
			ServiceDAO.save(service);
		}catch(Exception e){
			logger.error("could not create service: "+e);
			return false;
		}
		WebRequestsUtils.serviceReady(service.getId());
		MailUtils.sendServiceCreatedEmail(service.getName(), service.getUser().getEmail());
		return true;
	}

	@Transactional(readOnly= false, rollbackFor = Exception.class)
	public boolean addApplication(Application app){

		try{
			ApplicationDAO.save(app);	
		}catch(Exception e){
			logger.error("Failed to create application: "+e);
			return false;
		}

		return WebRequestsUtils.addApplication(app.getService().getId() , app.getId());
	}

	@Transactional(readOnly = true)
	public Service[] getServiceList(){
		try{
			ServiceCriteria serviceCriteria = new ServiceCriteria();
			return serviceCriteria.listService();
		}catch(Exception e){
			logger.error("Error while fetching components list: "+e);
			return null;
		}
	}

	@Transactional(readOnly = true)
	public Statistic getDBLastStatistic(long x){
		try{
			StatisticCriteria criteria = new StatisticCriteria();

			Statistic statistic = (Statistic) criteria.add(Restrictions.like("name", "space"))
			.addOrder(Order.desc("createdAt")).createCriteria("worker")
			.createAlias("component", "c").createAlias("service", "s")
			.add(Restrictions.like("c.name", "Database"))
			.add(Restrictions.eq("s.id", x)).setMaxResults(1).uniqueResult();

			return statistic;
		}catch(Exception e){
			logger.error("Error while fetching components list: "+e);
			return null;
		}
	}

	@Transactional(readOnly = true)
	public Statistic getStorageLastStatistic(long x){
		try{
			StatisticCriteria criteria = new StatisticCriteria();

			Statistic statistic = (Statistic) criteria.addOrder(Order.desc("createdAt")).createCriteria("worker")
			.createAlias("component", "c").createAlias("service", "s")
			.add(Restrictions.like("c.name", "Storage"))
			.add(Restrictions.eq("s.id", x)).setMaxResults(1).uniqueResult();

			return statistic;
		}catch(Exception e){
			logger.error("Error while fetching components list: "+e);
			return null;
		}
	}

	@Transactional(readOnly = true)
	public List<Statistic> getACPUIStatistic(long x){
		try{
			StatisticCriteria criteria = new StatisticCriteria();

			List<Statistic> statistics  = (List<Statistic>) criteria.add(Restrictions.like("name", "cpui")).addOrder(Order.desc("createdAt")).createCriteria("worker")
			.createAlias("component", "c").createAlias("service", "s")
			.add(Restrictions.like("c.name", "Application"))
			.add(Restrictions.eq("s.id", x)).setMaxResults(10).list();


			return statistics;
		}catch(Exception e){
			logger.error("Error while fetching components list: "+e);
			return null;
		}
	}

	@Transactional(readOnly = true)
	public List<Statistic> getACPUAStatistic(long x){
		try{
			StatisticCriteria criteria = new StatisticCriteria();

			List<Statistic> statistics  = (List<Statistic>) criteria.add(Restrictions.like("name", "cpua")).addOrder(Order.desc("createdAt")).createCriteria("worker")
			.createAlias("component", "c").createAlias("service", "s")
			.add(Restrictions.like("c.name", "Application"))
			.add(Restrictions.eq("s.id", x)).setMaxResults(10).list();


			return statistics;
		}catch(Exception e){
			logger.error("Error while fetching components list: "+e);
			return null;
		}
	}

	@Transactional(readOnly = true)
	public List<Statistic> getDCPUIStatistic(long x){
		try{
			StatisticCriteria criteria = new StatisticCriteria();

			List<Statistic> statistics  = (List<Statistic>) criteria.add(Restrictions.like("name", "cpui")).addOrder(Order.desc("createdAt")).createCriteria("worker")
			.createAlias("component", "c").createAlias("service", "s")
			.add(Restrictions.like("c.name", "Database"))
			.add(Restrictions.eq("s.id", x)).setMaxResults(10).list();


			return statistics;
		}catch(Exception e){
			logger.error("Error while fetching components list: "+e);
			return null;
		}
	}

	@Transactional(readOnly = true)
	public List<Statistic> getDCPUAStatistic(long x){
		try{
			StatisticCriteria criteria = new StatisticCriteria();

			List<Statistic> statistics  = (List<Statistic>) criteria.add(Restrictions.like("name", "cpua")).addOrder(Order.desc("createdAt")).createCriteria("worker")
			.createAlias("component", "c").createAlias("service", "s")
			.add(Restrictions.like("c.name", "Database"))
			.add(Restrictions.eq("s.id", x)).setMaxResults(10).list();


			return statistics;
		}catch(Exception e){
			logger.error("Error while fetching components list: "+e);
			return null;
		}
	}

	@Transactional(readOnly = true)
	public List<Statistic> getPHPMStatistic(long x){
		try{
			StatisticCriteria criteria = new StatisticCriteria();

			List<Statistic> statistics  = (List<Statistic>) criteria.add(Restrictions.like("name", "mem")).addOrder(Order.desc("createdAt")).createCriteria("worker")
			.createAlias("component", "c").createAlias("service", "s")
			.add(Restrictions.like("c.name", "Application"))
			.add(Restrictions.eq("s.id", x)).setMaxResults(10).list();

			return statistics;
		}catch(Exception e){
			logger.error("Error while fetching components list: "+e);
			return null;
		}
	}

	@Transactional(readOnly = true)
	public List<Statistic> getDBMStatistic(long x){
		try{
			StatisticCriteria criteria = new StatisticCriteria();

			List<Statistic> statistics  = (List<Statistic>) criteria.add(Restrictions.like("name", "mem")).addOrder(Order.desc("createdAt")).createCriteria("worker")
			.createAlias("component", "c").createAlias("service", "s")
			.add(Restrictions.like("c.name", "Database"))
			.add(Restrictions.eq("s.id", x)).setMaxResults(10).list();


			return statistics;
		}catch(Exception e){
			logger.error("Error while fetching components list: "+e);
			return null;
		}
	}
}
