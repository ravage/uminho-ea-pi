package service.impl;

import java.sql.Timestamp;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import orm.classes.ComponentDAO;
import orm.classes.ConfigurationDAO;
import orm.classes.DirectiveDAO;

import classes.Component;
import classes.ComponentCriteria;
import classes.Configuration;
import classes.Directive;
import classes.DirectiveCriteria;

import service.ComponentsService;

public class ComponentsServiceImpl implements ComponentsService {
	protected final Log logger = LogFactory.getLog(this.getClass());

	public Component[] getComponents(){
		try{
			ComponentCriteria componentCriteria = new ComponentCriteria();
			return componentCriteria.listComponent();
		}catch(Exception e){
			logger.error("Error while fetching components list: "+e);
			return null;
		}
	}

	public Directive[] getDirectivesForComponent(String componentKey){

		try{
			DirectiveCriteria directiveCriteria = new DirectiveCriteria();
			directiveCriteria.createComponentCriteria().key.eq(componentKey);
			return directiveCriteria.listDirective();

		}catch(Exception e){
			logger.equals(" could not fetch directives for component '"+componentKey+"' :"+e);
			return null;
		}		
	}

	@Override
	@Transactional(readOnly=false)
	public boolean removeDirective(Directive directive) {
		try{
			DirectiveDAO.delete(directive);
		}catch(Exception e){
			try{
				Directive d = DirectiveDAO.getDirectiveByORMID(directive.getId());
				logger.info("DISSOCIATE directive id: "+d.getId()+"\t hashCode: "+ d.hashCode());
				DirectiveDAO.deleteAndDissociate(d);
			}catch(Exception ex){
				logger.error("could not remove directive: "+e);
				return false;
			}
		}
		logger.info("removed directive with id: "+directive.getId());
		return true;
	}

	@Transactional(readOnly=false, rollbackFor = Exception.class)
	public Directive addDirective(Directive newDirective) {
		try{
			DirectiveDAO.save(newDirective);
			logger.info("added directive with id: "+newDirective.getId());
			return newDirective;
		}catch(Exception e){
			logger.error("could not add directive : "+e);
			return null;
		}
	}

	@Transactional(rollbackFor = Exception.class)
	public boolean saveFile(Configuration configuration){
		try{
			ConfigurationDAO.save(configuration);
			return true;
		}catch(Exception e){
			logger.error("could not save Configuration: "+e);
			return false;
		}		
	}

}
