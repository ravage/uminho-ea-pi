package service;

import classes.User;

/**
 * @author andre
 *
 */
public interface UserService {

    User getUser(String username);

	boolean checkUser(String username);

	boolean checkPassword(String username, String password);
	
	User addUser(User user);
	
	User updateUser(User user);
	
	boolean activateAccount(String username, String key);
	
	boolean passwordRecovery(String email);
	
	void sendSupportMessage(String email, String text);
	
	User[] getUsersList();
	
	boolean desactivateUser(classes.User u);
	
	boolean activateUser(User u);

}