package service;

import java.util.Collection;
import java.util.List;

import classes.Application;
import classes.Bundle;
import classes.Service;
import classes.Statistic;

public interface ServiceService {
	
	 Collection<Bundle> getBundles();
	 
	 boolean createService(Service service);
	 
	 Service getServiceForUser(String username);
	 
	 Service getServiceForId(long id);
	 
	 boolean addApplication(Application app);
	 
	 Service[] getServiceList();
	 
	 Statistic getDBLastStatistic(long x);
	 
	 Statistic getStorageLastStatistic(long x);
	 
	 List<Statistic> getACPUIStatistic(long x);
	 
	 List<Statistic> getACPUAStatistic(long x);

	 List<Statistic> getDCPUIStatistic(long x);
	 
	 List<Statistic> getDCPUAStatistic(long x);
	 
	 List<Statistic> getPHPMStatistic(long x);
	 
	 List<Statistic> getDBMStatistic(long x);

}
