package service;

import classes.Component;
import classes.Configuration;
import classes.Directive;

public interface ComponentsService {
	
	Component[] getComponents();

	Directive[] getDirectivesForComponent(String componentKey);
	
	boolean removeDirective(Directive directive);
	
	Directive addDirective(Directive directive);
	
	boolean saveFile(Configuration configuration);

}
