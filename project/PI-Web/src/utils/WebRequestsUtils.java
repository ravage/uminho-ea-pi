package utils;

import java.util.Properties;

import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class WebRequestsUtils {
	
	private WebRequestsUtils(){}

	public static boolean serviceReady(long serviceId){

		Properties p = FacesUtils.getProperties();

		if (p == null){
			return false;
		}		

		String path = p.getProperty("agent.mediator", "http://localhost:8080")+
		p.getProperty("service.activate", "/services/");	
		
		PostMethod post = new PostMethod(path);
		post.addParameter("key",Long.toString(serviceId));

		return sendRESTService(post);
	}


	public static boolean addApplication(long serviceId, long appId){

		Properties p = FacesUtils.getProperties();

		if (p == null){
			return false;
		}

		String path = p.getProperty("agent.mediator", "http://localhost:8080")+
		p.getProperty("service.activate", "/services/")+serviceId+
		p.getProperty("application.activate", "/apps/");
		
		PostMethod post = new PostMethod(path);
		post.addParameter("sid",Long.toString(serviceId));
		post.addParameter("aid",Long.toString(appId));
		
		return sendRESTService(post);
	}


	private static boolean sendRESTService(PostMethod  post){

		Log logger = LogFactory.getLog(WebRequestsUtils.class);
		boolean hasError = false;

		HttpClient httpclient = new HttpClient();
		PostMethod httpPost = post;
		try {
			int statusCode = httpclient.executeMethod(httpPost);
			logger.info("REST request to '"+post.getPath()+"' was sucessfull with response code "+statusCode);
			hasError = false;
		}catch(Exception e){
			logger.info("Failed to contact Agent: "+e);
			hasError = true;

		}finally {
			httpPost.releaseConnection();
		}

		return !hasError;
	}

}
