package utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class RunnableWrapper implements Runnable {

    // constants ------------------------------------------------------------------------------------------------------
    
    private static final Log LOG = LogFactory.getLog(RunnableWrapper.class);

    // internal vars --------------------------------------------------------------------------------------------------

    private static boolean tracing = false;

    // constructors ---------------------------------------------------------------------------------------------------

    public RunnableWrapper() {
    }

    // Runnable -------------------------------------------------------------------------------------------------------

    @Override
    public void run() {
        if (tracing && LOG.isTraceEnabled()) {
            LOG.trace("Thread executing runnable '{}'.");
        }
        try {
            this.work();
        } catch (Throwable throwable) {
            this.handleException(throwable);
            if (tracing && LOG.isTraceEnabled()) {
                LOG.trace("Thread with name {}).", throwable);
            }
        }
        if (tracing && LOG.isTraceEnabled()) {
            LOG.trace("Thread with name finished executing runnable ");
         }
    }

    // protected methods ----------------------------------------------------------------------------------------------

    /**
     * Perform the actual work of the asynchronous task.
     *
     * @throws Throwable Any unchecked exception that might be raised.
     */
    protected abstract void work() throws Throwable;

    /**
     * Default handling for unchecked exceptions.
     *
     * This should be overriden with a more appropriate logging method, rather than just print the stack trace.
     *
     * @param throwable Unchecked exception throw during execution of work().
     */
    protected void handleException(Throwable throwable) {
        LOG.error("Exception caught during RunnableWrapper work() execution.", throwable);
        throwable.printStackTrace();
    }
}