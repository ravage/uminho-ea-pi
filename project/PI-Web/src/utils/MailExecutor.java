package utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

public class MailExecutor {
	
	private ExecutorService executor;

	public MailExecutor() {
		executor = Executors.newCachedThreadPool();
	}
	
	public void addMailJob(SimpleMailMessage mail, MailSender mailServer){
		executor.submit(new SenderMail(mail, mailServer));		
	}
	
	public class SenderMail implements Runnable {
		
		protected final Log logger = LogFactory.getLog(this.getClass());	
		SimpleMailMessage mail;
		MailSender mailServer;

		public SenderMail(SimpleMailMessage mail, MailSender mailServer) {
			this.mail = mail;
			this.mailServer = mailServer;
		}

		@Override
		public void run() {
			mailServer.send(mail);
			logger.info("Email with subject '"+mail.getSubject() +"' successfully sent to "+mail.getTo()[0]);		
		}

	}
}
