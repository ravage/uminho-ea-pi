package utils;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class FacesUtils {
	
	private FacesUtils(){
	}
	
	public static Properties getProperties(){
		Log logger = LogFactory.getLog(FacesUtils.class);
		Properties properties = new Properties();
	try {  
    	String realPath = FacesUtils.getServletContext().getRealPath("/files");
    	properties.load( new FileInputStream(realPath+"/config.properties") );
    	return properties;
    } catch (FileNotFoundException e) {  
        logger.error("could not found the properties file:" +e);
        return null;
    } catch (IOException ex) {  
        logger.error("IO exception finding the properties file:" +ex);
		return null;
    }
    
	}

	public static FacesContext getFacesContext() {
		return FacesContext.getCurrentInstance();
	}

	public static HttpServletRequest getRequest() {
		return (HttpServletRequest) FacesUtils.getExternalContext().getRequest();
	}

	public static Boolean isAdmin() {
		return FacesUtils.getExternalContext().isUserInRole("ROLE_ADMIN");
	}
	
	public static Boolean isUser() {
		return FacesUtils.getExternalContext().isUserInRole("ROLE_USER");
	}

	public static HttpServletResponse getResponse() {
		return (HttpServletResponse) FacesUtils.getExternalContext().getResponse();
	}

	public static HttpSession getSession() {
		return (HttpSession) FacesUtils.getExternalContext().getSession(true);
	}

	public static ExternalContext getExternalContext() {
		return FacesContext.getCurrentInstance().getExternalContext();
	}

	public static ServletContext getServletContext() {
		return (ServletContext) FacesUtils.getExternalContext().getContext();
	}

	public static Object getBean(String beanName) {
		ApplicationContext appContext = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		return appContext.getBean(beanName);
	}

	public static String getRequestParameter(String name) {
		return (String) FacesUtils.getExternalContext().getRequestParameterMap().get(name);
	}

	public static void addInfoMessage(String msg) {
		addInfoMessage(null, msg);
	}

	public static void addInfoMessage(String clientId, String msg) {
		FacesContext.getCurrentInstance().addMessage(clientId,
				new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg));
	}

	public static void addErrorMessage(String msg) {
		addErrorMessage(null, msg);
	}

	public static void addErrorMessage(String clientId, String msg) {
		FacesContext.getCurrentInstance().addMessage(clientId,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg));
	}

	public static void addWarningMessage(String msg) {
		addWarningMessage(null, msg);
	}

	public static void addWarningMessage(String clientId, String msg) {
		FacesContext.getCurrentInstance().addMessage(clientId,
				new FacesMessage(FacesMessage.SEVERITY_WARN, msg, msg));
	}
}