package utils;

import java.math.BigInteger;
import java.security.SecureRandom;

public class JavaUtils {
	
	private JavaUtils(){}
	
	public static String generateRandomKey(int length){
	  SecureRandom random = new SecureRandom();
	  String key = new BigInteger(130, random).toString(32);
	  
	    return key.substring(0, (key.length() > length)?length:key.length()-1);
	}
}