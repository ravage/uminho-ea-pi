package utils;

import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

public class MailUtils {
	
	private MailUtils(){}
	
	public static void sendEmail(SimpleMailMessage mail){
		MailExecutor mailExecutor = (MailExecutor) FacesUtils.getBean("mailExecutor");
		MailSender mailServer = (MailSender) FacesUtils.getBean("mailServer");		
		mailExecutor.addMailJob(mail, mailServer);				
	}
	
	public static void sendActivationEmail(String key, String email, String username){
		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setFrom("ScaleMe Activation System <scalemeservice@gmail.com>");
		mail.setSubject("ScaleMe - Account Activation");
		mail.setTo(email);
		mail.setText("Hi! Welcome to ScaleMe.\n" +
				"You or someone else has used your email account ("+email+") to register an account at ScaleMe Portal.\n" +
						"To finish the registration process you should visit the following link in the next 24 hours to" +
						" activate your user account, otherwise the information will be automaticaly deleted by the system " +
						"and you should apply again:\n\n" +
						" http://10.1.0.2:8080/PI-Web/activation.jsf?username="+username+"&key="+ key);
		MailUtils.sendEmail(mail);
	}
	
	public static void sendRecoveryEmail(String password, String email, String username){
		SimpleMailMessage mail = new SimpleMailMessage();
		
		mail.setFrom("ScaleMe Password Recovery System <scalemeservice@gmail.com>");
		mail.setSubject("ScaleMe - Password Recovery");
		mail.setTo(email);
		mail.setText("Hi!" +
				"You or someone else has used your email account ("+email+") to request a new password for your account.\n\n" +
						"Your previous password is no longer valid. Use this credentials to login from now on:\n" +
						"\nusername: "+username+"\npassword: "+password+"\n\n ScaleMe Team.");
		MailUtils.sendEmail(mail);			
	}
	
	public static void sendSupportRequestEmail(String text, String email){
		SimpleMailMessage mail = new SimpleMailMessage();
		
		mail.setFrom("Support Center <noreply@scaleme.com>");
		mail.setReplyTo(email);
		mail.setSubject("ScaleMe - Support Request");
		mail.setTo("scalemeservice@gmail.com");
		mail.setText(text);
		MailUtils.sendEmail(mail);			
	}
	
	public static void sendServiceCreatedEmail(String url, String email){
		SimpleMailMessage mail = new SimpleMailMessage();
		
		mail.setFrom("ScaleMe Service deployer <scalemeservice@gmail.com>");
		mail.setReplyTo(email);
		mail.setSubject("ScaleMe - Service deployed");
		mail.setTo(email);
		mail.setText("Hi! \n" +
				"Your service has been created and is available at " + url);
		MailUtils.sendEmail(mail);
	}
}
