/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package orm.classes;

import org.orm.*;
import org.hibernate.Query;
import java.util.List;
import classes.*;

public class BundleComponentDAO {
	private static final org.apache.log4j.Logger _logger = org.apache.log4j.Logger.getLogger(BundleComponentDAO.class);
	public static BundleComponent loadBundleComponentByORMID(long id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadBundleComponentByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("loadBundleComponentByORMID(long id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static BundleComponent getBundleComponentByORMID(long id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getBundleComponentByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("getBundleComponentByORMID(long id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static BundleComponent loadBundleComponentByORMID(long id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadBundleComponentByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadBundleComponentByORMID(long id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static BundleComponent getBundleComponentByORMID(long id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getBundleComponentByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("getBundleComponentByORMID(long id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static BundleComponent loadBundleComponentByORMID(PersistentSession session, long id) throws PersistentException {
		try {
			return (BundleComponent) session.load(classes.BundleComponent.class, new Long(id));
		}
		catch (Exception e) {
			_logger.error("loadBundleComponentByORMID(PersistentSession session, long id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static BundleComponent getBundleComponentByORMID(PersistentSession session, long id) throws PersistentException {
		try {
			return (BundleComponent) session.get(classes.BundleComponent.class, new Long(id));
		}
		catch (Exception e) {
			_logger.error("getBundleComponentByORMID(PersistentSession session, long id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static BundleComponent loadBundleComponentByORMID(PersistentSession session, long id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (BundleComponent) session.load(classes.BundleComponent.class, new Long(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("loadBundleComponentByORMID(PersistentSession session, long id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static BundleComponent getBundleComponentByORMID(PersistentSession session, long id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (BundleComponent) session.get(classes.BundleComponent.class, new Long(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("getBundleComponentByORMID(PersistentSession session, long id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static BundleComponent[] listBundleComponentByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listBundleComponentByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("listBundleComponentByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static BundleComponent[] listBundleComponentByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listBundleComponentByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("listBundleComponentByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static BundleComponent[] listBundleComponentByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.BundleComponent as BundleComponent");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			List list = query.list();
			return (BundleComponent[]) list.toArray(new BundleComponent[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listBundleComponentByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static BundleComponent[] listBundleComponentByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.BundleComponent as BundleComponent");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			List list = query.list();
			return (BundleComponent[]) list.toArray(new BundleComponent[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listBundleComponentByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static BundleComponent loadBundleComponentByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadBundleComponentByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("loadBundleComponentByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static BundleComponent loadBundleComponentByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadBundleComponentByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadBundleComponentByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static BundleComponent loadBundleComponentByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		BundleComponent[] bundleComponents = listBundleComponentByQuery(session, condition, orderBy);
		if (bundleComponents != null && bundleComponents.length > 0)
			return bundleComponents[0];
		else
			return null;
	}
	
	public static BundleComponent loadBundleComponentByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		BundleComponent[] bundleComponents = listBundleComponentByQuery(session, condition, orderBy, lockMode);
		if (bundleComponents != null && bundleComponents.length > 0)
			return bundleComponents[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateBundleComponentByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateBundleComponentByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("iterateBundleComponentByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateBundleComponentByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateBundleComponentByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("iterateBundleComponentByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateBundleComponentByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.BundleComponent as BundleComponent");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateBundleComponentByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateBundleComponentByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.BundleComponent as BundleComponent");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateBundleComponentByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static BundleComponent createBundleComponent() {
		return new classes.BundleComponent();
	}
	
	public static boolean save(classes.BundleComponent bundleComponent) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().saveObject(bundleComponent);
			return true;
		}
		catch (Exception e) {
			_logger.error("save(classes.BundleComponent bundleComponent)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(classes.BundleComponent bundleComponent) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().deleteObject(bundleComponent);
			return true;
		}
		catch (Exception e) {
			_logger.error("delete(classes.BundleComponent bundleComponent)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.BundleComponent bundleComponent)throws PersistentException {
		try {
			if(bundleComponent.getBundle() != null) {
				bundleComponent.getBundle().bundleComponent.remove(bundleComponent);
			}
			
			if(bundleComponent.getComponent() != null) {
				bundleComponent.getComponent().bundleComponent.remove(bundleComponent);
			}
			
			return delete(bundleComponent);
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.BundleComponent bundleComponent, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(bundleComponent.getBundle() != null) {
				bundleComponent.getBundle().bundleComponent.remove(bundleComponent);
			}
			
			if(bundleComponent.getComponent() != null) {
				bundleComponent.getComponent().bundleComponent.remove(bundleComponent);
			}
			
			try {
				session.delete(bundleComponent);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(classes.BundleComponent bundleComponent) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().refresh(bundleComponent);
			return true;
		}
		catch (Exception e) {
			_logger.error("refresh(classes.BundleComponent bundleComponent)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(classes.BundleComponent bundleComponent) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().evict(bundleComponent);
			return true;
		}
		catch (Exception e) {
			_logger.error("evict(classes.BundleComponent bundleComponent)", e);
			throw new PersistentException(e);
		}
	}
	
	public static BundleComponent loadBundleComponentByCriteria(BundleComponentCriteria bundleComponentCriteria) {
		BundleComponent[] bundleComponents = listBundleComponentByCriteria(bundleComponentCriteria);
		if(bundleComponents == null || bundleComponents.length == 0) {
			return null;
		}
		return bundleComponents[0];
	}
	
	public static BundleComponent[] listBundleComponentByCriteria(BundleComponentCriteria bundleComponentCriteria) {
		return bundleComponentCriteria.listBundleComponent();
	}
}
