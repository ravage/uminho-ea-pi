/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package orm.classes;

import org.orm.*;
import org.hibernate.Query;
import java.util.List;
import classes.*;

public class ConfigurationDirectiveDAO {
	private static final org.apache.log4j.Logger _logger = org.apache.log4j.Logger.getLogger(ConfigurationDirectiveDAO.class);
	public static ConfigurationDirective loadConfigurationDirectiveByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadConfigurationDirectiveByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("loadConfigurationDirectiveByORMID(int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static ConfigurationDirective getConfigurationDirectiveByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getConfigurationDirectiveByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("getConfigurationDirectiveByORMID(int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static ConfigurationDirective loadConfigurationDirectiveByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadConfigurationDirectiveByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadConfigurationDirectiveByORMID(int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static ConfigurationDirective getConfigurationDirectiveByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getConfigurationDirectiveByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("getConfigurationDirectiveByORMID(int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static ConfigurationDirective loadConfigurationDirectiveByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (ConfigurationDirective) session.load(classes.ConfigurationDirective.class, new Integer(id));
		}
		catch (Exception e) {
			_logger.error("loadConfigurationDirectiveByORMID(PersistentSession session, int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static ConfigurationDirective getConfigurationDirectiveByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (ConfigurationDirective) session.get(classes.ConfigurationDirective.class, new Integer(id));
		}
		catch (Exception e) {
			_logger.error("getConfigurationDirectiveByORMID(PersistentSession session, int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static ConfigurationDirective loadConfigurationDirectiveByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (ConfigurationDirective) session.load(classes.ConfigurationDirective.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("loadConfigurationDirectiveByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static ConfigurationDirective getConfigurationDirectiveByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (ConfigurationDirective) session.get(classes.ConfigurationDirective.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("getConfigurationDirectiveByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static ConfigurationDirective[] listConfigurationDirectiveByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listConfigurationDirectiveByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("listConfigurationDirectiveByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static ConfigurationDirective[] listConfigurationDirectiveByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listConfigurationDirectiveByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("listConfigurationDirectiveByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static ConfigurationDirective[] listConfigurationDirectiveByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.ConfigurationDirective as ConfigurationDirective");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			List list = query.list();
			return (ConfigurationDirective[]) list.toArray(new ConfigurationDirective[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listConfigurationDirectiveByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static ConfigurationDirective[] listConfigurationDirectiveByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.ConfigurationDirective as ConfigurationDirective");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			List list = query.list();
			return (ConfigurationDirective[]) list.toArray(new ConfigurationDirective[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listConfigurationDirectiveByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static ConfigurationDirective loadConfigurationDirectiveByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadConfigurationDirectiveByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("loadConfigurationDirectiveByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static ConfigurationDirective loadConfigurationDirectiveByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadConfigurationDirectiveByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadConfigurationDirectiveByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static ConfigurationDirective loadConfigurationDirectiveByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		ConfigurationDirective[] configurationDirectives = listConfigurationDirectiveByQuery(session, condition, orderBy);
		if (configurationDirectives != null && configurationDirectives.length > 0)
			return configurationDirectives[0];
		else
			return null;
	}
	
	public static ConfigurationDirective loadConfigurationDirectiveByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		ConfigurationDirective[] configurationDirectives = listConfigurationDirectiveByQuery(session, condition, orderBy, lockMode);
		if (configurationDirectives != null && configurationDirectives.length > 0)
			return configurationDirectives[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateConfigurationDirectiveByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateConfigurationDirectiveByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("iterateConfigurationDirectiveByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateConfigurationDirectiveByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateConfigurationDirectiveByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("iterateConfigurationDirectiveByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateConfigurationDirectiveByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.ConfigurationDirective as ConfigurationDirective");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateConfigurationDirectiveByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateConfigurationDirectiveByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.ConfigurationDirective as ConfigurationDirective");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateConfigurationDirectiveByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static ConfigurationDirective createConfigurationDirective() {
		return new classes.ConfigurationDirective();
	}
	
	public static boolean save(classes.ConfigurationDirective configurationDirective) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().saveObject(configurationDirective);
			return true;
		}
		catch (Exception e) {
			_logger.error("save(classes.ConfigurationDirective configurationDirective)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(classes.ConfigurationDirective configurationDirective) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().deleteObject(configurationDirective);
			return true;
		}
		catch (Exception e) {
			_logger.error("delete(classes.ConfigurationDirective configurationDirective)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.ConfigurationDirective configurationDirective)throws PersistentException {
		try {
			if(configurationDirective.getWorker() != null) {
				configurationDirective.getWorker().configurationDirective.remove(configurationDirective);
			}
			
			return delete(configurationDirective);
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.ConfigurationDirective configurationDirective, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(configurationDirective.getWorker() != null) {
				configurationDirective.getWorker().configurationDirective.remove(configurationDirective);
			}
			
			try {
				session.delete(configurationDirective);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(classes.ConfigurationDirective configurationDirective) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().refresh(configurationDirective);
			return true;
		}
		catch (Exception e) {
			_logger.error("refresh(classes.ConfigurationDirective configurationDirective)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(classes.ConfigurationDirective configurationDirective) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().evict(configurationDirective);
			return true;
		}
		catch (Exception e) {
			_logger.error("evict(classes.ConfigurationDirective configurationDirective)", e);
			throw new PersistentException(e);
		}
	}
	
	public static ConfigurationDirective loadConfigurationDirectiveByCriteria(ConfigurationDirectiveCriteria configurationDirectiveCriteria) {
		ConfigurationDirective[] configurationDirectives = listConfigurationDirectiveByCriteria(configurationDirectiveCriteria);
		if(configurationDirectives == null || configurationDirectives.length == 0) {
			return null;
		}
		return configurationDirectives[0];
	}
	
	public static ConfigurationDirective[] listConfigurationDirectiveByCriteria(ConfigurationDirectiveCriteria configurationDirectiveCriteria) {
		return configurationDirectiveCriteria.listConfigurationDirective();
	}
}
