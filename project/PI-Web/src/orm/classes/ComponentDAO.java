/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package orm.classes;

import org.orm.*;
import org.hibernate.Query;
import java.util.List;
import classes.*;

public class ComponentDAO {
	private static final org.apache.log4j.Logger _logger = org.apache.log4j.Logger.getLogger(ComponentDAO.class);
	public static Component loadComponentByORMID(long id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadComponentByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("loadComponentByORMID(long id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Component getComponentByORMID(long id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getComponentByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("getComponentByORMID(long id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Component loadComponentByORMID(long id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadComponentByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadComponentByORMID(long id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Component getComponentByORMID(long id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getComponentByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("getComponentByORMID(long id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Component loadComponentByORMID(PersistentSession session, long id) throws PersistentException {
		try {
			return (Component) session.load(classes.Component.class, new Long(id));
		}
		catch (Exception e) {
			_logger.error("loadComponentByORMID(PersistentSession session, long id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Component getComponentByORMID(PersistentSession session, long id) throws PersistentException {
		try {
			return (Component) session.get(classes.Component.class, new Long(id));
		}
		catch (Exception e) {
			_logger.error("getComponentByORMID(PersistentSession session, long id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Component loadComponentByORMID(PersistentSession session, long id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Component) session.load(classes.Component.class, new Long(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("loadComponentByORMID(PersistentSession session, long id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Component getComponentByORMID(PersistentSession session, long id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Component) session.get(classes.Component.class, new Long(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("getComponentByORMID(PersistentSession session, long id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Component[] listComponentByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listComponentByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("listComponentByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Component[] listComponentByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listComponentByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("listComponentByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Component[] listComponentByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Component as Component");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			List list = query.list();
			return (Component[]) list.toArray(new Component[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listComponentByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Component[] listComponentByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Component as Component");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			List list = query.list();
			return (Component[]) list.toArray(new Component[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listComponentByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Component loadComponentByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadComponentByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("loadComponentByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Component loadComponentByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadComponentByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadComponentByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Component loadComponentByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Component[] components = listComponentByQuery(session, condition, orderBy);
		if (components != null && components.length > 0)
			return components[0];
		else
			return null;
	}
	
	public static Component loadComponentByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Component[] components = listComponentByQuery(session, condition, orderBy, lockMode);
		if (components != null && components.length > 0)
			return components[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateComponentByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateComponentByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("iterateComponentByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateComponentByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateComponentByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("iterateComponentByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateComponentByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Component as Component");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateComponentByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateComponentByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Component as Component");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateComponentByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Component createComponent() {
		return new classes.Component();
	}
	
	public static boolean save(classes.Component component) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().saveObject(component);
			return true;
		}
		catch (Exception e) {
			_logger.error("save(classes.Component component)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(classes.Component component) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().deleteObject(component);
			return true;
		}
		catch (Exception e) {
			_logger.error("delete(classes.Component component)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.Component component)throws PersistentException {
		try {
			classes.Worker[] lWorkers = component.worker.toArray();
			for(int i = 0; i < lWorkers.length; i++) {
				lWorkers[i].setComponent(null);
			}
			classes.BundleComponent[] lBundleComponents = component.bundleComponent.toArray();
			for(int i = 0; i < lBundleComponents.length; i++) {
				lBundleComponents[i].setComponent(null);
			}
			classes.Configuration[] lConfigurations = component.configuration.toArray();
			for(int i = 0; i < lConfigurations.length; i++) {
				lConfigurations[i].setComponent(null);
			}
			classes.Agent[] lAgents = component.agent.toArray();
			for(int i = 0; i < lAgents.length; i++) {
				lAgents[i].setComponent(null);
			}
			classes.Directive[] lDirectives = component.directive.toArray();
			for(int i = 0; i < lDirectives.length; i++) {
				lDirectives[i].setComponent(null);
			}
			return delete(component);
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.Component component, org.orm.PersistentSession session)throws PersistentException {
		try {
			classes.Worker[] lWorkers = component.worker.toArray();
			for(int i = 0; i < lWorkers.length; i++) {
				lWorkers[i].setComponent(null);
			}
			classes.BundleComponent[] lBundleComponents = component.bundleComponent.toArray();
			for(int i = 0; i < lBundleComponents.length; i++) {
				lBundleComponents[i].setComponent(null);
			}
			classes.Configuration[] lConfigurations = component.configuration.toArray();
			for(int i = 0; i < lConfigurations.length; i++) {
				lConfigurations[i].setComponent(null);
			}
			classes.Agent[] lAgents = component.agent.toArray();
			for(int i = 0; i < lAgents.length; i++) {
				lAgents[i].setComponent(null);
			}
			classes.Directive[] lDirectives = component.directive.toArray();
			for(int i = 0; i < lDirectives.length; i++) {
				lDirectives[i].setComponent(null);
			}
			try {
				session.delete(component);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(classes.Component component) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().refresh(component);
			return true;
		}
		catch (Exception e) {
			_logger.error("refresh(classes.Component component)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(classes.Component component) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().evict(component);
			return true;
		}
		catch (Exception e) {
			_logger.error("evict(classes.Component component)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Component loadComponentByCriteria(ComponentCriteria componentCriteria) {
		Component[] components = listComponentByCriteria(componentCriteria);
		if(components == null || components.length == 0) {
			return null;
		}
		return components[0];
	}
	
	public static Component[] listComponentByCriteria(ComponentCriteria componentCriteria) {
		return componentCriteria.listComponent();
	}
}
