/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package orm.classes;

import org.orm.*;
import org.hibernate.Query;
import java.util.List;
import classes.*;

public class UserDAO {
	private static final org.apache.log4j.Logger _logger = org.apache.log4j.Logger.getLogger(UserDAO.class);
	public static User loadUserByORMID(long id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadUserByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("loadUserByORMID(long id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static User getUserByORMID(long id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getUserByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("getUserByORMID(long id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static User loadUserByORMID(long id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadUserByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadUserByORMID(long id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static User getUserByORMID(long id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getUserByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("getUserByORMID(long id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static User loadUserByORMID(PersistentSession session, long id) throws PersistentException {
		try {
			return (User) session.load(classes.User.class, new Long(id));
		}
		catch (Exception e) {
			_logger.error("loadUserByORMID(PersistentSession session, long id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static User getUserByORMID(PersistentSession session, long id) throws PersistentException {
		try {
			return (User) session.get(classes.User.class, new Long(id));
		}
		catch (Exception e) {
			_logger.error("getUserByORMID(PersistentSession session, long id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static User loadUserByORMID(PersistentSession session, long id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (User) session.load(classes.User.class, new Long(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("loadUserByORMID(PersistentSession session, long id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static User getUserByORMID(PersistentSession session, long id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (User) session.get(classes.User.class, new Long(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("getUserByORMID(PersistentSession session, long id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static User[] listUserByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listUserByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("listUserByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static User[] listUserByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listUserByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("listUserByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static User[] listUserByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.User as User");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			List list = query.list();
			return (User[]) list.toArray(new User[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listUserByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static User[] listUserByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.User as User");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			List list = query.list();
			return (User[]) list.toArray(new User[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listUserByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static User loadUserByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadUserByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("loadUserByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static User loadUserByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadUserByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadUserByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static User loadUserByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		User[] users = listUserByQuery(session, condition, orderBy);
		if (users != null && users.length > 0)
			return users[0];
		else
			return null;
	}
	
	public static User loadUserByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		User[] users = listUserByQuery(session, condition, orderBy, lockMode);
		if (users != null && users.length > 0)
			return users[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateUserByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateUserByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("iterateUserByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateUserByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateUserByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("iterateUserByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateUserByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.User as User");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateUserByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateUserByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.User as User");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateUserByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static User createUser() {
		return new classes.User();
	}
	
	public static boolean save(classes.User user) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().saveObject(user);
			return true;
		}
		catch (Exception e) {
			_logger.error("save(classes.User user)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(classes.User user) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().deleteObject(user);
			return true;
		}
		catch (Exception e) {
			_logger.error("delete(classes.User user)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.User user)throws PersistentException {
		try {
			classes.Service[] lServices = user.service.toArray();
			for(int i = 0; i < lServices.length; i++) {
				lServices[i].setUser(null);
			}
			classes.Authority[] lAuthoritys = user.authority.toArray();
			for(int i = 0; i < lAuthoritys.length; i++) {
				lAuthoritys[i].setUser(null);
			}
			return delete(user);
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.User user, org.orm.PersistentSession session)throws PersistentException {
		try {
			classes.Service[] lServices = user.service.toArray();
			for(int i = 0; i < lServices.length; i++) {
				lServices[i].setUser(null);
			}
			classes.Authority[] lAuthoritys = user.authority.toArray();
			for(int i = 0; i < lAuthoritys.length; i++) {
				lAuthoritys[i].setUser(null);
			}
			try {
				session.delete(user);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(classes.User user) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().refresh(user);
			return true;
		}
		catch (Exception e) {
			_logger.error("refresh(classes.User user)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(classes.User user) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().evict(user);
			return true;
		}
		catch (Exception e) {
			_logger.error("evict(classes.User user)", e);
			throw new PersistentException(e);
		}
	}
	
	public static User loadUserByCriteria(UserCriteria userCriteria) {
		User[] users = listUserByCriteria(userCriteria);
		if(users == null || users.length == 0) {
			return null;
		}
		return users[0];
	}
	
	public static User[] listUserByCriteria(UserCriteria userCriteria) {
		return userCriteria.listUser();
	}
}
