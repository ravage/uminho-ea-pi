/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package orm.classes;

import org.orm.*;
import org.hibernate.Query;
import java.util.List;
import classes.*;

public class ServiceDAO {
	private static final org.apache.log4j.Logger _logger = org.apache.log4j.Logger.getLogger(ServiceDAO.class);
	public static Service loadServiceByORMID(long id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadServiceByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("loadServiceByORMID(long id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Service getServiceByORMID(long id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getServiceByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("getServiceByORMID(long id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Service loadServiceByORMID(long id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadServiceByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadServiceByORMID(long id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Service getServiceByORMID(long id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getServiceByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("getServiceByORMID(long id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Service loadServiceByORMID(PersistentSession session, long id) throws PersistentException {
		try {
			return (Service) session.load(classes.Service.class, new Long(id));
		}
		catch (Exception e) {
			_logger.error("loadServiceByORMID(PersistentSession session, long id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Service getServiceByORMID(PersistentSession session, long id) throws PersistentException {
		try {
			return (Service) session.get(classes.Service.class, new Long(id));
		}
		catch (Exception e) {
			_logger.error("getServiceByORMID(PersistentSession session, long id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Service loadServiceByORMID(PersistentSession session, long id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Service) session.load(classes.Service.class, new Long(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("loadServiceByORMID(PersistentSession session, long id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Service getServiceByORMID(PersistentSession session, long id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Service) session.get(classes.Service.class, new Long(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("getServiceByORMID(PersistentSession session, long id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Service[] listServiceByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listServiceByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("listServiceByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Service[] listServiceByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listServiceByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("listServiceByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Service[] listServiceByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Service as Service");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			List list = query.list();
			return (Service[]) list.toArray(new Service[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listServiceByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Service[] listServiceByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Service as Service");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			List list = query.list();
			return (Service[]) list.toArray(new Service[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listServiceByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Service loadServiceByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadServiceByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("loadServiceByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Service loadServiceByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadServiceByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadServiceByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Service loadServiceByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Service[] services = listServiceByQuery(session, condition, orderBy);
		if (services != null && services.length > 0)
			return services[0];
		else
			return null;
	}
	
	public static Service loadServiceByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Service[] services = listServiceByQuery(session, condition, orderBy, lockMode);
		if (services != null && services.length > 0)
			return services[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateServiceByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateServiceByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("iterateServiceByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateServiceByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateServiceByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("iterateServiceByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateServiceByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Service as Service");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateServiceByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateServiceByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Service as Service");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateServiceByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Service createService() {
		return new classes.Service();
	}
	
	public static boolean save(classes.Service service) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().saveObject(service);
			return true;
		}
		catch (Exception e) {
			_logger.error("save(classes.Service service)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(classes.Service service) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().deleteObject(service);
			return true;
		}
		catch (Exception e) {
			_logger.error("delete(classes.Service service)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.Service service)throws PersistentException {
		try {
			if(service.getUser() != null) {
				service.getUser().service.remove(service);
			}
			
			classes.Application[] lApplications = service.application.toArray();
			for(int i = 0; i < lApplications.length; i++) {
				lApplications[i].setService(null);
			}
			classes.Worker[] lWorkers = service.worker.toArray();
			for(int i = 0; i < lWorkers.length; i++) {
				lWorkers[i].setService(null);
			}
			return delete(service);
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.Service service, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(service.getUser() != null) {
				service.getUser().service.remove(service);
			}
			
			classes.Application[] lApplications = service.application.toArray();
			for(int i = 0; i < lApplications.length; i++) {
				lApplications[i].setService(null);
			}
			classes.Worker[] lWorkers = service.worker.toArray();
			for(int i = 0; i < lWorkers.length; i++) {
				lWorkers[i].setService(null);
			}
			try {
				session.delete(service);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(classes.Service service) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().refresh(service);
			return true;
		}
		catch (Exception e) {
			_logger.error("refresh(classes.Service service)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(classes.Service service) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().evict(service);
			return true;
		}
		catch (Exception e) {
			_logger.error("evict(classes.Service service)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Service loadServiceByCriteria(ServiceCriteria serviceCriteria) {
		Service[] services = listServiceByCriteria(serviceCriteria);
		if(services == null || services.length == 0) {
			return null;
		}
		return services[0];
	}
	
	public static Service[] listServiceByCriteria(ServiceCriteria serviceCriteria) {
		return serviceCriteria.listService();
	}
}
