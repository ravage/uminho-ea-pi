/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package orm.classes;

import org.orm.*;
import org.hibernate.Query;
import java.util.List;
import classes.*;

public class AgentDAO {
	private static final org.apache.log4j.Logger _logger = org.apache.log4j.Logger.getLogger(AgentDAO.class);
	public static Agent loadAgentByORMID(long id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadAgentByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("loadAgentByORMID(long id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Agent getAgentByORMID(long id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getAgentByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("getAgentByORMID(long id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Agent loadAgentByORMID(long id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadAgentByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadAgentByORMID(long id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Agent getAgentByORMID(long id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getAgentByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("getAgentByORMID(long id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Agent loadAgentByORMID(PersistentSession session, long id) throws PersistentException {
		try {
			return (Agent) session.load(classes.Agent.class, new Long(id));
		}
		catch (Exception e) {
			_logger.error("loadAgentByORMID(PersistentSession session, long id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Agent getAgentByORMID(PersistentSession session, long id) throws PersistentException {
		try {
			return (Agent) session.get(classes.Agent.class, new Long(id));
		}
		catch (Exception e) {
			_logger.error("getAgentByORMID(PersistentSession session, long id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Agent loadAgentByORMID(PersistentSession session, long id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Agent) session.load(classes.Agent.class, new Long(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("loadAgentByORMID(PersistentSession session, long id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Agent getAgentByORMID(PersistentSession session, long id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Agent) session.get(classes.Agent.class, new Long(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("getAgentByORMID(PersistentSession session, long id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Agent[] listAgentByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listAgentByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("listAgentByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Agent[] listAgentByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listAgentByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("listAgentByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Agent[] listAgentByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Agent as Agent");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			List list = query.list();
			return (Agent[]) list.toArray(new Agent[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listAgentByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Agent[] listAgentByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Agent as Agent");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			List list = query.list();
			return (Agent[]) list.toArray(new Agent[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listAgentByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Agent loadAgentByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadAgentByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("loadAgentByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Agent loadAgentByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadAgentByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadAgentByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Agent loadAgentByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Agent[] agents = listAgentByQuery(session, condition, orderBy);
		if (agents != null && agents.length > 0)
			return agents[0];
		else
			return null;
	}
	
	public static Agent loadAgentByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Agent[] agents = listAgentByQuery(session, condition, orderBy, lockMode);
		if (agents != null && agents.length > 0)
			return agents[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateAgentByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateAgentByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("iterateAgentByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateAgentByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateAgentByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("iterateAgentByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateAgentByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Agent as Agent");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateAgentByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateAgentByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Agent as Agent");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateAgentByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Agent createAgent() {
		return new classes.Agent();
	}
	
	public static boolean save(classes.Agent agent) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().saveObject(agent);
			return true;
		}
		catch (Exception e) {
			_logger.error("save(classes.Agent agent)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(classes.Agent agent) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().deleteObject(agent);
			return true;
		}
		catch (Exception e) {
			_logger.error("delete(classes.Agent agent)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.Agent agent)throws PersistentException {
		try {
			if(agent.getComponent() != null) {
				agent.getComponent().agent.remove(agent);
			}
			
			return delete(agent);
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.Agent agent, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(agent.getComponent() != null) {
				agent.getComponent().agent.remove(agent);
			}
			
			try {
				session.delete(agent);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(classes.Agent agent) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().refresh(agent);
			return true;
		}
		catch (Exception e) {
			_logger.error("refresh(classes.Agent agent)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(classes.Agent agent) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().evict(agent);
			return true;
		}
		catch (Exception e) {
			_logger.error("evict(classes.Agent agent)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Agent loadAgentByCriteria(AgentCriteria agentCriteria) {
		Agent[] agents = listAgentByCriteria(agentCriteria);
		if(agents == null || agents.length == 0) {
			return null;
		}
		return agents[0];
	}
	
	public static Agent[] listAgentByCriteria(AgentCriteria agentCriteria) {
		return agentCriteria.listAgent();
	}
}
