package view.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import service.UserService;
import utils.FacesUtils;


/**
 * @author andre
 *
 */
@ManagedBean
@ViewScoped
public class RecoveryBean extends BasicBean{

	private String email;
	protected UserService userService;


	public RecoveryBean() {
		super();
		this.logger.debug("RecoveryBean is created");
		this.userService = (UserService)this.lookupService("userService");
		
	}

	public String generate() throws Exception {

		if (!userService.passwordRecovery(email)){
			FacesUtils.addErrorMessage("form:email", "Email not found!");
			return null;
		}		

		return "recovered";
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}