package view.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import service.ServiceService;
import classes.Component;
import classes.Service;
import classes.Worker;

@ManagedBean
@SessionScoped
public class ServiceListBean extends BasicBean {
	
	
	private final ServiceService serviceService;
	private List<BundleObject> bundleList;
	private Service[] services;

	
	private Service selectedService;
	
	private Component bdComponent; 
	private Component storageComponent; 
	private Component appComponent;
	
	public ServiceListBean() {
		super();
		this.logger.info("started ServiceListBean:"+this.hashCode());
		serviceService = (ServiceService)lookupService("serviceService");
		services = serviceService.getServiceList();
		bundleList = new ArrayList<BundleObject>();
		if (services.length > 0){
			selectedService = services[0];
		}
		long db=0;
		long st=0;
		long w=0;
		String user;
		for (Service s : services){
			for(Worker worker : s.worker.toArray()){
				if (worker.getComponent().getKey().equals("Database")){
					bdComponent = worker.getComponent();
					db = worker.getQuantity();
				}else if (worker.getComponent().getKey().equals("Storage")){
					storageComponent=worker.getComponent();
					st = worker.getQuantity();
				}else if (worker.getComponent().getKey().equals("Application")){
					appComponent=worker.getComponent();
					w = worker.getQuantity();
				}
			}
			user = s.getUser().getUsername();
			bundleList.add(new BundleObject(user,s.getName(), s.getPrice(), s.getDuration(),s.getInitDate(),db,st,w,s.getId()));
		}
	}

	
	public List<BundleObject> getBundleList(){
		return bundleList;
	}
	public void setBundleList(List<BundleObject> bundlelist){
		this.bundleList=bundlelist;
	}
	
	public Service[] getServices() {
		return services;
	}


	public void setServices(Service[] services) {
		this.services = services;
	}


	public Service getselectedService(){
		return selectedService;
	}
	public void setselectedService(Service s){
		this.selectedService=s;
	}
	
    
    public void setBdComponent(Component bdComponent) {
		this.bdComponent = bdComponent;
	}


	public Component getBdComponent() {
		return bdComponent;
	}


	public void setStorageComponent(Component storageComponent) {
		this.storageComponent = storageComponent;
	}


	public Component getStorageComponent() {
		return storageComponent;
	}


	public void setAppComponent(Component appComponent) {
		this.appComponent = appComponent;
	}


	public Component getAppComponent() {
		return appComponent;
	}


	public class BundleObject{

    	private String user;
		private String name;
		private BigDecimal price;
		private short duration;
		private Date initdate;
		private long database;
		private long storage;
		private long worker;
		private long id;

		public BundleObject(String user, String name, BigDecimal price, short duration, Date initdate, long database, long storage, long worker, long id) {
			super();
			this.setUser(user);
			this.name = name;
			this.price = price;
			this.duration = duration;
			this.initdate= initdate;
			this.database=database;
			this.storage=storage;
			this.worker=worker;
			this.id=id;
			
		}

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public BigDecimal getPrice() {
			return price;
		}
		public void setPrice(BigDecimal price) {
			this.price = price;
		}
		public short getDuration() {
			return duration;
		}
		public void setDuration(short duration) {
			this.duration = duration;
		}

		public void setInitdate(Date initdate) {
			this.initdate = initdate;
		}

		public Date getInitdate() {
			return initdate;
		}

		public void setWorker(long worker) {
			this.worker = worker;
		}

		public long getWorker() {
			return worker;
		}

		public void setUser(String user) {
			this.user = user;
		}

		public String getUser() {
			return user;
		}

		public void setDatabase(long database) {
			this.database = database;
		}

		public long getDatabase() {
			return database;
		}

		public void setStorage(long storage) {
			this.storage = storage;
		}

		public long getStorage() {
			return storage;
		}
	}

}
