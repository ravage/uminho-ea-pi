package view.beans;

import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.html.HtmlDataTable;

import service.UserService;
import utils.FacesUtils;

import classes.User;

@ManagedBean
@SessionScoped
public class ListUsersBean extends BasicBean{
	
	protected UserService userService;

	private User[] users;
	private ArrayList<UserObject> usersList;
	private User selectedUser;
	private HtmlDataTable table;
	
	
	public ListUsersBean() {
		
		super();
		this.userService = (UserService)this.lookupService("userService");
		this.logger.info("started ListUsersBean:"+this.hashCode());
		
		users = userService.getUsersList();// retorna lista de utilizadores
		usersList = new ArrayList<UserObject>();
		for (User u : users){
			usersList.add(new UserObject(u.getUsername(), u.getName(), u.getEmail(), u.getActive(), false));
		}
	}
	
	public void activateUser(UserObject uObj){
		User user = null;
		for (User u: users){
			if (u.getUsername().equals(uObj.getUsername()))
				user = u;
		}
		if (userService.activateUser(user)){
			uObj.setActive(true);
		}else{
			FacesUtils.addErrorMessage("Failed to activate user.");
		}
	}
	
	public void desactivateUser(UserObject uObj){
		User user = null;
		for (User u: users){
			if (u.getUsername().equals(uObj.getUsername()))
				user = u;
		}
		if (userService.desactivateUser(user)){
			uObj.setActive(false);
		}else{
			FacesUtils.addErrorMessage("Failed to desactivate user.");
		}
	}

	public ArrayList<UserObject> getUsersList() {
		return usersList;
	}

	public void setUsersList(ArrayList<UserObject> usersList) {
		this.usersList = usersList;
	}

	public void setTable(HtmlDataTable table) {
		this.table = table;
	}

	public HtmlDataTable getTable() {
		return table;
	}

	public void setSelectedUser(User selectedUser) {
		this.selectedUser = selectedUser;
	}

	public User getSelectedUser() {
		return selectedUser;
	}
	
	public class UserObject{
		
		String username;
		String name;
		String email;
		boolean active;
		boolean hasService;

		public UserObject(String username, String name, String email,
				boolean active, boolean hasService) {
			super();
			this.username = username;
			this.name = name;
			this.email = email;
			this.active = active;
			this.hasService = hasService;
		}
		public UserObject(UserObject u) {
			super();
			this.username = u.getUsername();
			this.name = u.getName();
			this.email = u.getEmail();
			this.active = u.isActive();
			this.hasService = u.isHasService();
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public boolean isActive() {
			return active;
		}

		public void setActive(boolean active) {
			this.active = active;
		}

		public boolean isHasService() {
			return hasService;
		}

		public void setHasService(boolean hasService) {
			this.hasService = hasService;
		}
		
	}


}
