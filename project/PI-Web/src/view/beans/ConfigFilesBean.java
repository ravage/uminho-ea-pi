package view.beans;

import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;

import classes.Component;
import classes.Configuration;

import service.ComponentsService;
import utils.FacesUtils;

@ManagedBean
@SessionScoped
public class ConfigFilesBean extends BasicBean {
	
	private final ComponentsService componentsService;

	private String fileName;
	private String template;
	private ArrayList<Component> components;
	private Component selectedComponent;
	private Configuration actualConfiguration;
	
	private HtmlInputTextarea templateArea;
	private HtmlInputText fileNameInput;
	

	public ConfigFilesBean() {
		super();
		componentsService = (ComponentsService) lookupService("componentsService");
		components = new ArrayList<Component>(); 
		for (Component c:componentsService.getComponents()){
			components.add(c);
		}
		selectedComponent = components.get(0);
		if (selectedComponent.configuration.size()>0){
			actualConfiguration = selectedComponent.configuration.toArray()[0];
		}
		if (actualConfiguration != null){
			fileName = actualConfiguration.getName();
			template = actualConfiguration.getTemplate();
		}else{
			fileName = "";
			template = "";			
		}
	}

	public void saveFile(AjaxBehaviorEvent event) throws AbortProcessingException{
		if (actualConfiguration == null){
			actualConfiguration = new Configuration();
			actualConfiguration.setComponent(selectedComponent);
		}
		actualConfiguration.setName((String)fileNameInput.getValue());
		actualConfiguration.setTemplate((String)templateArea.getValue());

		if (!componentsService.saveFile(actualConfiguration)){
			FacesUtils.addErrorMessage("Failed to save directive");
		}else{
			FacesUtils.addInfoMessage("File saved");
		}
	}

	public void changeComponent(ValueChangeEvent event) throws AbortProcessingException{
		int selectedIndex = Integer.parseInt(event.getNewValue().toString()) - 1;

		if (components.indexOf(selectedComponent) != selectedIndex){
			selectedComponent = components.get(selectedIndex);	
			if (selectedComponent.configuration.size()>0){	
				actualConfiguration = selectedComponent.configuration.toArray()[0];
			}else{
				actualConfiguration = null;
			}
			
			if (actualConfiguration != null){
				fileName = actualConfiguration.getName();
				template = actualConfiguration.getTemplate();
			}else{
				fileName = "";
				template = "";
				
			}
		}
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public ArrayList<Component> getComponents() {
		return components;
	}

	public void setComponents(ArrayList<Component> components) {
		this.components = components;
	}

	public HtmlInputTextarea getTemplateArea() {
		return templateArea;
	}

	public void setTemplateArea(HtmlInputTextarea templateArea) {
		this.templateArea = templateArea;
	}

	public HtmlInputText getFileNameInput() {
		return fileNameInput;
	}

	public void setFileNameInput(HtmlInputText fileNameInput) {
		this.fileNameInput = fileNameInput;
	}
}
