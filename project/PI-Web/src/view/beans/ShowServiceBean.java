package view.beans;

import java.io.IOException;
import java.io.FileOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.math.BigDecimal;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.commons.io.IOUtils;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.commons.io.FilenameUtils;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


import service.ServiceService;
import utils.FacesUtils;

import classes.Application;
import classes.Service;

@ManagedBean
@ViewScoped
public class ShowServiceBean extends BasicBean{

	private ServiceService serviceService;
	private List<Application> applications;
	
	private HtmlDataTable dataTable;	
	
	private boolean hasApps;
	private boolean hasService;

	private BigDecimal price;
	private short duration;
	private long database;
	private long storage;
	private long workers;
	private String appName;
	private String serviceName;
	private String serviceDesc;

	private Service service;

	private UploadedFile uploadedFile;
	private String fileName;

	public ShowServiceBean() {
		super();
		serviceService = (ServiceService)lookupService("serviceService");
		this.logger.debug("AddService is created");
	}
	
	@PostConstruct
	public void init(){
				
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		service = serviceService.getServiceForUser(auth.getName());
		
		if (service != null){
			hasService = true;
			applications = new ArrayList<Application>();
			for (Application a: service.application.toArray()){
				applications.add(a);
			}
		}else{
			hasService = false;
		}
	}
	
	public void addApp() {

		String prefix = FilenameUtils.getBaseName(uploadedFile.getName());
		String suffix = FilenameUtils.getExtension(uploadedFile.getName());
		
		if (!suffix.equals("zip")){
			FacesUtils.addErrorMessage("uploadForm:file","Please upload a .zip file");
			return;
		}
		
		Properties properties = FacesUtils.getProperties();
		if (properties == null){
			FacesUtils.addErrorMessage("uploadForm:file","Failed to upload file. Please contact support");
			return;
		}          

		File file = null;
		OutputStream output = null;

		try {
			// upload file
			file = File.createTempFile(prefix + "_", "." + suffix,
					new File(properties.getProperty("diskpath.store", "/mnt/storage/paas/uploads/")));
			output = new FileOutputStream(file);
			IOUtils.copy(uploadedFile.getInputStream(), output);
			
			// create and Save application
			Application app = new Application();
			app.setFileLocation(file.getPath());
			app.setName(getAppName());
			app.setUrl("http://"+service.getName());
			app.setService(service);
			
			if (!serviceService.addApplication(app)){
				FacesUtils.addErrorMessage("uploadForm:file",
						"Error creating application at our system. Please try again or contact support!");
				return;
			}
			// add application to UI
			applications.add(app);	
			FacesUtils.addInfoMessage("uploadForm:appname", "Application added sucessfully!");
						
		} catch (IOException e) {
			
			if (file != null) file.delete();
			FacesUtils.addErrorMessage("uploadForm:file","File upload failed with I/O error.");
			
			logger.error("Failed to save file '"+ file.getName() +"': "+e);
			
		} finally {
			IOUtils.closeQuietly(output);
		}
	}
	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public short getDuration() {
		return duration;
	}

	public void setDuration(short duration) {
		this.duration = duration;
	}

	public boolean isHasService() {
		return hasService;
	}

	public void setHasService(boolean hasService) {
		this.hasService = hasService;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<Application> getApplications() {
		return applications;
	}

	public void setApplications(List<Application> applications) {
		this.applications = applications;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceDesc() {
		return serviceDesc;
	}

	public void setServiceDesc(String serviceDesc) {
		this.serviceDesc = serviceDesc;
	}

	public long getDatabase() {
		return database;
	}

	public void setDatabase(long database) {
		this.database = database;
	}

	public long getStorage() {
		return storage;
	}

	public void setStorage(long storage) {
		this.storage = storage;
	}

	public long getWorkers() {
		return workers;
	}

	public void setWorkers(long workers) {
		this.workers = workers;
	}

	public boolean isHasApps() {
		return applications != null && applications.size()>0;
	}

	public void setHasApps(boolean hasApps) {
		this.hasApps = hasApps;
	}

}
