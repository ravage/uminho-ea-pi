package view.beans;

import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;

import service.UserService;

/**
 * @author andre
 *
 */
public class UserBean extends BasicBean{

	protected String username;
	protected String password;
	protected String passwordConfirmation;
	protected String name;
	protected boolean gender;
	protected Date birthdayDate;
	protected String email;
	protected String nif;

	protected UserService userService;


	@Inject
	public UserBean() {
		super();
		this.userService = (UserService)this.lookupService("userService");
	}


	public String getUsername (){
		return username;
	}

	public void setUsername (final String username){
		this.username = username;
	}

	public String getPassword (){
		return password;
	}

	public void setPassword (final String password){
		this.password = password;
	}

	public String getPasswordConfirmation (){
		return passwordConfirmation;
	}

	public void setPasswordConfirmation (final String passwordConfirmation){
		this.passwordConfirmation = passwordConfirmation;
	}

	public String getName (){
		return name;
	}

	public void setName (final String name){
		this.name = name;
	}
	
	public boolean getGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}
	
	public Date getBirthdayDate() {
		return birthdayDate;
	}

	public void setBirthdayDate(Date birthdayDate) {
		this.birthdayDate = birthdayDate;
	}

	public String getEmail (){
		return email;
	}

	public void setEmail (final String email){
		this.email = email;
	}

	public String getNif (){
		return nif;
	}

	public void setNif (final String nif){
		this.nif = nif;
	}
}