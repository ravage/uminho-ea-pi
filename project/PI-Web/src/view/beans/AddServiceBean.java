package view.beans;

import java.math.BigDecimal;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.faces.component.html.HtmlDataTable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import service.ServiceService;
import service.UserService;
import utils.FacesUtils;

import classes.Application;
import classes.Bundle;
import classes.BundleComponent;
import classes.Component;
import classes.Service;
import classes.Worker;

@ManagedBean
@ViewScoped
public class AddServiceBean extends BasicBean{

	private ServiceService serviceService;
	private UserService userService;
	private List<BundleObject> bundleList;
	private List<Application> applications;
	
	public boolean isHasService() {
		return hasService;
	}

	public void setHasService(boolean hasService) {
		this.hasService = hasService;
	}

	private HtmlDataTable dataTable;

	private BundleObject bundle;	
	
	private boolean hasApps;

	private BigDecimal price;
	private short duration;
	private long database;
	private long storage;
	private long workers;
	private String appName;
	private String serviceName;
	private String serviceDesc;
	private boolean hasService;

	public boolean isHasApps() {
		return hasApps;
	}

	private Component bdComponent; 
	private Component storageComponent; 
	private Component appComponent;
	private Component fcComponent; 
	private Component dnsComponent;

	private Service service;

	private UploadedFile uploadedFile;
	private String fileName;

	public AddServiceBean() {
		super();
		serviceService = (ServiceService)lookupService("serviceService");
		userService = (UserService)lookupService("userService");
		this.logger.debug("AddService is created");
	}
	
	@PostConstruct
	public void init(){
		bundleList = new ArrayList<BundleObject>();
		ArrayList<Bundle> list = new ArrayList<Bundle>(serviceService.getBundles());
		Integer db=0;
		Integer st=0;
		Integer wk=0;
		for (Bundle b:list){
			for (BundleComponent bc : b.bundleComponent.toArray()){
				if (bc.getComponent().getKey().equals("Database")){
					bdComponent = bc.getComponent();
					db = bc.getQuantity();
				}else if (bc.getComponent().getKey().equals("Storage")){
					storageComponent = bc.getComponent();
					st = bc.getQuantity();
				}else if (bc.getComponent().getKey().equals("Application")){
					appComponent = bc.getComponent();
					wk = bc.getQuantity();
				}else if (bc.getComponent().getKey().equals("FrontController")){
					fcComponent = bc.getComponent();
				}else if (bc.getComponent().getKey().equals("DNS")){
					dnsComponent = bc.getComponent();
				}
			}
			bundleList.add(new BundleObject(b.getName(), b.getPrice(), db, st, wk, b.getDuration()));
		}
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		service = serviceService.getServiceForUser(auth.getName());
		
		if (service != null){
			hasService = true;
			serviceName = service.getName();
			serviceDesc = service.getDescription();
			duration = service.getDuration();
			price = service.getPrice();
			for (Worker w:service.worker.toArray()){
				if (w.getComponent().getKey().equals("Database")){
					database = w.getQuantity();
				}else if (w.getComponent().getKey().equals("Storage")) {
					storage = w.getQuantity();
				}else if (w.getComponent().getKey().equals("Application")) {
					workers = w.getQuantity();
				}
			}
			serviceName = service.getName();
			serviceDesc = service.getDescription();
		}else{
			hasService = false;
		}
	}

	public String addService(){
		
		Properties p = FacesUtils.getProperties();

		Worker workerBd = new Worker();
		workerBd.setComponent(bdComponent);
		workerBd.setQuantity(getDatabase());
		workerBd.setDeploy_to(p.getProperty("deploy.database", "10.2.0.4"));

		Worker workerSt = new Worker();
		workerSt.setComponent(storageComponent);
		workerSt.setQuantity(getStorage());
		workerSt.setDeploy_to(p.getProperty("deploy.storage", "10.2.0.6"));

		Worker workerAp = new Worker();
		workerAp.setComponent(appComponent);
		workerAp.setQuantity(getWorkers());
		workerAp.setDeploy_to(p.getProperty("deploy.app", "10.2.0.2"));	

		Worker workerFc = new Worker();
		workerFc.setComponent(fcComponent);
		workerFc.setQuantity(getWorkers());
		workerFc.setDeploy_to(p.getProperty("deploy.fc", "10.2.0.5"));	

		Worker workerDns = new Worker();
		workerDns.setComponent(dnsComponent);
		workerDns.setQuantity(0);
		workerDns.setDeploy_to(p.getProperty("deploy.dns", "10.2.0.1"));		
		
		Service service = new Service();
		service.worker.add(workerAp);
		service.worker.add(workerBd);
		service.worker.add(workerSt);
		service.worker.add(workerDns);
		service.worker.add(workerFc);
		service.setDuration(getDuration());
		service.setPrice(getPrice());
		service.setDescription(getServiceDesc());
		service.setInitDate(new Date());
		service.setName(getServiceName());
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		service.setUser(userService.getUser(auth.getName()));

		if (!serviceService.createService(service)){
			FacesUtils.addErrorMessage("Failed to start Service. Please contact support.");
			return null;
		}
		this.service = service; 
		return "servicecreated";
	}

	public void changeBundle(){
		database = bundle.database;
		storage = bundle.storage;
		workers = bundle.workers;
		price = bundle.price;
		duration = bundle.duration;
		return;
	}

	public void setBundleList(List<BundleObject> bundleList) {
		this.bundleList = bundleList;
	}

	public List<BundleObject> getBundleList(){
		return bundleList; 
	}

	public BundleObject getBundle() {
		return bundle;
	}

	public void setBundle(BundleObject bundle) {
		this.bundle = bundle;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public short getDuration() {
		return duration;
	}

	public void setDuration(short duration) {
		this.duration = duration;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<Application> getApplications() {
		return applications;
	}

	public void setApplications(List<Application> applications) {
		this.applications = applications;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceDesc() {
		return serviceDesc;
	}

	public void setServiceDesc(String serviceDesc) {
		this.serviceDesc = serviceDesc;
	}

	public long getDatabase() {
		return database;
	}

	public void setDatabase(long database) {
		this.database = database;
	}

	public long getStorage() {
		return storage;
	}

	public void setStorage(long storage) {
		this.storage = storage;
	}

	public long getWorkers() {
		return workers;
	}

	public void setWorkers(long workers) {
		this.workers = workers;
	}

	public void setHasApps(boolean hasApps) {
		this.hasApps = hasApps;
	}

	public class BundleObject{

		private String name;
		private BigDecimal price;
		private Integer database;
		private Integer storage;
		private Integer workers;
		private short duration;

		public BundleObject(String name, BigDecimal price, Integer database,
				Integer storage, Integer workers, short duration) {
			super();
			this.name = name;
			this.price = price;
			this.database = database;
			this.storage = storage;
			this.workers = workers;
			this.duration = duration;
		}

		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public BigDecimal getPrice() {
			return price;
		}
		public void setPrice(BigDecimal price) {
			this.price = price;
		}
		public Integer getDatabase() {
			return database;
		}
		public void setDatabase(Integer database) {
			this.database = database;
		}
		public Integer getStorage() {
			return storage;
		}
		public void setStorage(Integer storage) {
			this.storage = storage;
		}
		public Integer getWorkers() {
			return workers;
		}
		public void setWorkers(Integer workers) {
			this.workers = workers;
		}

		public short getDuration() {
			return duration;
		}

		public void setDuration(short duration) {
			this.duration = duration;
		}
	}

}
