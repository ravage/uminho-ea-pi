/**
 * 
 */
package view.beans;

import java.io.IOException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import utils.FacesUtils;



/**
 * @author andre
 *
 */
@ManagedBean
@SessionScoped
public class LoginBean extends BasicBean{

	private String username;
	private String name;
	private String password;
	private boolean rememberMe;
	private boolean loggedIn;
	private boolean admin;

	public LoginBean() {
		super();
		this.logger.debug("LoginBean is created");
	}

	public String loginAction() throws IOException, ServletException{

		FacesUtils.getExternalContext().dispatch("/j_spring_security_check?j_username=" + username + "&j_password=" + password + "&_spring_security_remember_me=" + rememberMe);
		FacesUtils.getFacesContext().responseComplete();
		loggedIn = true;  
		return null;
	}    

	public void init(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		admin = FacesUtils.isAdmin();
		loggedIn = FacesUtils.isUser();
		name = auth.getName();
	}


	public String logoutAction() throws IOException, ServletException{

		RequestDispatcher dispatcher = FacesUtils.getRequest()
		.getRequestDispatcher("/j_spring_security_logout");

		dispatcher.forward(FacesUtils.getRequest(),FacesUtils.getResponse());

		FacesContext.getCurrentInstance().responseComplete();

		username = null;
		password = null;
		loggedIn = false;
		admin = false;
		rememberMe = false;
		return null;

	}

	public String getUsername (){
		return username;
	}

	public void setUsername (final String username){
		this.username = username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getPassword (){
		return password;
	}

	public void setPassword (final String password){
		this.password = password;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public boolean isRememberMe(){
		return this.rememberMe;
	}

	public void setRememberMe(final boolean rememberMe){
		this.rememberMe = rememberMe;
	}

}