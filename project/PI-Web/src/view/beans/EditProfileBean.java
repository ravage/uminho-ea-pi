package view.beans;


import javax.annotation.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import utils.FacesUtils;

import classes.User;

@ManagedBean
@ViewScoped
public class EditProfileBean extends UserBean {
	
	private String newPassword;
	private boolean passwordChanged; 


	public EditProfileBean() {
		super();
		this.logger.info("EditProfileBean is created");
	}

	public String save() throws Exception {
		
		
		if ((newPassword != null && newPassword.length()>0)
				|| (passwordConfirmation!= null && passwordConfirmation.length()>0)) {

			if (newPassword != null && newPassword.length()<5){
				FacesUtils.addErrorMessage("editProfileForm:newPassword", "Password must has at least 5 characters");
				return null;
			}

			if (newPassword == null || passwordConfirmation == null ||
					newPassword.length() == 0 || passwordConfirmation.length() ==0 ||
					!newPassword.equals(passwordConfirmation)) {
				FacesUtils.addErrorMessage("editProfileForm:confirmPassword", "Passwords don't match!");
				return null;
			}
		}
		
		if (!userService.checkPassword(SecurityContextHolder.getContext().getAuthentication().getName(), password)){
			FacesUtils.addErrorMessage("editProfileForm:password", "Wrong password");
			return null;
		} 
			

		User user = new User();

		user.setUsername( SecurityContextHolder.getContext().getAuthentication().getName());
		user.setPassword(newPassword);
		user.setNif(getNif());
		user.setName(getName());	
		user.setEmail(getEmail());		

		if (userService.updateUser(user) != null){
			passwordChanged = getNewPassword().length()>0;
			return "edited";
		}else{
			FacesUtils.addErrorMessage("Failed to update user!");
			return null;
		}
	}
	
	public void init() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

			User user = userService.getUser(auth.getName());
			if (user != null){
				username = user.getUsername();
				name = user.getName();
				email = user.getEmail();
				if (user.getNif() != null){
					nif = user.getNif();
				}		
		}
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public boolean isPasswordChanged() {
		return passwordChanged;
	}

	public void setPasswordChanged(boolean passwordChanged) {
		this.passwordChanged = passwordChanged;
	}


}
