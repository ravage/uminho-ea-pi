package view.beans;

import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;

import orm.classes.ComponentDAO;
import orm.classes.DirectiveDAO;
import orm.classes.UserDAO;

import service.ComponentsService;

import utils.FacesUtils;

import classes.Component;
import classes.Directive;

@ManagedBean
@SessionScoped
public class DirectivesBean extends BasicBean{
	
	private final ComponentsService componentsService;

	private ArrayList<Directive> directives;
	private ArrayList<Component> components;
	
	private Component selectedComponent;

	private HtmlInputText name;
	private HtmlInputText value;
	private HtmlDataTable dataTable;

	private String clear;
	
	public DirectivesBean() {
		super();
		this.logger.info("started DirectivesBean:"+this.hashCode());
		componentsService = (ComponentsService) lookupService("componentsService");
		
		components = new ArrayList<Component>(); 
		for (Component c:componentsService.getComponents()){
			components.add(c);
		}
		selectedComponent = components.get(0);
		directives = new ArrayList<Directive>(); 
		for (Directive d:componentsService.getDirectivesForComponent(selectedComponent.getKey())){
			directives.add(d);
		}
	}

	public void removeDirective(AjaxBehaviorEvent event) throws AbortProcessingException{		
		Directive d = (Directive) dataTable.getRowData();
		
		if (componentsService.removeDirective(d)){
			directives.remove(d);
		}else{
			FacesUtils.addErrorMessage("Failed to delete directive");
		}	
	}
	
	public void addDirective(AjaxBehaviorEvent event) throws AbortProcessingException{
		
		Directive directive = DirectiveDAO.createDirective();
		directive.setName((String)name.getValue());
		directive.setValue((String)value.getValue());
		directive.setComponent(selectedComponent);
		
		directive = componentsService.addDirective(directive); 
		if (directive != null){
			directives.add(directive);
			clear = "";
		}else{
			FacesUtils.addErrorMessage("Failed to save directive");
		}		
	}

	public void changeComponent(ValueChangeEvent event) throws AbortProcessingException{
		int selectedIndex = Integer.parseInt(event.getNewValue().toString()) - 1;
		
		if (components.indexOf(selectedComponent) != selectedIndex){
			selectedComponent = components.get(selectedIndex);
			directives = new ArrayList<Directive>();
			for (Directive d : selectedComponent.directive.toArray()){
				directives.add(d);
			}
		}
	}
	
	public ArrayList<Directive> getDirectives() {
		return directives;
	}

	public void setDirectives(ArrayList<Directive> directives) {
		this.directives = directives;
	}

	public ArrayList<Component> getComponents() {
		return components;
	}

	public void setComponents(ArrayList<Component> components) {
		this.components = components;
	}

	public HtmlInputText getName() {
		return name;
	}

	public void setName(HtmlInputText name) {
		this.name = name;
	}

	public HtmlInputText getValue() {
		return value;
	}

	public void setValue(HtmlInputText value) {
		this.value = value;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public String getClear() {
		return clear;
	}

	public void setClear(String clear) {
		this.clear = clear;
	}	
	
}
