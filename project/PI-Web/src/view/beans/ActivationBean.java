package view.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;

import service.UserService;

@ManagedBean
@RequestScoped
public class ActivationBean extends BasicBean {

	private boolean active;
	private String username;
	private String key;

	protected UserService userService;

	public ActivationBean() {
		super();
		this.userService = (UserService)this.lookupService("userService");
		this.logger.debug("ActivationBean is created");
	}
	
	public void init() {
		this.logger.info("tryActivation: username->"+username+" key->"+key);		
		active = userService.activateAccount(username, key);
	}
	
	public boolean isActive() {
		return active;
	}
	
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}	

}
