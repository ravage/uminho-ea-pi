package view.beans;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import utils.FacesUtils;

import classes.User;


/**
 * @author andre
 *
 */
@ManagedBean
@RequestScoped
public class RegisterBean extends UserBean{

	public RegisterBean() {
		super();
		this.logger.debug("RegisterBean is created");
	}

	public String register() throws Exception {
		
		if (!password.equals(passwordConfirmation)){
			FacesUtils.addErrorMessage("registerForm:confirmPasswordRegister", "Passwords don't match");
			return null;
		}
		
		
		User user = new User();

		user.setUsername(getUsername());
		user.setPassword(getPassword());
		user.setEmail(getEmail());
		user.setName(getName());

		if (userService.addUser(user) != null){
			return "registered";
		}else{
			return null;
		}
	}

	public void init() {

	}
	
	public void validateUsername(FacesContext context, UIComponent componentToValidate,
			Object value) throws ValidatorException {

		String inputUser = value.toString(); 

		if(inputUser.length() < 4 || inputUser.length() > 20){
			FacesMessage message =
				new FacesMessage("Username must has from 4 to 20 chars");
			throw new ValidatorException(message);
		}else if(userService.checkUser(inputUser)){
			FacesMessage message =
				new FacesMessage("Username already exists");
			throw new ValidatorException(message);
		}
	}

}