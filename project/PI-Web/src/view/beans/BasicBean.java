package view.beans;


import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class BasicBean {
	protected final Log logger = LogFactory.getLog(this.getClass());
	
	private ApplicationContext appContext;
	
	public BasicBean() {
		ServletContext context = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
		this.appContext = WebApplicationContextUtils.getRequiredWebApplicationContext(context);
	}

	public Object lookupService(String serviceBeanName) {
		return appContext.getBean(serviceBeanName);
	}

}
