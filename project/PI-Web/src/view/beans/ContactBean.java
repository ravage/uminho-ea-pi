package view.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import service.UserService;

@ManagedBean
@RequestScoped
public class ContactBean extends BasicBean {
	
	private final UserService userService;
	
	private String email;
	private String text;

	public ContactBean() {
		super();
		userService =  (UserService) lookupService("userService");
	}
	
	public String sendMessage(){
		
		userService.sendSupportMessage(email, text);		
		return "sent";
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	
	

}
