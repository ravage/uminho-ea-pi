package view.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.html.HtmlSelectOneRadio;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ValueChangeEvent;

import service.ServiceService;

import classes.Component;
import classes.Service;
import classes.Statistic;
import classes.Worker;


@ManagedBean
@RequestScoped
public class ServiceStatisticAdminBean extends BasicBean {

	private List<Worker> workers;
	private ServiceService serviceService;
	private Service service;
	private Boolean dbSel;

	private Component bdComponent;
	private Component storageComponent;
	private Component appComponent;
	private List<DbChartBean> lista;
	private List<DbChartBean> listacpu;
	private List<DbChartBean> listamem;

	private Boolean spaceGraphic;
	private Boolean cpuGraphic;
	private Boolean memoryGraphic;
	private Boolean gotService;
	
	private long id;

	public ServiceStatisticAdminBean() {
		super();
		this.logger.debug("ServiceStatisticAdmin is created");
	}

	public void init(){
		long database=0;
		long storage=0;
		gotService=true;
		spaceGraphic=false;
		cpuGraphic=false;
		memoryGraphic=false;
		Statistic statistic = null;
		Statistic statistic1 = null;
		List<Statistic> acpuistatistics = null;
		List<Statistic> acpuastatistics = null;
		List<Statistic> dbmstatistics = null;
		List<Statistic> phpmstatistics = null;
		List<Statistic> dcpuistatistics = null;
		List<Statistic> dcpuastatistics = null;
		long st =0;
		long st1 =0;

		serviceService = (ServiceService)lookupService("serviceService");
		service = serviceService.getServiceForId(id);
		if(service == null){
			gotService=false;
			return;
		}
		for(Worker worker : service.worker.toArray()){
			if (worker.getComponent().getKey().equals("Database")){
				bdComponent = worker.getComponent();
				database = worker.getQuantity();
				statistic = serviceService.getDBLastStatistic(service.getId());
				dbmstatistics = serviceService.getDBMStatistic(service.getId());
				dcpuistatistics = serviceService.getDCPUIStatistic(service.getId());
				dcpuastatistics = serviceService.getDCPUAStatistic(service.getId());
				//st = statistic.getUsage();
			}else if (worker.getComponent().getKey().equals("Storage")){
				storageComponent = worker.getComponent();
				storage = worker.getQuantity();
				statistic1 = serviceService.getStorageLastStatistic(service.getId());
				//st1=statistic1.getUsage();
			}else if (worker.getComponent().getKey().equals("Application")){
				appComponent = worker.getComponent();
				acpuistatistics = serviceService.getACPUIStatistic(service.getId());
				acpuastatistics = serviceService.getACPUAStatistic(service.getId());
				phpmstatistics = serviceService.getPHPMStatistic(service.getId());
			}
		}
		if(statistic!= null && statistic1!=null){
			st = statistic.getUsage();
			st1=statistic1.getUsage();
			spaceGraphic=true;
			lista = new ArrayList<ServiceStatisticAdminBean.DbChartBean>();
			DbChartBean totalspace = new DbChartBean();
			totalspace.setName("Free Space");
			totalspace.setV(storage+database-st-st1);
			lista.add(totalspace);
			DbChartBean databaseSpace = new DbChartBean();
			databaseSpace.setName("Database space used");
			databaseSpace.setV(st);
			lista.add(databaseSpace);
			DbChartBean storageSpace = new DbChartBean();
			storageSpace.setName("Storage space used");
			storageSpace.setV(st1);
			lista.add(storageSpace);
		}
		
		listamem = new ArrayList<ServiceStatisticAdminBean.DbChartBean>();
		if (acpuistatistics.size() > 0 && acpuastatistics.size() > 0 && dcpuistatistics.size() > 0 && dcpuastatistics.size() > 0 ){
			cpuGraphic=true;
			listacpu = new ArrayList<ServiceStatisticAdminBean.DbChartBean>();
			for (int i = acpuistatistics.size()-1; i >= 0; i--){
				DbChartBean cpuUsage = new DbChartBean();
				cpuUsage.setName(acpuistatistics.get(i).getCreatedAt().toString().substring(0,10));
				cpuUsage.setV(acpuistatistics.get(i).getUsage());
				cpuUsage.setV1(acpuastatistics.get(i).getUsage());
				cpuUsage.setV2(dcpuistatistics.get(i).getUsage());
				cpuUsage.setV3(dcpuastatistics.get(i).getUsage());
				listacpu.add(cpuUsage);
			}
		}
		if(phpmstatistics.size() > 0 && dbmstatistics.size() > 0 ){
			memoryGraphic=true;
			listamem = new ArrayList<ServiceStatisticAdminBean.DbChartBean>();
			for (int i = phpmstatistics.size()-1; i>=0; i--){
				DbChartBean cpuUsage = new DbChartBean();
				cpuUsage.setName(phpmstatistics.get(i).getCreatedAt().toString().substring(0,10));
				cpuUsage.setV(phpmstatistics.get(i).getUsage());
				cpuUsage.setV1(dbmstatistics.get(i).getUsage());
				listamem.add(cpuUsage);
			}
		}
	}


	public void componentChange(ValueChangeEvent event) throws AbortProcessingException{
		//int selectedIndex = Integer.parseInt(event.getNewValue().toString()) - 1;
		HtmlSelectOneRadio radio = (HtmlSelectOneRadio)event.getComponent();  
		String label = (String)radio.getValue();
		if ("Storage".equals(label)){ 
			setDbSel(false);
		}
		else setDbSel(true);
	}

	public void setWorkers(List<Worker> workers) {
		this.workers = workers;
	}

	public List<Worker> getWorkers() {
		return workers;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}



	public void setBdComponent(Component bdComponent) {
		this.bdComponent = bdComponent;
	}



	public Component getBdComponent() {
		return bdComponent;
	}



	public void setStorageComponent(Component storageComponent) {
		this.storageComponent = storageComponent;
	}



	public Component getStorageComponent() {
		return storageComponent;
	}



	public void setAppComponent(Component appComponent) {
		this.appComponent = appComponent;
	}



	public Component getAppComponent() {
		return appComponent;
	}

	public List<DbChartBean> getLista() {
		return lista;
	}

	public void setLista(List<DbChartBean> lista) {
		this.lista = lista;
	}

	public void setDbSel(Boolean dbSel) {
		this.dbSel = dbSel;
	}

	public Boolean getDbSel() {
		return dbSel;
	}


	public void setListacpu(List<DbChartBean> listacpu) {
		this.listacpu = listacpu;
	}

	public List<DbChartBean> getListacpu() {
		return listacpu;
	}


	public List<DbChartBean> getListamem() {
		return listamem;
	}

	public void setListamem(List<DbChartBean> listamem) {
		this.listamem = listamem;
	}


	public void setSpaceGraphic(Boolean spaceGraphic) {
		this.spaceGraphic = spaceGraphic;
	}


	public Boolean getSpaceGraphic() {
		return spaceGraphic;
	}


	public void setCpuGraphic(Boolean cpuGraphic) {
		this.cpuGraphic = cpuGraphic;
	}


	public Boolean getCpuGraphic() {
		return cpuGraphic;
	}


	public void setMemoryGraphic(Boolean memoryGraphic) {
		this.memoryGraphic = memoryGraphic;
	}


	public Boolean getMemoryGraphic() {
		return memoryGraphic;
	}


	public void setGotService(Boolean gotService) {
		this.gotService = gotService;
	}


	public Boolean getGotService() {
		return gotService;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	public class DbChartBean implements Serializable {

		String name;
		long v;
		long v1;
		long v2;
		long v3;


		public DbChartBean() {
			name = new String();
			v=0;
			v1=0;
			v2=0;
			v3=0;
		}

		public String getName() {
			return name;
		}
		public void setName(String n) {
			this.name= n;
		}

		public long getV() {
			return v;
		}

		public void setV(long v) {
			this.v = v;
		}

		public long getV1() {
			return v1;
		}

		public void setV1(long v1) {
			this.v1 = v1;
		}

		public long getV2() {
			return v2;
		}

		public void setV2(long v2) {
			this.v2 = v2;
		}

		public long getV3() {
			return v3;
		}

		public void setV3(long v3) {
			this.v3 = v3;
		}

	}


}
