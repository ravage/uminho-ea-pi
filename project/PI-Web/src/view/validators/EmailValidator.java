package view.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public class EmailValidator implements Validator {

	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object arg2)
	throws ValidatorException {

		boolean isValid = true;
		String value = null;

		if ((arg0 == null) || (arg1 == null)) {
			throw new NullPointerException();
		}
		if (!(arg1 instanceof UIInput)) {
			return;
		}
		if (null == arg2) {
			return;
		}
		value = arg2.toString();
		int atIndex = value.indexOf('@');
		if (atIndex < 0) {
			isValid = false;
		}
		else if (value.lastIndexOf('.') < atIndex) {
			isValid = false;
		}
		if ( !isValid ) {
			throw new ValidatorException(new FacesMessage("Wrong email format"));
		}

	}

}
