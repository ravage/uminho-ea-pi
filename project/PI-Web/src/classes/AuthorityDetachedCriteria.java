/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class AuthorityDetachedCriteria extends AbstractORMDetachedCriteria {
	public final StringExpression authority;
	public final TimestampExpression createdAt;
	public final TimestampExpression updated_at;
	
	public AuthorityDetachedCriteria() {
		super(classes.Authority.class, classes.AuthorityCriteria.class);
		authority = new StringExpression("authority", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
		updated_at = new TimestampExpression("updated_at", this.getDetachedCriteria());
	}
	
	public AuthorityDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, classes.AuthorityCriteria.class);
		authority = new StringExpression("authority", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
		updated_at = new TimestampExpression("updated_at", this.getDetachedCriteria());
	}
	
	public UserDetachedCriteria createUserCriteria() {
		return new UserDetachedCriteria(createCriteria("ORM_User"));
	}
	
	public Authority uniqueAuthority(PersistentSession session) {
		return (Authority) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Authority[] listAuthority(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Authority[]) list.toArray(new Authority[list.size()]);
	}
}

