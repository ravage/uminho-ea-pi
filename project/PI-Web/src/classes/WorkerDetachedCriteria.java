/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class WorkerDetachedCriteria extends AbstractORMDetachedCriteria {
	public final LongExpression id;
	public final LongExpression quantity;
	public final LongExpression percentageLimit;
	public final TimestampExpression createdAt;
	public final TimestampExpression updated_at;
	public final StringExpression deploy_to;
	
	public WorkerDetachedCriteria() {
		super(classes.Worker.class, classes.WorkerCriteria.class);
		id = new LongExpression("id", this.getDetachedCriteria());
		quantity = new LongExpression("quantity", this.getDetachedCriteria());
		percentageLimit = new LongExpression("percentageLimit", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
		updated_at = new TimestampExpression("updated_at", this.getDetachedCriteria());
		deploy_to = new StringExpression("deploy_to", this.getDetachedCriteria());
	}
	
	public WorkerDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, classes.WorkerCriteria.class);
		id = new LongExpression("id", this.getDetachedCriteria());
		quantity = new LongExpression("quantity", this.getDetachedCriteria());
		percentageLimit = new LongExpression("percentageLimit", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
		updated_at = new TimestampExpression("updated_at", this.getDetachedCriteria());
		deploy_to = new StringExpression("deploy_to", this.getDetachedCriteria());
	}
	
	public ServiceDetachedCriteria createServiceCriteria() {
		return new ServiceDetachedCriteria(createCriteria("service"));
	}
	
	public ComponentDetachedCriteria createComponentCriteria() {
		return new ComponentDetachedCriteria(createCriteria("component"));
	}
	
	public StatisticDetachedCriteria createStatisticCriteria() {
		return new StatisticDetachedCriteria(createCriteria("ORM_Statistic"));
	}
	
	public ConfigurationDirectiveDetachedCriteria createConfigurationDirectiveCriteria() {
		return new ConfigurationDirectiveDetachedCriteria(createCriteria("ORM_ConfigurationDirective"));
	}
	
	public Worker uniqueWorker(PersistentSession session) {
		return (Worker) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Worker[] listWorker(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Worker[]) list.toArray(new Worker[list.size()]);
	}
}

