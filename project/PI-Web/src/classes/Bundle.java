/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

public class Bundle {
	public Bundle() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == classes.ORMConstants.KEY_BUNDLE_BUNDLECOMPONENT) {
			return ORM_bundleComponent;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private long id;
	
	private String name;
	
	private java.math.BigDecimal price;
	
	private short duration;
	
	private java.sql.Timestamp createdAt;
	
	private java.sql.Timestamp updated_at;
	
	private java.util.Set ORM_bundleComponent = new java.util.HashSet();
	
	private void setId(long value) {
		this.id = value;
	}
	
	public long getId() {
		return id;
	}
	
	public long getORMID() {
		return getId();
	}
	
	public void setName(String value) {
		this.name = value;
	}
	
	public String getName() {
		return name;
	}
	
	public void setPrice(java.math.BigDecimal value) {
		this.price = value;
	}
	
	public java.math.BigDecimal getPrice() {
		return price;
	}
	
	public void setDuration(short value) {
		this.duration = value;
	}
	
	public short getDuration() {
		return duration;
	}
	
	public void setCreatedAt(java.sql.Timestamp value) {
		this.createdAt = value;
	}
	
	public java.sql.Timestamp getCreatedAt() {
		return createdAt;
	}
	
	public void setUpdated_at(java.sql.Timestamp value) {
		this.updated_at = value;
	}
	
	public java.sql.Timestamp getUpdated_at() {
		return updated_at;
	}
	
	private void setORM_BundleComponent(java.util.Set value) {
		this.ORM_bundleComponent = value;
	}
	
	private java.util.Set getORM_BundleComponent() {
		return ORM_bundleComponent;
	}
	
	public final classes.BundleComponentSetCollection bundleComponent = new classes.BundleComponentSetCollection(this, _ormAdapter, classes.ORMConstants.KEY_BUNDLE_BUNDLECOMPONENT, classes.ORMConstants.KEY_BUNDLECOMPONENT_BUNDLE, classes.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
