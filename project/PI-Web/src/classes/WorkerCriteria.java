/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Criterion;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class WorkerCriteria extends AbstractORMCriteria {
	public final LongExpression id;
	public final LongExpression quantity;
	public final LongExpression percentageLimit;
	public final TimestampExpression createdAt;
	public final TimestampExpression updated_at;
	public final StringExpression deploy_to;
	
	public WorkerCriteria(Criteria criteria) {
		super(criteria);
		id = new LongExpression("id", this);
		quantity = new LongExpression("quantity", this);
		percentageLimit = new LongExpression("percentageLimit", this);
		createdAt = new TimestampExpression("createdAt", this);
		updated_at = new TimestampExpression("updated_at", this);
		deploy_to = new StringExpression("deploy_to", this);
	}
	
	public WorkerCriteria(PersistentSession session) {
		this(session.createCriteria(Worker.class));
	}
	
	public WorkerCriteria() throws PersistentException {
		this(orm.Hibernatev3PersistentManager.instance().getSession());
	}
	
	public ServiceCriteria createServiceCriteria() {
		return new ServiceCriteria(createCriteria("service"));
	}
	
	public ComponentCriteria createComponentCriteria() {
		return new ComponentCriteria(createCriteria("component"));
	}
	
	public StatisticCriteria createStatisticCriteria() {
		return new StatisticCriteria(createCriteria("ORM_Statistic"));
	}
	
	public ConfigurationDirectiveCriteria createConfigurationDirectiveCriteria() {
		return new ConfigurationDirectiveCriteria(createCriteria("ORM_ConfigurationDirective"));
	}
	
	public Worker uniqueWorker() {
		return (Worker) super.uniqueResult();
	}
	
	public Worker[] listWorker() {
		java.util.List list = super.list();
		return (Worker[]) list.toArray(new Worker[list.size()]);
	}

	@Override
	public Criteria createAlias(String arg0, String arg1, int arg2,
			Criterion arg3) throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Criteria createCriteria(String arg0, String arg1, int arg2,
			Criterion arg3) throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isReadOnly() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isReadOnlyInitialized() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Criteria setReadOnly(boolean arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}

