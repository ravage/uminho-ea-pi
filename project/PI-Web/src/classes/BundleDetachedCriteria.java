/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class BundleDetachedCriteria extends AbstractORMDetachedCriteria {
	public final LongExpression id;
	public final StringExpression name;
	public final BigDecimalExpression price;
	public final ShortExpression duration;
	public final TimestampExpression createdAt;
	public final TimestampExpression updated_at;
	
	public BundleDetachedCriteria() {
		super(classes.Bundle.class, classes.BundleCriteria.class);
		id = new LongExpression("id", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		price = new BigDecimalExpression("price", this.getDetachedCriteria());
		duration = new ShortExpression("duration", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
		updated_at = new TimestampExpression("updated_at", this.getDetachedCriteria());
	}
	
	public BundleDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, classes.BundleCriteria.class);
		id = new LongExpression("id", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		price = new BigDecimalExpression("price", this.getDetachedCriteria());
		duration = new ShortExpression("duration", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
		updated_at = new TimestampExpression("updated_at", this.getDetachedCriteria());
	}
	
	public BundleComponentDetachedCriteria createBundleComponentCriteria() {
		return new BundleComponentDetachedCriteria(createCriteria("ORM_BundleComponent"));
	}
	
	public Bundle uniqueBundle(PersistentSession session) {
		return (Bundle) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Bundle[] listBundle(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Bundle[]) list.toArray(new Bundle[list.size()]);
	}
}

