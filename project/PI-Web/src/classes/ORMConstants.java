/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

public interface ORMConstants extends org.orm.util.ORMBaseConstants {
	final int KEY_AGENT_COMPONENT = -462827485;
	
	final int KEY_APPLICATION_SERVICE = 1907944102;
	
	final int KEY_AUTHORITY_USER = -1366983833;
	
	final int KEY_BUNDLECOMPONENT_BUNDLE = 1528874694;
	
	final int KEY_BUNDLECOMPONENT_COMPONENT = -1342394311;
	
	final int KEY_BUNDLE_BUNDLECOMPONENT = 934243582;
	
	final int KEY_COMPONENT_AGENT = -1484191453;
	
	final int KEY_COMPONENT_BUNDLECOMPONENT = -1655523527;
	
	final int KEY_COMPONENT_CONFIGURATION = -770425580;
	
	final int KEY_COMPONENT_DIRECTIVE = 1744146285;
	
	final int KEY_COMPONENT_WORKER = 1872318752;
	
	final int KEY_CONFIGURATIONDIRECTIVE_WORKER = -588997020;
	
	final int KEY_CONFIGURATION_COMPONENT = 299051220;
	
	final int KEY_DIRECTIVE_COMPONENT = -1253240595;
	
	final int KEY_SERVICE_APPLICATION = -1515025754;
	
	final int KEY_SERVICE_USER = 524426037;
	
	final int KEY_SERVICE_WORKER = 1516195944;
	
	final int KEY_STATISTIC_WORKER = -1393032851;
	
	final int KEY_USER_AUTHORITY = 2040707055;
	
	final int KEY_USER_SERVICE = -620842399;
	
	final int KEY_WORKER_COMPONENT = -1896158820;
	
	final int KEY_WORKER_CONFIGURATIONDIRECTIVE = 1660147130;
	
	final int KEY_WORKER_SERVICE = 1076375316;
	
	final int KEY_WORKER_STATISTIC = 1717454319;
	
}
