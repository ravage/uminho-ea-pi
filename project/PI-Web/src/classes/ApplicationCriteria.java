/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Criterion;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ApplicationCriteria extends AbstractORMCriteria {
	public final LongExpression id;
	public final StringExpression name;
	public final StringExpression fileLocation;
	public final StringExpression url;
	public final TimestampExpression createdAt;
	public final TimestampExpression updated_at;
	
	public ApplicationCriteria(Criteria criteria) {
		super(criteria);
		id = new LongExpression("id", this);
		name = new StringExpression("name", this);
		fileLocation = new StringExpression("fileLocation", this);
		url = new StringExpression("url", this);
		createdAt = new TimestampExpression("createdAt", this);
		updated_at = new TimestampExpression("updated_at", this);
	}
	
	public ApplicationCriteria(PersistentSession session) {
		this(session.createCriteria(Application.class));
	}
	
	public ApplicationCriteria() throws PersistentException {
		this(orm.Hibernatev3PersistentManager.instance().getSession());
	}
	
	public ServiceCriteria createServiceCriteria() {
		return new ServiceCriteria(createCriteria("service"));
	}
	
	public Application uniqueApplication() {
		return (Application) super.uniqueResult();
	}
	
	public Application[] listApplication() {
		java.util.List list = super.list();
		return (Application[]) list.toArray(new Application[list.size()]);
	}

	@Override
	public Criteria createAlias(String arg0, String arg1, int arg2,
			Criterion arg3) throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Criteria createCriteria(String arg0, String arg1, int arg2,
			Criterion arg3) throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isReadOnly() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isReadOnlyInitialized() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Criteria setReadOnly(boolean arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}

