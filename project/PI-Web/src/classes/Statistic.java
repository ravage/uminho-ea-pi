/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

public class Statistic {
	public Statistic() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == classes.ORMConstants.KEY_STATISTIC_WORKER) {
			this.worker = (classes.Worker) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private long id;
	
	private String name;
	
	private java.util.Date statisticsDate;
	
	private long usage;
	
	private String observations;
	
	private classes.Worker worker;
	
	private java.sql.Timestamp createdAt;
	
	private java.sql.Timestamp updated_at;
	
	private void setId(long value) {
		this.id = value;
	}
	
	public long getId() {
		return id;
	}
	
	public long getORMID() {
		return getId();
	}
	
	public void setName(String value) {
		this.name = value;
	}
	
	public String getName() {
		return name;
	}
	
	public void setStatisticsDate(java.util.Date value) {
		this.statisticsDate = value;
	}
	
	public java.util.Date getStatisticsDate() {
		return statisticsDate;
	}
	
	public void setUsage(long value) {
		this.usage = value;
	}
	
	public long getUsage() {
		return usage;
	}
	
	public void setObservations(String value) {
		this.observations = value;
	}
	
	public String getObservations() {
		return observations;
	}
	
	public void setCreatedAt(java.sql.Timestamp value) {
		this.createdAt = value;
	}
	
	public java.sql.Timestamp getCreatedAt() {
		return createdAt;
	}
	
	public void setUpdated_at(java.sql.Timestamp value) {
		this.updated_at = value;
	}
	
	public java.sql.Timestamp getUpdated_at() {
		return updated_at;
	}
	
	public void setWorker(classes.Worker value) {
		if (worker != null) {
			worker.statistic.remove(this);
		}
		if (value != null) {
			value.statistic.add(this);
		}
	}
	
	public classes.Worker getWorker() {
		return worker;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Worker(classes.Worker value) {
		this.worker = value;
	}
	
	private classes.Worker getORM_Worker() {
		return worker;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
