/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

public class Component {
	public Component() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == classes.ORMConstants.KEY_COMPONENT_WORKER) {
			return ORM_worker;
		}
		else if (key == classes.ORMConstants.KEY_COMPONENT_BUNDLECOMPONENT) {
			return ORM_bundleComponent;
		}
		else if (key == classes.ORMConstants.KEY_COMPONENT_CONFIGURATION) {
			return ORM_configuration;
		}
		else if (key == classes.ORMConstants.KEY_COMPONENT_AGENT) {
			return ORM_agent;
		}
		else if (key == classes.ORMConstants.KEY_COMPONENT_DIRECTIVE) {
			return ORM_directive;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private long id;
	
	private String name;
	
	private String description;
	
	private java.math.BigDecimal price;
	
	private Boolean selectable;
	
	private String key;
	
	private java.sql.Timestamp createdAt;
	
	private java.sql.Timestamp updated_at;
	
	private java.util.Set ORM_worker = new java.util.HashSet();
	
	private java.util.Set ORM_bundleComponent = new java.util.HashSet();
	
	private java.util.Set ORM_configuration = new java.util.HashSet();
	
	private java.util.Set ORM_agent = new java.util.HashSet();
	
	private java.util.Set ORM_directive = new java.util.HashSet();
	
	private void setId(long value) {
		this.id = value;
	}
	
	public long getId() {
		return id;
	}
	
	public long getORMID() {
		return getId();
	}
	
	public void setDescription(String value) {
		this.description = value;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setPrice(java.math.BigDecimal value) {
		this.price = value;
	}
	
	public java.math.BigDecimal getPrice() {
		return price;
	}
	
	public void setSelectable(boolean value) {
		setSelectable(new Boolean(value));
	}
	
	public void setSelectable(Boolean value) {
		this.selectable = value;
	}
	
	public Boolean getSelectable() {
		return selectable;
	}
	
	public void setCreatedAt(java.sql.Timestamp value) {
		this.createdAt = value;
	}
	
	public java.sql.Timestamp getCreatedAt() {
		return createdAt;
	}
	
	public void setUpdated_at(java.sql.Timestamp value) {
		this.updated_at = value;
	}
	
	public java.sql.Timestamp getUpdated_at() {
		return updated_at;
	}
	
	public void setKey(String value) {
		this.key = value;
	}
	
	public String getKey() {
		return key;
	}
	
	public void setName(String value) {
		this.name = value;
	}
	
	public String getName() {
		return name;
	}
	
	private void setORM_Worker(java.util.Set value) {
		this.ORM_worker = value;
	}
	
	private java.util.Set getORM_Worker() {
		return ORM_worker;
	}
	
	public final classes.WorkerSetCollection worker = new classes.WorkerSetCollection(this, _ormAdapter, classes.ORMConstants.KEY_COMPONENT_WORKER, classes.ORMConstants.KEY_WORKER_COMPONENT, classes.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_BundleComponent(java.util.Set value) {
		this.ORM_bundleComponent = value;
	}
	
	private java.util.Set getORM_BundleComponent() {
		return ORM_bundleComponent;
	}
	
	public final classes.BundleComponentSetCollection bundleComponent = new classes.BundleComponentSetCollection(this, _ormAdapter, classes.ORMConstants.KEY_COMPONENT_BUNDLECOMPONENT, classes.ORMConstants.KEY_BUNDLECOMPONENT_COMPONENT, classes.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Configuration(java.util.Set value) {
		this.ORM_configuration = value;
	}
	
	private java.util.Set getORM_Configuration() {
		return ORM_configuration;
	}
	
	public final classes.ConfigurationSetCollection configuration = new classes.ConfigurationSetCollection(this, _ormAdapter, classes.ORMConstants.KEY_COMPONENT_CONFIGURATION, classes.ORMConstants.KEY_CONFIGURATION_COMPONENT, classes.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Agent(java.util.Set value) {
		this.ORM_agent = value;
	}
	
	private java.util.Set getORM_Agent() {
		return ORM_agent;
	}
	
	public final classes.AgentSetCollection agent = new classes.AgentSetCollection(this, _ormAdapter, classes.ORMConstants.KEY_COMPONENT_AGENT, classes.ORMConstants.KEY_AGENT_COMPONENT, classes.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Directive(java.util.Set value) {
		this.ORM_directive = value;
	}
	
	private java.util.Set getORM_Directive() {
		return ORM_directive;
	}
	
	public final classes.DirectiveSetCollection directive = new classes.DirectiveSetCollection(this, _ormAdapter, classes.ORMConstants.KEY_COMPONENT_DIRECTIVE, classes.ORMConstants.KEY_DIRECTIVE_COMPONENT, classes.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
