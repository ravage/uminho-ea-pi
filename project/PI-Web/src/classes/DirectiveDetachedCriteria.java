/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DirectiveDetachedCriteria extends AbstractORMDetachedCriteria {
	public final LongExpression id;
	public final StringExpression name;
	public final StringExpression value;
	public final TimestampExpression createdAt;
	public final TimestampExpression updated_at;
	
	public DirectiveDetachedCriteria() {
		super(classes.Directive.class, classes.DirectiveCriteria.class);
		id = new LongExpression("id", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		value = new StringExpression("value", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
		updated_at = new TimestampExpression("updated_at", this.getDetachedCriteria());
	}
	
	public DirectiveDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, classes.DirectiveCriteria.class);
		id = new LongExpression("id", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		value = new StringExpression("value", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
		updated_at = new TimestampExpression("updated_at", this.getDetachedCriteria());
	}
	
	public ComponentDetachedCriteria createComponentCriteria() {
		return new ComponentDetachedCriteria(createCriteria("component"));
	}
	
	public Directive uniqueDirective(PersistentSession session) {
		return (Directive) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Directive[] listDirective(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Directive[]) list.toArray(new Directive[list.size()]);
	}
}

