/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Criterion;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class AlertServiceCriteria extends AbstractORMCriteria {
	public final LongExpression id;
	public final StringExpression name;
	public final StringExpression description;
	public final TimestampExpression createdAt;
	public final TimestampExpression updated_at;
	
	public AlertServiceCriteria(Criteria criteria) {
		super(criteria);
		id = new LongExpression("id", this);
		name = new StringExpression("name", this);
		description = new StringExpression("description", this);
		createdAt = new TimestampExpression("createdAt", this);
		updated_at = new TimestampExpression("updated_at", this);
	}
	
	public AlertServiceCriteria(PersistentSession session) {
		this(session.createCriteria(AlertService.class));
	}
	
	public AlertServiceCriteria() throws PersistentException {
		this(orm.Hibernatev3PersistentManager.instance().getSession());
	}
	
	public AlertService uniqueAlertService() {
		return (AlertService) super.uniqueResult();
	}
	
	public AlertService[] listAlertService() {
		java.util.List list = super.list();
		return (AlertService[]) list.toArray(new AlertService[list.size()]);
	}

	@Override
	public Criteria createAlias(String arg0, String arg1, int arg2,
			Criterion arg3) throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Criteria createCriteria(String arg0, String arg1, int arg2,
			Criterion arg3) throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isReadOnly() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isReadOnlyInitialized() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Criteria setReadOnly(boolean arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}

