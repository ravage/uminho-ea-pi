/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class AgentDetachedCriteria extends AbstractORMDetachedCriteria {
	public final LongExpression id;
	public final StringExpression address;
	public final StringExpression status;
	public final StringExpression key;
	public final TimestampExpression lastCheck;
	public final TimestampExpression createdAt;
	public final TimestampExpression updated_at;
	
	public AgentDetachedCriteria() {
		super(classes.Agent.class, classes.AgentCriteria.class);
		id = new LongExpression("id", this.getDetachedCriteria());
		address = new StringExpression("address", this.getDetachedCriteria());
		status = new StringExpression("status", this.getDetachedCriteria());
		key = new StringExpression("key", this.getDetachedCriteria());
		lastCheck = new TimestampExpression("lastCheck", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
		updated_at = new TimestampExpression("updated_at", this.getDetachedCriteria());
	}
	
	public AgentDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, classes.AgentCriteria.class);
		id = new LongExpression("id", this.getDetachedCriteria());
		address = new StringExpression("address", this.getDetachedCriteria());
		status = new StringExpression("status", this.getDetachedCriteria());
		key = new StringExpression("key", this.getDetachedCriteria());
		lastCheck = new TimestampExpression("lastCheck", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
		updated_at = new TimestampExpression("updated_at", this.getDetachedCriteria());
	}
	
	public ComponentDetachedCriteria createComponentCriteria() {
		return new ComponentDetachedCriteria(createCriteria("component"));
	}
	
	public Agent uniqueAgent(PersistentSession session) {
		return (Agent) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Agent[] listAgent(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Agent[]) list.toArray(new Agent[list.size()]);
	}
}

