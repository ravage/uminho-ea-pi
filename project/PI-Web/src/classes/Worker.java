/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

public class Worker {
	public Worker() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == classes.ORMConstants.KEY_WORKER_STATISTIC) {
			return ORM_statistic;
		}
		else if (key == classes.ORMConstants.KEY_WORKER_CONFIGURATIONDIRECTIVE) {
			return ORM_configurationDirective;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == classes.ORMConstants.KEY_WORKER_SERVICE) {
			this.service = (classes.Service) owner;
		}
		
		else if (key == classes.ORMConstants.KEY_WORKER_COMPONENT) {
			this.component = (classes.Component) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private long id;
	
	private classes.Service service;
	
	private classes.Component component;
	
	private long quantity;
	
	private long percentageLimit;
	
	private java.sql.Timestamp createdAt;
	
	private java.sql.Timestamp updated_at;
	
	private String deploy_to;
	
	private java.util.Set ORM_statistic = new java.util.HashSet();
	
	private java.util.Set ORM_configurationDirective = new java.util.HashSet();
	
	private void setId(long value) {
		this.id = value;
	}
	
	public long getId() {
		return id;
	}
	
	public long getORMID() {
		return getId();
	}
	
	public void setQuantity(long value) {
		this.quantity = value;
	}
	
	public long getQuantity() {
		return quantity;
	}
	
	public void setPercentageLimit(long value) {
		this.percentageLimit = value;
	}
	
	public long getPercentageLimit() {
		return percentageLimit;
	}
	
	public void setCreatedAt(java.sql.Timestamp value) {
		this.createdAt = value;
	}
	
	public java.sql.Timestamp getCreatedAt() {
		return createdAt;
	}
	
	public void setUpdated_at(java.sql.Timestamp value) {
		this.updated_at = value;
	}
	
	public java.sql.Timestamp getUpdated_at() {
		return updated_at;
	}
	
	public void setDeploy_to(String value) {
		this.deploy_to = value;
	}
	
	public String getDeploy_to() {
		return deploy_to;
	}
	
	public void setService(classes.Service value) {
		if (service != null) {
			service.worker.remove(this);
		}
		if (value != null) {
			value.worker.add(this);
		}
	}
	
	public classes.Service getService() {
		return service;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Service(classes.Service value) {
		this.service = value;
	}
	
	private classes.Service getORM_Service() {
		return service;
	}
	
	public void setComponent(classes.Component value) {
		if (component != null) {
			component.worker.remove(this);
		}
		if (value != null) {
			value.worker.add(this);
		}
	}
	
	public classes.Component getComponent() {
		return component;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Component(classes.Component value) {
		this.component = value;
	}
	
	private classes.Component getORM_Component() {
		return component;
	}
	
	private void setORM_Statistic(java.util.Set value) {
		this.ORM_statistic = value;
	}
	
	private java.util.Set getORM_Statistic() {
		return ORM_statistic;
	}
	
	public final classes.StatisticSetCollection statistic = new classes.StatisticSetCollection(this, _ormAdapter, classes.ORMConstants.KEY_WORKER_STATISTIC, classes.ORMConstants.KEY_STATISTIC_WORKER, classes.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_ConfigurationDirective(java.util.Set value) {
		this.ORM_configurationDirective = value;
	}
	
	private java.util.Set getORM_ConfigurationDirective() {
		return ORM_configurationDirective;
	}
	
	public final classes.ConfigurationDirectiveSetCollection configurationDirective = new classes.ConfigurationDirectiveSetCollection(this, _ormAdapter, classes.ORMConstants.KEY_WORKER_CONFIGURATIONDIRECTIVE, classes.ORMConstants.KEY_CONFIGURATIONDIRECTIVE_WORKER, classes.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
