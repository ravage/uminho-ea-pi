/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ApplicationDetachedCriteria extends AbstractORMDetachedCriteria {
	public final LongExpression id;
	public final StringExpression name;
	public final StringExpression fileLocation;
	public final StringExpression url;
	public final TimestampExpression createdAt;
	public final TimestampExpression updated_at;
	
	public ApplicationDetachedCriteria() {
		super(classes.Application.class, classes.ApplicationCriteria.class);
		id = new LongExpression("id", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		fileLocation = new StringExpression("fileLocation", this.getDetachedCriteria());
		url = new StringExpression("url", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
		updated_at = new TimestampExpression("updated_at", this.getDetachedCriteria());
	}
	
	public ApplicationDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, classes.ApplicationCriteria.class);
		id = new LongExpression("id", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		fileLocation = new StringExpression("fileLocation", this.getDetachedCriteria());
		url = new StringExpression("url", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
		updated_at = new TimestampExpression("updated_at", this.getDetachedCriteria());
	}
	
	public ServiceDetachedCriteria createServiceCriteria() {
		return new ServiceDetachedCriteria(createCriteria("service"));
	}
	
	public Application uniqueApplication(PersistentSession session) {
		return (Application) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Application[] listApplication(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Application[]) list.toArray(new Application[list.size()]);
	}
}

