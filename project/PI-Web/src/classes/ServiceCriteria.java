/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Criterion;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ServiceCriteria extends AbstractORMCriteria {
	public final LongExpression id;
	public final DateExpression initDate;
	public final ShortExpression duration;
	public final StringExpression name;
	public final StringExpression description;
	public final BigDecimalExpression price;
	public final TimestampExpression createdAt;
	public final TimestampExpression updated_at;
	
	public ServiceCriteria(Criteria criteria) {
		super(criteria);
		id = new LongExpression("id", this);
		initDate = new DateExpression("initDate", this);
		duration = new ShortExpression("duration", this);
		name = new StringExpression("name", this);
		description = new StringExpression("description", this);
		price = new BigDecimalExpression("price", this);
		createdAt = new TimestampExpression("createdAt", this);
		updated_at = new TimestampExpression("updated_at", this);
	}
	
	public ServiceCriteria(PersistentSession session) {
		this(session.createCriteria(Service.class));
	}
	
	public ServiceCriteria() throws PersistentException {
		this(orm.Hibernatev3PersistentManager.instance().getSession());
	}
	
	public UserCriteria createUserCriteria() {
		return new UserCriteria(createCriteria("user"));
	}
	
	public ApplicationCriteria createApplicationCriteria() {
		return new ApplicationCriteria(createCriteria("ORM_Application"));
	}
	
	public WorkerCriteria createWorkerCriteria() {
		return new WorkerCriteria(createCriteria("ORM_Worker"));
	}
	
	public Service uniqueService() {
		return (Service) super.uniqueResult();
	}
	
	public Service[] listService() {
		java.util.List list = super.list();
		return (Service[]) list.toArray(new Service[list.size()]);
	}

	@Override
	public Criteria createAlias(String arg0, String arg1, int arg2,
			Criterion arg3) throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Criteria createCriteria(String arg0, String arg1, int arg2,
			Criterion arg3) throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isReadOnly() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isReadOnlyInitialized() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Criteria setReadOnly(boolean arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}

