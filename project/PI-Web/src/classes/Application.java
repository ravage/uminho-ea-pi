/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

public class Application {
	public Application() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == classes.ORMConstants.KEY_APPLICATION_SERVICE) {
			this.service = (classes.Service) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private long id;
	
	private String name;
	
	private String fileLocation;
	
	private String url;
	
	private classes.Service service;
	
	private java.sql.Timestamp createdAt;
	
	private java.sql.Timestamp updated_at;
	
	private void setId(long value) {
		this.id = value;
	}
	
	public long getId() {
		return id;
	}
	
	public long getORMID() {
		return getId();
	}
	
	public void setName(String value) {
		this.name = value;
	}
	
	public String getName() {
		return name;
	}
	
	public void setFileLocation(String value) {
		this.fileLocation = value;
	}
	
	public String getFileLocation() {
		return fileLocation;
	}
	
	public void setUrl(String value) {
		this.url = value;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setCreatedAt(java.sql.Timestamp value) {
		this.createdAt = value;
	}
	
	public java.sql.Timestamp getCreatedAt() {
		return createdAt;
	}
	
	public void setUpdated_at(java.sql.Timestamp value) {
		this.updated_at = value;
	}
	
	public java.sql.Timestamp getUpdated_at() {
		return updated_at;
	}
	
	public void setService(classes.Service value) {
		if (service != null) {
			service.application.remove(this);
		}
		if (value != null) {
			value.application.add(this);
		}
	}
	
	public classes.Service getService() {
		return service;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Service(classes.Service value) {
		this.service = value;
	}
	
	private classes.Service getORM_Service() {
		return service;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
