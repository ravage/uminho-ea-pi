/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

public class BundleComponent {
	public BundleComponent() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == classes.ORMConstants.KEY_BUNDLECOMPONENT_BUNDLE) {
			this.bundle = (classes.Bundle) owner;
		}
		
		else if (key == classes.ORMConstants.KEY_BUNDLECOMPONENT_COMPONENT) {
			this.component = (classes.Component) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private long id;
	
	private classes.Bundle bundle;
	
	private classes.Component component;
	
	private Integer quantity;
	
	private java.sql.Timestamp createdAt;
	
	private java.sql.Timestamp updated_at;
	
	private void setId(long value) {
		this.id = value;
	}
	
	public long getId() {
		return id;
	}
	
	public long getORMID() {
		return getId();
	}
	
	public void setQuantity(int value) {
		setQuantity(new Integer(value));
	}
	
	public void setQuantity(Integer value) {
		this.quantity = value;
	}
	
	public Integer getQuantity() {
		return quantity;
	}
	
	public void setCreatedAt(java.sql.Timestamp value) {
		this.createdAt = value;
	}
	
	public java.sql.Timestamp getCreatedAt() {
		return createdAt;
	}
	
	public void setUpdated_at(java.sql.Timestamp value) {
		this.updated_at = value;
	}
	
	public java.sql.Timestamp getUpdated_at() {
		return updated_at;
	}
	
	public void setBundle(classes.Bundle value) {
		if (bundle != null) {
			bundle.bundleComponent.remove(this);
		}
		if (value != null) {
			value.bundleComponent.add(this);
		}
	}
	
	public classes.Bundle getBundle() {
		return bundle;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Bundle(classes.Bundle value) {
		this.bundle = value;
	}
	
	private classes.Bundle getORM_Bundle() {
		return bundle;
	}
	
	public void setComponent(classes.Component value) {
		if (component != null) {
			component.bundleComponent.remove(this);
		}
		if (value != null) {
			value.bundleComponent.add(this);
		}
	}
	
	public classes.Component getComponent() {
		return component;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Component(classes.Component value) {
		this.component = value;
	}
	
	private classes.Component getORM_Component() {
		return component;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
