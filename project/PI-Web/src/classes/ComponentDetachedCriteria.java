/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ComponentDetachedCriteria extends AbstractORMDetachedCriteria {
	public final LongExpression id;
	public final StringExpression name;
	public final StringExpression description;
	public final BigDecimalExpression price;
	public final BooleanExpression selectable;
	public final StringExpression key;
	public final TimestampExpression createdAt;
	public final TimestampExpression updated_at;
	
	public ComponentDetachedCriteria() {
		super(classes.Component.class, classes.ComponentCriteria.class);
		id = new LongExpression("id", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		description = new StringExpression("description", this.getDetachedCriteria());
		price = new BigDecimalExpression("price", this.getDetachedCriteria());
		selectable = new BooleanExpression("selectable", this.getDetachedCriteria());
		key = new StringExpression("key", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
		updated_at = new TimestampExpression("updated_at", this.getDetachedCriteria());
	}
	
	public ComponentDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, classes.ComponentCriteria.class);
		id = new LongExpression("id", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		description = new StringExpression("description", this.getDetachedCriteria());
		price = new BigDecimalExpression("price", this.getDetachedCriteria());
		selectable = new BooleanExpression("selectable", this.getDetachedCriteria());
		key = new StringExpression("key", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
		updated_at = new TimestampExpression("updated_at", this.getDetachedCriteria());
	}
	
	public WorkerDetachedCriteria createWorkerCriteria() {
		return new WorkerDetachedCriteria(createCriteria("ORM_Worker"));
	}
	
	public BundleComponentDetachedCriteria createBundleComponentCriteria() {
		return new BundleComponentDetachedCriteria(createCriteria("ORM_BundleComponent"));
	}
	
	public ConfigurationDetachedCriteria createConfigurationCriteria() {
		return new ConfigurationDetachedCriteria(createCriteria("ORM_Configuration"));
	}
	
	public AgentDetachedCriteria createAgentCriteria() {
		return new AgentDetachedCriteria(createCriteria("ORM_Agent"));
	}
	
	public DirectiveDetachedCriteria createDirectiveCriteria() {
		return new DirectiveDetachedCriteria(createCriteria("ORM_Directive"));
	}
	
	public Component uniqueComponent(PersistentSession session) {
		return (Component) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Component[] listComponent(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Component[]) list.toArray(new Component[list.size()]);
	}
}

