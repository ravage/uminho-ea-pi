/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

public class AlertService {
	public AlertService() {
	}
	
	private long id;
	
	private String name;
	
	private String description;
	
	private java.sql.Timestamp createdAt;
	
	private java.sql.Timestamp updated_at;
	
	private void setId(long value) {
		this.id = value;
	}
	
	public long getId() {
		return id;
	}
	
	public long getORMID() {
		return getId();
	}
	
	public void setName(String value) {
		this.name = value;
	}
	
	public String getName() {
		return name;
	}
	
	public void setDescription(String value) {
		this.description = value;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setCreatedAt(java.sql.Timestamp value) {
		this.createdAt = value;
	}
	
	public java.sql.Timestamp getCreatedAt() {
		return createdAt;
	}
	
	public void setUpdated_at(java.sql.Timestamp value) {
		this.updated_at = value;
	}
	
	public java.sql.Timestamp getUpdated_at() {
		return updated_at;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
