/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import java.io.Serializable;
public class Authority implements Serializable {
	public Authority() {
	}
	
	public boolean equals(Object aObj) {
		if (aObj == this)
			return true;
		if (!(aObj instanceof Authority))
			return false;
		Authority authority = (Authority)aObj;
		if (getUser() == null) {
			if (authority.getUser() != null)
				return false;
		}
		else if (!getUser().equals(authority.getUser()))
			return false;
		if ((getAuthority() != null && !getAuthority().equals(authority.getAuthority())) || (getAuthority() == null && authority.getAuthority() != null))
			return false;
		return true;
	}
	
	public int hashCode() {
		int hashcode = 0;
		if (getUser() != null) {
			hashcode = hashcode + (int) getUser().getORMID();
		}
		hashcode = hashcode + (getAuthority() == null ? 0 : getAuthority().hashCode());
		return hashcode;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == classes.ORMConstants.KEY_AUTHORITY_USER) {
			this.user = (classes.User) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private classes.User user;
	
	private long userId;
	
	public void setUserId(long value) {
		this.userId = value;
	}
	
	public long getUserId() {
		return userId;
	}
	
	private String authority;
	
	private java.sql.Timestamp createdAt;
	
	private java.sql.Timestamp updated_at;
	
	public void setAuthority(String value) {
		this.authority = value;
	}
	
	public String getAuthority() {
		return authority;
	}
	
	public void setCreatedAt(java.sql.Timestamp value) {
		this.createdAt = value;
	}
	
	public java.sql.Timestamp getCreatedAt() {
		return createdAt;
	}
	
	public void setUpdated_at(java.sql.Timestamp value) {
		this.updated_at = value;
	}
	
	public java.sql.Timestamp getUpdated_at() {
		return updated_at;
	}
	
	public void setUser(classes.User value) {
		if (user != null) {
			user.authority.remove(this);
		}
		if (value != null) {
			value.authority.add(this);
		}
	}
	
	public classes.User getUser() {
		return user;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_User(classes.User value) {
		this.user = value;
	}
	
	private classes.User getORM_User() {
		return user;
	}
	
	private long user_id;
	
	public String toString() {
		return String.valueOf(((getUser() == null) ? "" : String.valueOf(getUser().getORMID())) + " " + getAuthority());
	}
	
}
