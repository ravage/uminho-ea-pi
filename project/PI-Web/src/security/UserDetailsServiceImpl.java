package security;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import classes.Authority;

import service.UserService;

public class UserDetailsServiceImpl implements UserDetailsService{   
	
	protected final Log logger = LogFactory.getLog(this.getClass());

	private UserService userService;
	public UserService getUserService() {
		return userService;
	}
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username)
	throws UsernameNotFoundException, DataAccessException {

		classes.User user = userService.getUser(username);
		if (user == null || !user.getActive()){
			throw new UsernameNotFoundException("user not found");
		}else{
			

			this.logger.info("Retrieving UserDetails for user '"+username+"' ->"+user.getPassword());

			Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			for (Authority a : user.authority.toArray()){
				authorities.add(new GrantedAuthorityImpl(a.getAuthority()));
			}
			User userDetails = new User(username, user.getPassword(), true, true, true, true, authorities); 

			return userDetails;
		}
	}
}
