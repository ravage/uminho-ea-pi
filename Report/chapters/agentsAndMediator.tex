\section{\emph{Agentes e Mediador}}
De forma a gerir todo o processo de colocação e configuração das aplicações é necessário articular um conjunto de quatro máquinas virtuais, cada uma com o seu serviço e leque de configurações. Estas configurações devem estar de acordo com as escolhas efetuadas pelo cliente e suportar modificações \emph{on-demand}.

Para cumprir com o objetivo supracitado optou-se por desenvolver um conjunto de agentes em \emph{Java} e delegar tarefas específicas a cada um deles, algo idêntico aos agentes \ac{SNMP}. Cada agente é completamente contido sem necessitar de um servidor aplicacional, o contentor e servidor \ac{HTTP} são proporcionados pelo \emph{Grizzly} e incluídos no próprio agente.

A nível de tarefas a executar existe o conceito de unidade de trabalho que cada agente executa de forma assíncrona e notifica a sua conclusão através de um \emph{callback}. Com este processo as unidades de trabalho são executadas no seu próprio \emph{thread} gerido numa \emph{cached pool} sem impedir que o agente receba mais pedidos, com este processo o agente pode executar e receber novos pedidos por parte do mediador mantendo as tarefas em execução.

De forma a manter os agentes o mais \emph{dumb} possível, todas as tarefas fora do âmbito de configuração foram retiradas, assim apenas recebe um pacote de configurações a executar. O agente mediador trata de toda a comunicação entre a base de dados, aplicação e agentes sob o seu comando. Seguindo este padrão mediador/agente alcança-se desacoplação de todo o processo, tornando modular a criação e modificação de agentes.

A comunicação realizada entre os três pontos principais, aplicação/mediador/agente é realizada através de \emph{web services} integrados com \emph{Jersey}. A troca de mensagens é efetuada de forma transparente com \ac{JSON} ou \ac{XML}, a serialização está a cargo de \emph{JAXB}, biblioteca já integrada no \emph{Jersey}.

\subsection{Mediador}
O mediador trata da comunicação com os agentes de acordo com os pedidos da aplicação \emph{web} e recebe resultados por parte dos agentes, bem como dados pertinentes como estatísticas de utilização. Como já descrito, toda esta comunicação é realizada através de \emph{web services}.

É deste componente que partem as ordens para as unidades de trabalho dos agentes. As diretivas de configuração são compiladas através das opções selecionadas pelos clientes durante o processo de criação de conta e entregues aos agentes responsáveis. O acesso ao repositório de dados está implementado com \emph{Hibernate} e as respetivas entidades anotadas com \emph{JPA}, uma vez que é utilizado o \emph{EntityManager} e não o \emph{SessionFactory} do \emph{Hibernate}.

Assim como os agentes básicos, tudo no mediador é assíncrono utilizando o mesmo sistema de \emph{callbacks} para não bloquear novos pedidos enquanto trata de toda a coordenação com os agentes e prepara as configurações para entrega.

\begin{figure}[ht]
  \centering
  \includegraphics[scale=0.5]{images/agents}
  \caption{Mediador / Agentes}
  \label{fig:mediator_agent}
\end{figure}

\subsection{Agentes} % (fold)
\label{sub:Agentes}
Implementados com o intuito de realizar tarefas específicas, cada agente integra em si mesmo um mini servidor \ac{HTTP} (\emph{Grizzly}) com um \emph{web service} como forma de receção de unidades de trabalho. Cumpre com um funcionamento assíncrono para não bloquear tarefas e responde ao mediador o estado das mesmas.

Durante o arranque o agente notifica o mediador da sua disponibilidade e tipo de trabalho capaz de realizar, mantém o mediador informado realizando \emph{check-in} em intervalos de trinta segundos. O intuito destas notificações é fazer saber ao mediador com que agentes pode contar, onde se encontram (ip:porta) e que tipo de tarefas realizam. Permite também manter um estado, \emph{Up, Down ou Maintenance}.

Os agentes encontram-se distribuídos pelas \emph{VMs} (Figura~\ref{fig:mediator_agent}) de acordo com as necessidades de configuração das mesmas. Ativam o processo de configuração assim que são ordenados e ficam em estado \emph{idle} notificando apenas o seu estado. Esta articulação entre agentes e mediador evita \emph{pooling} para determinar eventos, mantendo a arquitetura "silenciosa" até existirem tarefas a executar.

O processo de configuração efetuado pelos agentes passa pela receção de um pacote com uma coleção de dados no formato \emph{chave: valor}, que por sua vez é passada por uma \emph{template} para substituição com auxilio de \emph{StringTemplate}.

Note-se que a distribuição dos agente significa que a colocação dos diversos componentes acontece em simultâneo. Uma vez que o sistema de ficheiros é distribuído a pasta de serviço registada fica disponível em todas as \emph{VMs}.

\lstinputlisting[style=c, caption=Exemplo de uma \emph{template} de configuração, label=lst:template]{listings/template.sr}

% subsection Agentes (end)

\subsection{Agente: Application Deployer}
Assim que um cliente faz upload da sua aplicação comprimida (em formato \textit{.zip}), ela é guardada com um nome único numa pasta partilhada (\textit{uploads}). O caminho para o ficheiro submetido é também adicionado no campo \textit{location} da aplicação em questão, na tabela \textit{applications}.\\
De seguida, é feito um pedido ao mediador, através de web service, para que extraia e mova aplicação para o local correto. Neste pedido é enviada informação sobre a aplicação e o sobre o serviço. O mediador, assim que recebe este pedido, consulta a localização do ficheiro submetido, na tabela \textit{applications}, e reenvia ao Agente \textit{ApplicationDeployer} um pedido, também por web service, contendo esta informação.\\
Quando recebe o pedido o agente extrai a informação sobre o cliente, e sobre o caminho para o ficheiro submetido. De seguida calcula o caminho onde estão guardadas todas as aplicações deste cliente, e extrai o ficheiro para lá.\\
A extração do ficheiro é feita pela class \textit{Unzip}, que faz uso do pacote \textit{java.util.zip}. 

\subsection{Agent: \ac{DNS}} % (fold)
\label{sub:Agent: DNS}
A cargo da configuração do sistema de \ac{DNS} este agente dá entrada da nova zona no ficheiro \emph{named.conf} e cria o respetivo ficheiro de zona e recarrega os ficheiros de configuração do \emph{BIND} de forma a fazer conhecer o novo domínio. A partir deste ponto o domínio fica disponível para o cliente.

\lstinputlisting[style=c, caption=\emph{Template} \emph{DNS}, label=lst:dns_template]{listings/dns_template.sr}
% subsection Agent: \ac{DNS} (end)

\subsection{Agente: Base de Dados} % (fold)
\label{sub:Agente: Base de Dados}
Para cada cliente é criado um novo \emph{cluster} e iniciado um processo \emph{PostgreSQL} independente, o processo pertence ao cliente (\emph{username}) de forma a manter alguma separação e reconhecimento do mesmo.

As tarefas para a criação da base de dados são várias, criação do utilizador, modificação das permissões da pasta que recebe a base de dados, inicialização do \emph{cluster}, alteração da porta, validação do arranque da base de dados, alteração das credenciais do utilizador. Uma execução bem sucedida deste processo disponibiliza a base de dados par o serviço, completando assim mais um passo para na construção do serviço.
% subsection Agente: Base de Dados (end)

\subsection{Agente: Aplicacional} % (fold)
\label{sub:Agente: Aplicacional}
Os processos lançados pelo \emph{PHP-FPM} são também independentes e por cliente, por consequência correm sob o utilizador registado pelo cliente. O processo de configuração inclui criação do utilizador, configuração da \emph{pool} de \emph{workers} e atualização do serviço para refletir o espaço aplicacional. A \emph{template} de configuração apresenta-se na Figura~\ref{lst:template}. Este processo prepara o serviço para receber e correr a aplicação que será colocada pelo agente \emph{Deployer}.
% subsection Agente: Aplicacional (end)

\subsection{Agente: Front Controller} % (fold)
\label{sub:Agente: Front Controller}
Sem a configuração deste componente a aplicação não fica acessível para o exterior, é necessário dar a conhecer o \emph{ip:porta} do servidor onde a aplicação se encontra. O processo de colocação passa pela receção das diretivas de configuração, substituição da \emph{template} designada para este agente e atualização doa nova configuração para dar a conhecer o novo caminho.

% subsection Agente: Front Controller (end)
