/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monitor;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)

@XmlType(name = "StatCollection", propOrder = {
    "stats"
})

@XmlRootElement(name = "StatCollection")
public class StatCollection {
    @XmlElementWrapper
    private List<Stat> stats;
    
    public StatCollection( List<Stat> stats) {
        this.stats = stats;
    }
    
    public StatCollection(){}

    /**
     * @return the stats
     */
    public List<Stat> getStats() {
        return stats;
    }

    /**
     * @param stats the stats to set
     */
    public void setStats(List<Stat> stats) {
        this.stats = stats;
    }
    
    public void addStat(Stat s){
        this.stats.add(s);
    }
    
}
