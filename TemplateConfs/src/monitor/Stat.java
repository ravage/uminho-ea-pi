/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monitor;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;


/**
 *
 * @author cartas
 */
@XmlAccessorType(XmlAccessType.FIELD)

@XmlRootElement(name = "Stat")
public class Stat {
    @XmlAttribute
    private String username;
    @XmlAttribute
    private String type;
    @XmlAttribute
    private long value;
    
    @XmlAttribute
    private int worker;
    //APPLICATION = 1;
    //DATABASE = 2;
    //STORAGE = 3;
    //DNS = 4;
    //FRONT_CONTROLLER = 5;
    
    public Stat(String username,String type, int worker,long value){
        this.username = username;
        this.type = type;
        this.value = value;
        this.worker = worker;
    }
    
    public Stat(){}
    
    public void setUsername(String u){this.username = u;}
    public void setType(String t){this.type = t;}
    public void setValue(long v){this.value = v;}
    public void setWorker(int w){this.worker = w;}
    
    public String getUsername(){return this.username;}
    public long getValue(){return this.value;}
    public String getType(){return this.type;}
    public int getWorker(){return this.worker;}
}
