/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package templateconfs;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
//import org.stringtemplate.v4.ST;

/**
 *
 * @author Pedro
 */
public class NginxConf {
 
    //variables
    private String templatePath; //path of the tempalte pgsql file
    private String outputPath; //where to write the file
    private String server_name; //server name 
    private String access_log; //path for access_log
    private String error_log; //path for error_log
    private String root; //path for php's public folder  
    private String proxy_pass;
    private String proxy_cache;   
    private ArrayList<String> upstream; 
    
        //constructors
    public NginxConf(){
        this.templatePath = "";
        this.outputPath = "";
        this.server_name = "";
        this.access_log = "";
        this.error_log = "";
        this.root = "";
        this.proxy_pass = "";
        this.proxy_cache = "";
        this.upstream = new ArrayList<String>();
    }

    public NginxConf(String tPath, String oPath, String server_name,
                     String access_log, String error_log, String root,
                     String proxy_pass, String proxy_cache, ArrayList<String> upst) {
        this.templatePath = tPath;
        this.outputPath = oPath;
        this.server_name = server_name;
        this.access_log = access_log;
        this.error_log = error_log;
        this.root = root;
        this.proxy_pass = proxy_pass;
        this.proxy_cache = proxy_cache;
        this.upstream = new ArrayList<String>(upst);
    }
    
    //gets
    public String getAccess_log() {return access_log;}
    public String getError_log() {return error_log;}
    public String getRoot() {return root;}
    public String getServer_name() {return server_name;}
    public String getProxy_cache() {return proxy_cache;}
    public String getProxy_pass() {return proxy_pass;}
    public String getOutputPath() {return outputPath;}
    public String getTemplatePath() {return templatePath;}
    public List<String> getUpstream() {return new ArrayList<String>(upstream);}
    
    //sets
    public void setAccess_log(String access_log) {this.access_log = access_log;}
    public void setError_log(String error_log) {this.error_log = error_log;}
    public void setRoot(String root) {this.root = root;}
    public void setServer_name(String server_name) {this.server_name = server_name;}
    public void setProxy_cache(String proxy_cache) {this.proxy_cache = proxy_cache;}
    public void setProxy_pass(String proxy_pass) {this.proxy_pass = proxy_pass;}
    public void setOutputPath(String outputPath) {this.outputPath = outputPath;}
    public void setTemplatePath(String templatePath) {this.templatePath = templatePath;}
    public void setUpstream(List<String> upstream) {this.upstream = new ArrayList<String>(upstream);}
    
    
    //methods
    /*public void create() throws IOException{
        if (this.emptyValues())
            System.out.println("One of the necessary fields wasn't initialized!");
        else{
            String template = NginxConf.readFileAsString(templatePath);
            ST st = new ST(template);
            st.add("server_name", server_name);
            st.add("access_log", access_log);
            st.add("error_log", error_log);
            st.add("root", root);
            st.add("proxy_pass", proxy_pass);
            st.add("proxy_cache", proxy_cache);
            st.add("error_log", error_log);
            st.add("upstream",upstream);
            File f = new File(outputPath);
            st.write(f, null);
        }
    }*/
    
    private static String readFileAsString(String filePath) throws java.io.IOException{
        byte[] buffer = new byte[(int) new File(filePath).length()];
        BufferedInputStream f = null;
        try {
            f = new BufferedInputStream(new FileInputStream(filePath));
            f.read(buffer);
        } finally {
            if (f != null) try { f.close(); } catch (IOException ignored) { }
        }
        return new String(buffer);
    }
    
    private boolean emptyValues(){
        if (this.templatePath.equals("") ||
        this.outputPath.equals("") ||
        this.server_name.equals("") ||
        this.access_log.equals("") ||
        this.error_log.equals("") ||
        this.root.equals("") ||
        this.proxy_pass.equals("") ||
        this.proxy_cache.equals(""))
            return true;
        else 
            return false;
    }
    
}
