/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package templateconfs;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
//import org.stringtemplate.v4.ST;
/**
 *
 * @author Pedro
 */
public class PgConf {
    //variables
    private String templatePath; //path of the tempalte pgsql file
    private String outputPath; //where to write the file
    private Integer port; //port where pgsql listens
    private Integer maxCon; //max connections pgsql accepts
    private String data_dir; //dir where data is located

    
    //constructors
    public PgConf (String tPath, String oPath, Integer port, Integer maxCon, String data_dir){
        this.templatePath = tPath;
        this.outputPath = oPath;
        this.port = port;
        this.maxCon = maxCon;
        this.data_dir = data_dir;
    }
    
    public PgConf (String tPath, String oPath){
        this.templatePath = tPath;
        this.outputPath = oPath;
        this.port = 5432;
        this.maxCon = 100;
        this.data_dir = "ConfigDir";
    }
    
    public PgConf() {
        this.templatePath = "";
        this.outputPath = "";
        this.port = 5432;
        this.maxCon = 100;
        this.data_dir = "ConfigDir";
    }
    
    //gets
    public String getData_dir() {return data_dir;}
    public Integer getMaxCon() {return maxCon;}
    public Integer getPort() {return port;}
    public String getOutputPath() {return outputPath;}
    public String getTemplatePath() {return templatePath;}
    
    //sets
    public void setData_dir(String data_dir) {this.data_dir = data_dir;}
    public void setMaxCon(Integer maxCon) {this.maxCon = maxCon;}
    public void setPort(Integer port) {this.port = port;}
    public void setOutputPath(String outputPath) {this.outputPath = outputPath;}
    public void setTemplatePath(String templatePath) {this.templatePath = templatePath;}
    
    
    //methods
    /*public void create() throws IOException{
        if (templatePath.equals("") || outputPath.equals(""))
            System.out.println("Template path or output path weren't initialized!");
        else{
            String template = PgConf.readFileAsString(templatePath);
            ST st = new ST(template);
            st.add("port", port);
            st.add("max_conn", maxCon);
            st.add("data_dir", data_dir);
            File f = new File(outputPath);
            st.write(f, null);
        }
    }*/
    
    private static String readFileAsString(String filePath) throws java.io.IOException{
        byte[] buffer = new byte[(int) new File(filePath).length()];
        BufferedInputStream f = null;
        try {
            f = new BufferedInputStream(new FileInputStream(filePath));
            f.read(buffer);
        } finally {
            if (f != null) try { f.close(); } catch (IOException ignored) { }
        }
        return new String(buffer);
    }
    
}
