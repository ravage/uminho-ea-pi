/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author ravage
 */
public abstract class GenericController<E extends Serializable, K extends Serializable>  implements IGenericController<E, K> {
    private static final int BATCH_SIZE = 20;
    //protected EntityManager entityManager;
    private Class<E> type;
    private final static ThreadLocal<EntityManager> localEntityManager = new ThreadLocal<EntityManager>();
    
    public GenericController() {
        this.type = (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    public E findById(K id) {
        return getEntityManager().find(type, id);
    }

    @Override
    public Collection<E> findAll() {
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<E> query = builder.createQuery(type);
        query.select(query.from(type));
        return getEntityManager().createQuery(query).getResultList();
    }

    @Override
    public void persist(E entity) {
        getEntityManager().getTransaction().begin();
        getEntityManager().persist(entity);
        getEntityManager().getTransaction().commit();
    }
    
    @Override
    public E update(E entity) {
        getEntityManager().getTransaction().begin();
        E result = getEntityManager().merge(entity);
        getEntityManager().getTransaction().commit();
        return result;
    }
    
    public void persist(Collection<E> batch) {
        int counter = 0;
        getEntityManager().getTransaction().begin();
        for (E item : batch) {
            getEntityManager().persist(item);
            if (counter % BATCH_SIZE == 0) {
                getEntityManager().flush();
                getEntityManager().clear();
            }
        }
        getEntityManager().getTransaction().commit();
    }
    
    @Override
    public void flush() {
        if (!getEntityManager().getTransaction().isActive())
            getEntityManager().getTransaction().begin();
        
        getEntityManager().getTransaction().commit();
        
        if (getEntityManager().getTransaction().isActive())
            getEntityManager().getTransaction().commit();
    }
    
    @Override
    public boolean isPersisted(E entity) {
        return getEntityManager().contains(entity);
    }
    
    @Override
    public void delete(E entity) {
        getEntityManager().getTransaction().begin();
        getEntityManager().remove(entity);
        getEntityManager().getTransaction().commit();
    }
    
    @Override
    public void setEntityManager(EntityManager entityManager) {
        localEntityManager.set(entityManager);
    }

    @Override
    public EntityManager getEntityManager() {
        return localEntityManager.get();
    }
    
    @Override
    public long count() {
        Number result = (Number) getEntityManager().createQuery(
                String.format("SELECT COUNT(*) FROM %s", 
                type.getName())).getSingleResult();
        
        return result.longValue();
    }
}
