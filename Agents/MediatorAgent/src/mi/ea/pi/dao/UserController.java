/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.dao;

import mi.ea.pi.entities.User;
import mi.ea.pi.entities.Worker;
import org.apache.log4j.Logger;

/**
 *
 * @author Pedro
 */
public class UserController extends GenericController<Worker, Long> {
    
    private static Logger logger = Logger.getLogger(UserController.class.getName());
    
    public User findByUsername(String username){

        return getEntityManager().createNamedQuery("User.findByUsername",User.class)
                .setParameter("username", username).getSingleResult();
    }
    
    public User findById(long id){

        return getEntityManager().createNamedQuery("User.findById",User.class)
                .setParameter("id", id).getSingleResult();
    }
}