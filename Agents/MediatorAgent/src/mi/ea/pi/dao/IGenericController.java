/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.dao;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.EntityManager;

/**
 *
 * @author ravage
 */
public interface IGenericController<E extends Serializable, K extends Serializable> {
    E findById(K id);
    Collection<E> findAll();
    void persist(E entity);
    void flush();
    boolean isPersisted(E entity);
    E update(E entity);
    void delete(E entity);
    void setEntityManager(EntityManager entityManager);
    EntityManager getEntityManager(); 
    long count();
}
