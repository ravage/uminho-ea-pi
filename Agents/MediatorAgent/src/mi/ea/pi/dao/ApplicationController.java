/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.dao;
import mi.ea.pi.entities.Application;
import mi.ea.pi.entities.Worker;

/**
 *
 * @author Pedro
 */
public class ApplicationController extends GenericController<Worker, Long>{
    
    public Application findById(long id){
        return getEntityManager().createNamedQuery("Application.findById",Application.class)
                .setParameter("id", id).getSingleResult();
    }
 
}
