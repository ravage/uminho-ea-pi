/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.dao;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import mi.ea.pi.entities.Agent;
import mi.ea.pi.entities.Service;
import mi.ea.pi.entities.User;
import mi.ea.pi.entities.Worker;

/**
 *
 * @author ravage
 */
public class AgentController extends GenericController<Agent, Long>  {
    
    public Agent findByKey(String key) {
        try {
            return getEntityManager().createNamedQuery("Agent.findByKey", Agent.class)
                    .setParameter("key", key)
                    .getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }
}