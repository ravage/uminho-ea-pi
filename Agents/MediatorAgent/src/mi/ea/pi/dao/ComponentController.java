/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.persistence.TypedQuery;
import mi.ea.pi.ComponentType;
import mi.ea.pi.entities.Component;
import mi.ea.pi.entities.Directive;

/**
 *
 * @author ravage
 */
public class ComponentController extends GenericController<Component, Long> {

    public List<Directive> getApplicationDirectives() {
        return getDirectivesFor(ComponentType.Application);
    }
    
    public List<Directive> getDatabaseDirectives() {
        return getDirectivesFor(ComponentType.Database);
    }

    public List<Directive> getStorageDirectives() {
        return getDirectivesFor(ComponentType.Storage);
    }

    private List<Directive> getDirectivesFor(ComponentType componentType) {
        TypedQuery<Directive> query = getEntityManager().createQuery("SELECT directives "
                + "FROM Component component "
                + "JOIN component.directives directives "
                + "WHERE component.key = :component", Directive.class);
        
        query.setParameter("component", String.valueOf(componentType));
        return query.getResultList();
    }

    public List<Directive> getFrontControllerDirectives() {
        return getDirectivesFor(ComponentType.FrontController);
    }
}
