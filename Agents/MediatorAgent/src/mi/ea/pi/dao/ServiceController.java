/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.dao;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import mi.ea.pi.ComponentType;
import mi.ea.pi.entities.Service;
import mi.ea.pi.entities.Worker;

/**
 *
 * @author ravage
 */
public class ServiceController extends GenericController<Service, Long> {

    public void test() {

        Query query = getEntityManager().createNativeQuery("SELECT users.*, services.*, workers.* FROM services "
                + "JOIN users ON services.user_id = users.id "
                + "JOIN workers ON workers.service_id = services.id "
                + "WHERE services.id = 1 and workers.component_id = 1;", "TESTE");

        Object[] results = (Object[]) query.getSingleResult();
    }

    public Worker getDatabaseWorkerFor(Service service) {
        return getWorkerFor(service, ComponentType.Database);
    }

    public Worker getStorageWorkerFor(Service service) {
        return getWorkerFor(service, ComponentType.Storage);
    }

    public Worker getApplicationWorkerFor(Service service) {
        return getWorkerFor(service, ComponentType.Application);
    }

    private Worker getWorkerFor(Service service, ComponentType componentType) {
        TypedQuery<Worker> query = getEntityManager().createQuery("SELECT worker FROM Service service "
                + "JOIN Service.workers worker "
                + "JOIN Worker.component component "
                + "WHERE component.key = :component "
                + "AND service.id = :service", Worker.class);

        query.setParameter("service", service.getId());
        query.setParameter("component", String.valueOf(componentType));
        return query.getSingleResult();
    }

    public Worker getFrontControllerWorkerFor(Service service) {
        return getWorkerFor(service, ComponentType.FrontController);
    }

    public Worker getDNSWorkerFor(Service service) {
        return getWorkerFor(service, ComponentType.DNS);
    }
    
    public Service findById(long id){

        return getEntityManager().createNamedQuery("Service.findById",Service.class)
                .setParameter("id", id).getSingleResult();
    }
}
