/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.workers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import mi.ea.pi.ControllerFactory;
import mi.ea.pi.dao.ComponentController;
import mi.ea.pi.dao.ServiceController;
import mi.ea.pi.dao.WorkerController;
import mi.ea.pi.entities.ConfigurationDirective;
import mi.ea.pi.entities.Directive;
import mi.ea.pi.entities.Service;
import mi.ea.pi.entities.User;
import mi.ea.pi.entities.Worker;

/**
 *
 * @author ravage
 */
public class DatabaseConfiguration extends AbstractConfiguration implements Callable<Worker> {
    private final long serviceId;
    
    public DatabaseConfiguration(long serviceId) {
        this.serviceId = serviceId;
    }

    @Override
    public Worker call() throws Exception {
        ControllerFactory controllerFactory = ControllerFactory.INSTANCE;
        ServiceController serviceController = controllerFactory.getController(ServiceController.class);
        ComponentController componentController = controllerFactory.getController(ComponentController.class);
        WorkerController workerController = controllerFactory.getController(WorkerController.class);
        
        Service service = serviceController.findById(serviceId);
        Worker databaseWorker = serviceController.getDatabaseWorkerFor(service);
        User user = service.getUserId();
        
        List<Directive> directives = componentController.getDatabaseDirectives();        
        Set<ConfigurationDirective> databaseDirectives = new HashSet<ConfigurationDirective>();
        
        for (Directive directive : directives) {
            databaseDirectives.add(new ConfigurationDirective(directive.getName(), directive.getValue()));
        }
        
        databaseDirectives.add(new ConfigurationDirective("username", 
                user.getUsername().toLowerCase()));
        
        databaseDirectives.add(new ConfigurationDirective("service_id", 
                Long.toString(serviceId)));
        
        databaseDirectives.add(new ConfigurationDirective("quantity", 
                Long.toString(databaseWorker.getQuantity())));
        
        databaseWorker.setConfigurationDirectives(databaseDirectives);
        workerController.flush();
        
        deploy(databaseWorker);
        
        return databaseWorker;
    }
    
}