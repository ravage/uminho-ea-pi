/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.workers;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import mi.ea.pi.ControllerFactory;
import mi.ea.pi.dao.ApplicationController;
import mi.ea.pi.dao.ServiceController;
import mi.ea.pi.entities.Application;
import mi.ea.pi.entities.DirectiveItem;
import mi.ea.pi.entities.DirectiveItemCollection;
import mi.ea.pi.entities.Service;
import mi.ea.pi.entities.User;
import mi.ea.pi.entities.Worker;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.client.apache.ApacheHttpClient;
import org.apache.log4j.Logger;

/**
 *
 * @author Pedro
 */
public class AppDeployConfiguration implements Callable<Worker>{

    
    private final long appId;
    private final long servId;
    private final static Logger logger = Logger.getLogger(AppDeployConfiguration.class);
    

    public AppDeployConfiguration(long servId,long appId) {
        this.appId = appId;
        this.servId = servId;
    }
    
    @Override
    public Worker call() throws Exception {
        ControllerFactory controllerFactory = ControllerFactory.INSTANCE;        
        ApplicationController appController = controllerFactory.getController(ApplicationController.class);
        ServiceController servController = controllerFactory.getController(ServiceController.class);
        
        //get user id for this service
        Service service = servController.findById(servId);
        //get user with giver id
        User user = service.getUserId();
        String username = user.getUsername();
        
        //get app with this id
        Application app = appController.findById(appId);
        String upload_path = app.getFileLocation();
        
        //create directives
        DirectiveItem di = new DirectiveItem("username", username);
        DirectiveItem di2 = new DirectiveItem("path", upload_path);
        List <DirectiveItem> dilist = new ArrayList<DirectiveItem>();
        dilist.add(di);
        dilist.add(di2);
        DirectiveItemCollection dic = new DirectiveItemCollection("", dilist);
        
        //create endpoint webresource and send directives
        String endpoint = "http://10.1.0.1:9936";
        Client client = ApacheHttpClient.create();
        WebResource appDeployResource = client.resource(endpoint);
        
        logger.info("Vai enviar dados, para o agente fazer o unzip no local correcto");
        
        appDeployResource.path("deploy").put(dic);
        
        
        
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
            
    
}
