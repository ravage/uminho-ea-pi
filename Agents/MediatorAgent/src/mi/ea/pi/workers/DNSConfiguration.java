/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.workers;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import mi.ea.pi.ControllerFactory;
import mi.ea.pi.dao.ServiceController;
import mi.ea.pi.dao.WorkerController;
import mi.ea.pi.entities.ConfigurationDirective;
import mi.ea.pi.entities.Service;
import mi.ea.pi.entities.User;
import mi.ea.pi.entities.Worker;

/**
 *
 * @author ravage
 */
public class DNSConfiguration extends AbstractConfiguration implements Callable<Worker> {
    private final long serviceId;
    
    public DNSConfiguration(long serviceId) {
        this.serviceId = serviceId;
    }
    
    @Override
    public Worker call() throws Exception {
        ControllerFactory controllerFactory = ControllerFactory.INSTANCE;
        ServiceController serviceController = controllerFactory.getController(ServiceController.class);
        WorkerController workerController = controllerFactory.getController(WorkerController.class);
        
        Service service = serviceController.findById(serviceId);
        Worker dnsWorker = serviceController.getDNSWorkerFor(service);
        
        User user = service.getUserId();
        
        Set<ConfigurationDirective> dnsDirectives = new HashSet<ConfigurationDirective>();
        
        dnsDirectives.add(new ConfigurationDirective("domain", service.getName().toLowerCase()));
        dnsDirectives.add(new ConfigurationDirective("username", user.getUsername().toLowerCase()));
        dnsDirectives.add(new ConfigurationDirective("service_id", Long.toString(serviceId)));
        
        dnsWorker.setConfigurationDirectives(dnsDirectives);
        workerController.flush();
        
        deploy(dnsWorker);
        
        return dnsWorker;
    }
}
