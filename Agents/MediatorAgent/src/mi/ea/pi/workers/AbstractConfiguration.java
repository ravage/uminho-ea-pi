/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.workers;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.client.apache.ApacheHttpClient;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import mi.ea.pi.entities.Configuration;
import mi.ea.pi.entities.ConfigurationDirective;
import mi.ea.pi.entities.DirectiveItem;
import mi.ea.pi.entities.DirectiveItemCollection;
import mi.ea.pi.entities.Worker;

/**
 *
 * @author ravage
 */
public abstract class AbstractConfiguration {

    protected void deploy(Worker worker) {
        // build the client
        String endpoint = String.format("http://%s", worker.getDeployTo());
        Client client = ApacheHttpClient.create();
        WebResource frontControllerResource = client.resource(endpoint);

        //build directives to send
        List<DirectiveItem> directives = new ArrayList<DirectiveItem>();

        for (ConfigurationDirective directive : worker.getConfigurationDirectives()) {
            directives.add(new DirectiveItem(directive.getName(), directive.getValue()));
        }

        Set<Configuration> configs = worker.getComponent().getConfiguration();

        Iterator<Configuration> configIterator = worker.getComponent().getConfiguration().iterator();
        Configuration config = configIterator.next();
        DirectiveItemCollection directiveCollection = new DirectiveItemCollection(config.getTemplate(), directives);

        frontControllerResource.path("deploy").put(directiveCollection);
    }
}
