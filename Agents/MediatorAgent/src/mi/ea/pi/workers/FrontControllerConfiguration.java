/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.workers;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.client.apache.ApacheHttpClient;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import mi.ea.pi.ControllerFactory;
import mi.ea.pi.dao.ComponentController;
import mi.ea.pi.dao.ServiceController;
import mi.ea.pi.dao.WorkerController;
import mi.ea.pi.entities.Configuration;
import mi.ea.pi.entities.ConfigurationDirective;
import mi.ea.pi.entities.Directive;
import mi.ea.pi.entities.DirectiveItem;
import mi.ea.pi.entities.DirectiveItemCollection;
import mi.ea.pi.entities.Service;
import mi.ea.pi.entities.User;
import mi.ea.pi.entities.Worker;

/**
 *
 * @author ravage
 */
public class FrontControllerConfiguration extends AbstractConfiguration implements Callable<Worker> {

    private final long serviceId;

    public FrontControllerConfiguration(long serviceId) {
        this.serviceId = serviceId;
    }

    @Override
    public Worker call() throws Exception {
        ControllerFactory controllerFactory = ControllerFactory.INSTANCE;
        ServiceController serviceController = controllerFactory.getController(ServiceController.class);
        ComponentController componentController = controllerFactory.getController(ComponentController.class);
        WorkerController workerControler = controllerFactory.getController(WorkerController.class);


        Service service = serviceController.findById(serviceId);
        User user = service.getUserId();
        Worker frontControllerWorker = serviceController.getFrontControllerWorkerFor(service);

        List<Directive> directives = componentController.getFrontControllerDirectives();

        Set<ConfigurationDirective> frontControllerDirectives = new HashSet<ConfigurationDirective>();

        for (Directive directive : directives) {
            frontControllerDirectives.add(new ConfigurationDirective(directive.getName(), directive.getValue()));
        }

        String username = user.getUsername().toLowerCase();

        frontControllerDirectives.add(new ConfigurationDirective("service_id",
                Long.toString(serviceId)));

        frontControllerDirectives.add(new ConfigurationDirective("username",
                username));

        frontControllerDirectives.add(new ConfigurationDirective("workers",
                Long.toString(frontControllerWorker.getQuantity())));
        
        frontControllerDirectives.add(new ConfigurationDirective("domain",
                service.getName()));

        // FIXME: will starve server ports
        long count = 6000 + (serviceId * 100);
        
        String port = String.format("server %s:%d;", "10.2.0.2", count);
        frontControllerDirectives.add(new ConfigurationDirective("port", port));

        frontControllerWorker.setConfigurationDirectives(frontControllerDirectives);
        workerControler.flush();
        
        deploy(frontControllerWorker);

        return frontControllerWorker;
    }

    @Override
    protected void deploy(Worker worker) {
        // build the client
        String endpoint = String.format("http://%s", worker.getDeployTo());
        Client client = ApacheHttpClient.create();
        WebResource frontControllerResource = client.resource(endpoint);
        
        //build directives to send
        List<DirectiveItem> directives = new ArrayList<DirectiveItem>();

        for (ConfigurationDirective directive : worker.getConfigurationDirectives()) {
            directives.add(new DirectiveItem(directive.getName(), directive.getValue()));
            
            /*if (directive.getName().equals("port")) {
                // get endpoints
                for (int i = 0; i < Long.valueOf(worker.getQuantity()); i++) {
                    int port = Integer.valueOf(directive.getValue()) + i;
                    directives.add(new DirectiveItem("endpoints",
                            String.format("server %s:%d", "10.1.0.2", port)));
                }
                
            }*/
        }
        
        Set<Configuration> configs = worker.getComponent().getConfiguration();

        Iterator<Configuration> configIterator = worker.getComponent().getConfiguration().iterator();
        Configuration config = configIterator.next();
        DirectiveItemCollection directiveCollection = new DirectiveItemCollection(config.getTemplate(), directives);
        
        frontControllerResource.path("deploy").put(directiveCollection);
    }
}