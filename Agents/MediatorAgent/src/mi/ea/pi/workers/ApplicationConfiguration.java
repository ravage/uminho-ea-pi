/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.workers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import mi.ea.pi.ControllerFactory;
import mi.ea.pi.dao.ComponentController;
import mi.ea.pi.dao.ServiceController;
import mi.ea.pi.dao.WorkerController;
import mi.ea.pi.entities.ConfigurationDirective;
import mi.ea.pi.entities.Directive;
import mi.ea.pi.entities.Service;
import mi.ea.pi.entities.User;
import mi.ea.pi.entities.Worker;

/**
 *
 * @author ravage
 */
public class ApplicationConfiguration extends AbstractConfiguration implements Callable<Worker> {

    private final long serviceId;

    public ApplicationConfiguration(long id) {
        serviceId = id;
    }

    @Override
    public Worker call() throws Exception {
        ControllerFactory controllerFactory = ControllerFactory.INSTANCE;
        ServiceController serviceController = controllerFactory.getController(ServiceController.class);
        ComponentController componentController = controllerFactory.getController(ComponentController.class);
        WorkerController workerController = controllerFactory.getController(WorkerController.class);

        Service service = serviceController.findById(serviceId);
        Worker applicationWorker = serviceController.getApplicationWorkerFor(service);
        User user = service.getUserId();

        List<Directive> directives = componentController.getApplicationDirectives();
        Set<ConfigurationDirective> applicationDirectives = new HashSet<ConfigurationDirective>();

        for (Directive directive : directives) {
            applicationDirectives.add(new ConfigurationDirective(directive.getName(), directive.getValue()));
        }

        // FIXME: will starve server ports
        long port = 6000 + (serviceId * 100);

        String username = user.getUsername().toString().toLowerCase();

        applicationDirectives.add(new ConfigurationDirective("service_id",
                Long.toString(serviceId)));

        applicationDirectives.add(new ConfigurationDirective("username",
                username));

        applicationDirectives.add(new ConfigurationDirective("listen",
                String.format("%d", port)));

        applicationDirectives.add(new ConfigurationDirective("user",
                username));

        applicationDirectives.add(new ConfigurationDirective("pm_max_children",
                Long.toString(applicationWorker.getQuantity())));

        applicationDirectives.add(new ConfigurationDirective("slowlog",
                String.format("/mnt/storage/paas/services/%s/logs/slow.log", user.getUsername().toLowerCase())));

        applicationWorker.setConfigurationDirectives(applicationDirectives);
        workerController.flush();
        
        deploy(applicationWorker);
        
        return applicationWorker;
    }
}