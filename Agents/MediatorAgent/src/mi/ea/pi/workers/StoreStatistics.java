/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.workers;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import mi.ea.pi.entities.StatCollection;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import mi.ea.pi.ControllerFactory;
import mi.ea.pi.dao.ServiceController;
import mi.ea.pi.dao.UserController;
import mi.ea.pi.entities.Service;
import mi.ea.pi.entities.Stat;
import mi.ea.pi.entities.Statistic;
import mi.ea.pi.entities.User;
import mi.ea.pi.entities.Worker;

/**
 *
 * @author Pedro
 */
public class StoreStatistics implements Callable<Worker> {
    private final int APPLICATION = 1;
    private final int DATABASE = 2;
    private final int STORAGE = 3;
    private final int DNS = 4;
    private final int FRONT_CONTROLLER = 5;
    
    private final ArrayList<Stat> stats;
    
    public StoreStatistics(StatCollection sc){
        this.stats = (ArrayList<Stat>) sc.getStats();
    }
    
    @Override
    public Worker call() throws Exception {
        ControllerFactory controllerFactory = ControllerFactory.INSTANCE;
        UserController userController = controllerFactory.getController(UserController.class);
        ServiceController serviceController = controllerFactory.getController(ServiceController.class);
        
        User user = null;
        Service serv = null;
        Worker worker = null;
        TreeSet<Statistic> setStats = null;
        Statistic statistic = null;
                
        for(Stat stat : this.stats){
            user = userController.findByUsername(stat.getUsername());
            serv = user.getServiceSet().iterator().next();
            
            switch(stat.getWorker()){
                case(APPLICATION): worker = serviceController.getApplicationWorkerFor(serv);
                                break;
                case(DATABASE): worker = serviceController.getDatabaseWorkerFor(serv);
                                break;
                case(STORAGE): worker = serviceController.getStorageWorkerFor(serv);
                                break;
            }
            
            statistic = new Statistic(stat.getType() ,stat.getValue(), new Date());
            
            setStats = new TreeSet<Statistic>();
            setStats.add(statistic);
            
            worker.setStatistics(setStats);
            serviceController.flush();
        }
        
        
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
