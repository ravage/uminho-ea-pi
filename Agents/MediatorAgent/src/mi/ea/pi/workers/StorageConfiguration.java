/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.workers;

import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import mi.ea.pi.ControllerFactory;
import mi.ea.pi.dao.ComponentController;
import mi.ea.pi.dao.ServiceController;
import mi.ea.pi.dao.WorkerController;
import mi.ea.pi.entities.ConfigurationDirective;
import mi.ea.pi.entities.Directive;
import mi.ea.pi.entities.Service;
import mi.ea.pi.entities.User;
import mi.ea.pi.entities.Worker;

/**
 *
 * @author ravage
 */
public class StorageConfiguration implements Callable<Worker> {
    private final long serviceId;
    
    public StorageConfiguration(long serviceId) {
        this.serviceId = serviceId;
    }

    @Override
    public Worker call() throws Exception {
        ControllerFactory controllerFactory = ControllerFactory.INSTANCE;
        ServiceController serviceController = controllerFactory.getController(ServiceController.class);
        ComponentController componentController = controllerFactory.getController(ComponentController.class);
        WorkerController workerControler = controllerFactory.getController(WorkerController.class);
        
        Service service = serviceController.findById(serviceId);
        User user = service.getUserId();
        Worker storageWorker = serviceController.getStorageWorkerFor(service);
        
        List<Directive> directives = componentController.getStorageDirectives();
        Set<ConfigurationDirective> storageDirectives = storageWorker.getConfigurationDirectives();
        
        for (Directive directive : directives) {
            storageDirectives.add(new ConfigurationDirective(directive.getName(), directive.getValue()));
        }
        
        storageDirectives.add(new ConfigurationDirective("username", user.getUsername().toLowerCase()));
        storageDirectives.add(new ConfigurationDirective("service_id", Long.toString(serviceId)));
        storageDirectives.add(new ConfigurationDirective("size", Long.toString(storageWorker.getQuantity())));
        
        
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}