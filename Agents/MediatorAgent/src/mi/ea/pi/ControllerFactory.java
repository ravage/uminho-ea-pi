/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import mi.ea.pi.dao.IGenericController;

/**
 *
 * @author ravage
 */
public enum ControllerFactory {
    INSTANCE;
    
    private EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("mi.ea.pi.jpa");
    private EntityManager entityManager = entityManagerFactory.createEntityManager();
    
    public <T extends IGenericController> T getController(Class<T> type) {
        try {
            T klass = type.newInstance();
            klass.setEntityManager(entityManagerFactory.createEntityManager());
            return klass;
        } catch (InstantiationException ex) {
            Logger.getLogger(ControllerFactory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ControllerFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
