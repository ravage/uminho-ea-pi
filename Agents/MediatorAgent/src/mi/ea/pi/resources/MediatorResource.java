/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.resources;

import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import mi.ea.pi.AgentStatus;
import mi.ea.pi.workers.ApplicationConfiguration;
import mi.ea.pi.ControllerFactory;
import mi.ea.pi.async.AsynchronousTask;
import mi.ea.pi.async.IAsyncListener;
import mi.ea.pi.dao.AgentController;
import mi.ea.pi.dao.ComponentController;
import mi.ea.pi.entities.Agent;
import mi.ea.pi.entities.Component;
import mi.ea.pi.entities.StatCollection;
import mi.ea.pi.entities.Worker;
import mi.ea.pi.workers.DNSConfiguration;
import mi.ea.pi.workers.DatabaseConfiguration;
import mi.ea.pi.workers.AppDeployConfiguration;
import mi.ea.pi.workers.FrontControllerConfiguration;
import mi.ea.pi.workers.StoreStatistics;
import org.apache.log4j.Logger;

/**
 *
 * @author ravage
 */
@Path("/")
@Produces(MediaType.APPLICATION_XML)
public class MediatorResource {

    private static Logger logger = Logger.getLogger(MediatorResource.class.getName());
    private static Logger restLogger = Logger.getLogger("RestLogger");
    private static final Executor executor = Executors.newCachedThreadPool();
    private final ControllerFactory controllerFactory;
    @Context
    private HttpServletRequest request;

    public MediatorResource() {
        controllerFactory = ControllerFactory.INSTANCE;
    }

    @GET
    @Path("/work/{id}")
    public String getWork(@PathParam("id") int id) {
        return String.format("got work unit %d", id);
    }
    
    @POST
    @Path("/services/{sid}/apps")
    public void deployApp(@PathParam("sid") long sid, @FormParam("aid") long aid) {
        AsynchronousTask<Worker> deployTask = new AsynchronousTask<Worker>(
                new AppDeployConfiguration(sid,aid), new AsyncMetricsCallback());
        
        executor.execute(deployTask);
    }

    @POST
    @Path("/services")
    public void updateWork(@FormParam("key") long key) {
        logger.debug("Key: " + key);

        AsynchronousTask<Worker> applicationTask = new AsynchronousTask<Worker>(
                new ApplicationConfiguration(key), new AsyncCallback());

        AsynchronousTask<Worker> frontControllerTask = new AsynchronousTask<Worker>(
                new FrontControllerConfiguration(key), new AsyncCallback());

        AsynchronousTask<Worker> databaseControllerTask = new AsynchronousTask<Worker>(
                new DatabaseConfiguration(key), new AsyncCallback());

        AsynchronousTask<Worker> dnsControllerTask = new AsynchronousTask<Worker>(
                new DNSConfiguration(key), new AsyncCallback());

        executor.execute(applicationTask);
        executor.execute(frontControllerTask);
        executor.execute(databaseControllerTask);
        executor.execute(dnsControllerTask);
    }

    @POST
    @Path("/agents/check-in")
    public void checkIn(@FormParam("key") String key, @FormParam("type") long type) {
        AgentController agentController = controllerFactory.getController(AgentController.class);
        ComponentController componentController = controllerFactory.getController(ComponentController.class);

        Agent agent = agentController.findByKey(key);

        if (agent == null) {
            agent = new Agent();
            Component component = componentController.findById(type);
            agent.setComponentId(component);
        }

        agent.setAddress(request.getRemoteAddr());
        agent.setStatus(AgentStatus.UP);
        agent.setKey(key);
        agent.setLastCheck(new Date());

        if (agentController.isPersisted(agent)) {
            agentController.flush();
        } else {
            agentController.persist(agent);
        }
    }

    @POST
    @Path("/metrics")
    public void saveMetrics(StatCollection stats) {
        AsynchronousTask<Worker> statisticsTask = new AsynchronousTask<Worker>(
                new StoreStatistics(stats), new AsyncMetricsCallback());

        executor.execute(statisticsTask);
    }

    private class AsyncMetricsCallback implements IAsyncListener<Worker> {

        @Override
        public void complete(AsynchronousTask<Worker> result) {
            logger.debug(result);
        }
    }

    private class AsyncCallback implements IAsyncListener<Worker> {

        @Override
        public void complete(AsynchronousTask<Worker> result) {
            Worker worker = null;

            try {
                worker = result.get();
                logger.info(String.format("Completed: %s", worker.getComponent().getKey()));
            } catch (InterruptedException ex) {
                java.util.logging.Logger.getLogger(MediatorResource.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ExecutionException ex) {
                java.util.logging.Logger.getLogger(MediatorResource.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
