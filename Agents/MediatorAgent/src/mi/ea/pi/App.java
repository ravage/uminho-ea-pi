/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi;
import java.io.IOException;
import mi.ea.pi.agent.WebAgent;
import org.apache.log4j.Logger;

/**
 *
 * @author ravage
 */
public class App {

    public static void main(String[] args) {
        
        WebAgent agent = new WebAgent("mi.ea.pi.resources", 8080);
        try {
            agent.start();
        } catch (IOException ex) {
            Logger.getLogger(App.class).error(ex.getStackTrace());
        }
    }
}
