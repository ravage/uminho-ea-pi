/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ravage
 */
@Entity
@Table(name = "bundles")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bundle.findAll", query = "SELECT b FROM Bundle b"),
    @NamedQuery(name = "Bundle.findById", query = "SELECT b FROM Bundle b WHERE b.id = :id"),
    @NamedQuery(name = "Bundle.findByName", query = "SELECT b FROM Bundle b WHERE b.name = :name"),
    @NamedQuery(name = "Bundle.findByPrice", query = "SELECT b FROM Bundle b WHERE b.price = :price"),
    @NamedQuery(name = "Bundle.findByDuration", query = "SELECT b FROM Bundle b WHERE b.duration = :duration"),
    @NamedQuery(name = "Bundle.findByCreatedAt", query = "SELECT b FROM Bundle b WHERE b.createdAt = :createdAt")})
public class Bundle implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;
    
    @Basic(optional = false)
    @Column(name = "name", nullable = false, length = 40)
    private String name;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    
    @Basic(optional = false)
    @Column(name = "price", nullable = false, precision = 7, scale = 2)
    private BigDecimal price;
    
    @Basic(optional = false)
    @Column(name = "duration", nullable = false)
    private short duration;
    
    @Column(name = "created_at", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bundleId")
    private Set<BundleComponent> bundleComponentSet;

    public Bundle() {
    }

    public Bundle(Long id) {
        this.id = id;
    }

    public Bundle(Long id, String name, BigDecimal price, short duration) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.duration = duration;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public short getDuration() {
        return duration;
    }

    public void setDuration(short duration) {
        this.duration = duration;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @XmlTransient
    public Set<BundleComponent> getBundleComponentSet() {
        return bundleComponentSet;
    }

    public void setBundleComponentSet(Set<BundleComponent> bundleComponentSet) {
        this.bundleComponentSet = bundleComponentSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bundle)) {
            return false;
        }
        Bundle other = (Bundle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mi.ea.pi.entities.Bundle[ id=" + id + " ]";
    }
    
}
