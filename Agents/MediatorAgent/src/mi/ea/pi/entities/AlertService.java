/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ravage
 */
@Entity
@Table(name = "alert_services")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AlertService.findAll", query = "SELECT a FROM AlertService a"),
    @NamedQuery(name = "AlertService.findById", query = "SELECT a FROM AlertService a WHERE a.id = :id"),
    @NamedQuery(name = "AlertService.findByName", query = "SELECT a FROM AlertService a WHERE a.name = :name"),
    @NamedQuery(name = "AlertService.findByDescription", query = "SELECT a FROM AlertService a WHERE a.description = :description"),
    @NamedQuery(name = "AlertService.findByCreatedAt", query = "SELECT a FROM AlertService a WHERE a.createdAt = :createdAt")})
public class AlertService implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;
    
    @Basic(optional = false)
    @Column(name = "name", nullable = false, length = 128)
    private String name;
    
    @Basic(optional = false)
    @Column(name = "description", nullable = false, length = 512)
    private String description;
    
    @Column(name = "created_at", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    public AlertService() {
    }

    public AlertService(Long id) {
        this.id = id;
    }

    public AlertService(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AlertService)) {
            return false;
        }
        AlertService other = (AlertService) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mi.ea.pi.entities.AlertService[ id=" + id + " ]";
    }
    
}
