/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ravage
 */
@Entity
@Table(name = "directives")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Directive.findAll", query = "SELECT d FROM Directive d"),
    @NamedQuery(name = "Directive.findById", query = "SELECT d FROM Directive d WHERE d.id = :id"),
    @NamedQuery(name = "Directive.findByName", query = "SELECT d FROM Directive d WHERE d.name = :name"),
    @NamedQuery(name = "Directive.findByValue", query = "SELECT d FROM Directive d WHERE d.value = :value"),
    @NamedQuery(name = "Directive.findByCreatedAt", query = "SELECT d FROM Directive d WHERE d.createdAt = :createdAt")})
public class Directive implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;
    
    @Basic(optional = false)
    @Column(name = "name", nullable = false, length = 128)
    private String name;
    
    @Basic(optional = false)
    @Column(name = "value", nullable = false, length = 128)
    private String value;
    
    @Column(name = "created_at", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    
    @JoinColumn(name = "component_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Component component;

    public Directive() {
    }

    public Directive(Long id) {
        this.id = id;
    }

    public Directive(Long id, String name, String value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Component getComponent() {
        return component;
    }

    public void setComponent(Component component) {
        this.component = component;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Directive)) {
            return false;
        }
        Directive other = (Directive) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mi.ea.pi.entities.Directive[ id=" + id + " ]";
    }
    
}
