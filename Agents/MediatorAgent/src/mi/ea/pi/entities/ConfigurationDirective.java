/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ravage
 */
@Entity
@Table(name = "configuration_directives")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConfigurationDirective.findAll", query = "SELECT c FROM ConfigurationDirective c"),
    @NamedQuery(name = "ConfigurationDirective.findById", query = "SELECT c FROM ConfigurationDirective c WHERE c.id = :id"),
    @NamedQuery(name = "ConfigurationDirective.findByName", query = "SELECT c FROM ConfigurationDirective c WHERE c.name = :name"),
    @NamedQuery(name = "ConfigurationDirective.findByValue", query = "SELECT c FROM ConfigurationDirective c WHERE c.value = :value"),
    @NamedQuery(name = "ConfigurationDirective.findByCreatedAt", query = "SELECT c FROM ConfigurationDirective c WHERE c.createdAt = :createdAt")})
public class ConfigurationDirective implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;
    
    @Basic(optional = false)
    @Column(name = "name", nullable = false, length = 128)
    private String name;
    
    @Basic(optional = false)
    @Column(name = "value", nullable = false, length = 128)
    private String value;
    
    @Column(name = "created_at", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    
    @JoinColumn(name = "worker_id", referencedColumnName = "id")
    @ManyToOne
    private Worker worker;

    public ConfigurationDirective() {
    }

    public ConfigurationDirective(Long id) {
        this.id = id;
    }

    public ConfigurationDirective(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Worker getWorker() {
        return worker;
    }

    public void setWorker(Worker worker) {
        this.worker = worker;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ConfigurationDirective)) {
            return false;
        }
        
        if (id == null)
            return false;
        
        ConfigurationDirective other = (ConfigurationDirective) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mi.ea.pi.entities.ConfigurationDirective[ id=" + id + " ]";
    }
    
}
