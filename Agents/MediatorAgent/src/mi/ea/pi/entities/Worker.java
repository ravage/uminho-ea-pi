/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ravage
 */
@Entity
@Table(name = "workers", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"service_id", "component_id"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Worker.findAll", query = "SELECT w FROM Worker w"),
    @NamedQuery(name = "Worker.findById", query = "SELECT w FROM Worker w WHERE w.id = :id"),
    @NamedQuery(name = "Worker.findByQuantity", query = "SELECT w FROM Worker w WHERE w.quantity = :quantity"),
    @NamedQuery(name = "Worker.findByPercentageLimit", query = "SELECT w FROM Worker w WHERE w.percentageLimit = :percentageLimit"),
    @NamedQuery(name = "Worker.findByCreatedAt", query = "SELECT w FROM Worker w WHERE w.createdAt = :createdAt"),
    @NamedQuery(name = "Worker.findByDeployTo", query = "SELECT w FROM Worker w WHERE w.deployTo = :deployTo")})
public class Worker implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;
    
    @Basic(optional = false)
    @Column(name = "quantity", nullable = false)
    private long quantity;
    
    @Basic(optional = false)
    @Column(name = "percentage_limit", nullable = false)
    private long percentageLimit;
    
    @Basic(optional = false)
    @Column(name = "deploy_to", nullable = false, length = 16) 
    private String deployTo; 
    
    @Column(name = "created_at", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    
    @JoinColumn(name = "service_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Service service;
    
    @JoinColumn(name = "component_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Component component;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "worker")
    private Set<Statistic> statistics;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "worker")
    private Set<ConfigurationDirective> configurationDirectives;

    public Worker() {
    }

    public Worker(Long id) {
        this.id = id;
    }

    public Worker(Long id, long quantity, long percentageLimit) {
        this.id = id;
        this.quantity = quantity;
        this.percentageLimit = percentageLimit;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public long getPercentageLimit() {
        return percentageLimit;
    }

    public void setPercentageLimit(long percentageLimit) {
        this.percentageLimit = percentageLimit;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Component getComponent() {
        return component;
    }

    public void setComponent(Component component) {
        this.component = component;
    }
    
    public String getDeployTo() {
        return deployTo;
    }

    public void setDeployTo(String deployTo) {
        this.deployTo = deployTo;
    }

    @XmlTransient
    public Set<Statistic> getStatistics() {
        return statistics;
    }

    public void setStatistics(Set<Statistic> statistics) {
        this.statistics = statistics;
        for (Statistic stat : this.statistics)
            stat.setWorkerId(this);
    }

    @XmlTransient
    public Set<ConfigurationDirective> getConfigurationDirectives() {
        return configurationDirectives;
    }

    public void setConfigurationDirectives(Set<ConfigurationDirective> configurationDirectives) {
        this.configurationDirectives = configurationDirectives;
        
        for (ConfigurationDirective directive : this.configurationDirectives)
            directive.setWorker(this);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Worker)) {
            return false;
        }
        Worker other = (Worker) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mi.ea.pi.entities.Worker[ id=" + id + " ]";
    }
    
}
