/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ravage
 */
@Entity
@Table(name = "statistics")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Statistic.findAll", query = "SELECT s FROM Statistic s"),
    @NamedQuery(name = "Statistic.findById", query = "SELECT s FROM Statistic s WHERE s.id = :id"),
    @NamedQuery(name = "Statistic.findByName", query = "SELECT s FROM Statistic s WHERE s.name = :name"),
    @NamedQuery(name = "Statistic.findByUsage", query = "SELECT s FROM Statistic s WHERE s.usage = :usage"),
    @NamedQuery(name = "Statistic.findByObservations", query = "SELECT s FROM Statistic s WHERE s.observations = :observations"),
    @NamedQuery(name = "Statistic.findByCreatedAt", query = "SELECT s FROM Statistic s WHERE s.createdAt = :createdAt")})
public class Statistic implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;
    
    @Basic(optional = false)
    @Column(name = "name", nullable = false, length = 128)
    private String name;

    @Column(name = "statistics_date",nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date statisticsDate;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    
    @Basic(optional = false)
    @Column(name = "usage", nullable = false)
    private long usage;
    
    @Column(name = "observations", length = 512)
    private String observations;
    
    @Column(name = "created_at", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    
    @JoinColumn(name = "worker_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Worker worker;

    public Statistic() {
    }

    public Statistic(Long id) {
        this.id = id;
    }

    public Statistic(String name, long usage, Date date) {
        this.name = name;
        this.usage = usage;
        this.statisticsDate = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getUsage() {
        return usage;
    }

    public void setUsage(long usage) {
        this.usage = usage;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
    
    public Date getStatisticsDate() {
        return this.statisticsDate;
    }

    public void setStatisticsDate(Date statisticsDate) {
        this.statisticsDate = statisticsDate;
    }

    public Worker getWorkerId() {
        return worker;
    }

    public void setWorkerId(Worker workerId) {
        this.worker = workerId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Statistic)) {
            return false;
        }
        
        if(id==null) 
            return false;
        
        Statistic other = (Statistic) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mi.ea.pi.entities.Statistic[ id=" + id + " ]";
    }
    
}
