/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import mi.ea.pi.AgentStatus;

/**
 *
 * @author ravage
 */
@Entity
@Table(name = "agents")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Agent.findAll", query = "SELECT a FROM Agent a"),
    @NamedQuery(name = "Agent.findById", query = "SELECT a FROM Agent a WHERE a.id = :id"),
    @NamedQuery(name = "Agent.findByAddress", query = "SELECT a FROM Agent a WHERE a.address = :address"),
    @NamedQuery(name = "Agent.findByStatus", query = "SELECT a FROM Agent a WHERE a.status = :status"),
    @NamedQuery(name = "Agent.findByLastCheck", query = "SELECT a FROM Agent a WHERE a.lastCheck = :lastCheck"),
    @NamedQuery(name = "Agent.findByCreatedAt", query = "SELECT a FROM Agent a WHERE a.createdAt = :createdAt"),
    @NamedQuery(name = "Agent.findByKey", query = "SELECT a FROM Agent a WHERE a.key = :key")})
public class Agent implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "address", length = 16)
    private String address;
    
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private AgentStatus status;
    
    @Basic(optional = false)
    @Column(name = "last_check", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastCheck;
    
    @Column(name = "created_at", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    
    @Basic(optional = false)
    @Column(name = "key", nullable = false, length = 64)
    private String key;
    
    @JoinColumn(name = "component_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Component componentId;

    public Agent() {
    }

    public Agent(Long id) {
        this.id = id;
    }

    public Agent(Long id, Date lastCheck, String key) {
        this.id = id;
        this.lastCheck = lastCheck;
        this.key = key;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public AgentStatus getStatus() {
        return status;
    }

    public void setStatus(AgentStatus status) {
        this.status = status;
    }

    public Date getLastCheck() {
        return lastCheck;
    }

    public void setLastCheck(Date lastCheck) {
        this.lastCheck = lastCheck;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Component getComponentId() {
        return componentId;
    }

    public void setComponentId(Component componentId) {
        this.componentId = componentId;
    }
    
    @Transient
    public boolean isNew() {
        return (this.id == null);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Agent)) {
            return false;
        }
        Agent other = (Agent) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mi.ea.pi.entities.Agent[ id=" + id + " ]";
    }
    
}
