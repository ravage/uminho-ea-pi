/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ravage
 */
@Entity
@Table(name = "bundle_components", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"bundle_id", "component_id"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BundleComponent.findAll", query = "SELECT b FROM BundleComponent b"),
    @NamedQuery(name = "BundleComponent.findById", query = "SELECT b FROM BundleComponent b WHERE b.id = :id"),
    @NamedQuery(name = "BundleComponent.findByQuantity", query = "SELECT b FROM BundleComponent b WHERE b.quantity = :quantity"),
    @NamedQuery(name = "BundleComponent.findByCreatedAt", query = "SELECT b FROM BundleComponent b WHERE b.createdAt = :createdAt")})
public class BundleComponent implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;
    
    @Column(name = "quantity")
    private Integer quantity;
    
    @Column(name = "created_at", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    
    @JoinColumn(name = "component_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Component componentId;
    
    @JoinColumn(name = "bundle_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Bundle bundleId;

    public BundleComponent() {
    }

    public BundleComponent(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Component getComponentId() {
        return componentId;
    }

    public void setComponentId(Component componentId) {
        this.componentId = componentId;
    }

    public Bundle getBundleId() {
        return bundleId;
    }

    public void setBundleId(Bundle bundleId) {
        this.bundleId = bundleId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BundleComponent)) {
            return false;
        }
        BundleComponent other = (BundleComponent) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mi.ea.pi.entities.BundleComponent[ id=" + id + " ]";
    }
    
}
