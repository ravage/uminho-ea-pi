/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ravage
 */
@Entity
@Table(name = "components")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Component.findAll", query = "SELECT c FROM Component c"),
    @NamedQuery(name = "Component.findById", query = "SELECT c FROM Component c WHERE c.id = :id"),
    @NamedQuery(name = "Component.findByName", query = "SELECT c FROM Component c WHERE c.name = :name"),
    @NamedQuery(name = "Component.findByDescription", query = "SELECT c FROM Component c WHERE c.description = :description"),
    @NamedQuery(name = "Component.findByPrice", query = "SELECT c FROM Component c WHERE c.price = :price"),
    @NamedQuery(name = "Component.findBySelectable", query = "SELECT c FROM Component c WHERE c.selectable = :selectable"),
    @NamedQuery(name = "Component.findByCreatedAt", query = "SELECT c FROM Component c WHERE c.createdAt = :createdAt")})
public class Component implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;
    
    @Basic(optional = false)
    @Column(name = "name", nullable = false, length = 128)
    private String name;
    
    @Column(name = "description", length = 512)
    private String description;
    
    @Column(name = "price", precision = 7, scale = 2)
    private BigDecimal price;
    
    @Column(name = "selectable")
    private Boolean selectable;
    
    @Column(name = "key", nullable = false, length = 128)
    private String key;
    
    @Column(name = "created_at", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "componentId")
    private Set<BundleComponent> bundleComponentSet;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "componentId")
    private Set<Agent> agents;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "component")
    private Set<Directive> directives;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "component")
    private Set<Worker> workers;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "componentId")
    private Set<Configuration> configuration;

    public Component() {
    }

    public Component(Long id) {
        this.id = id;
    }

    public Component(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Boolean getSelectable() {
        return selectable;
    }

    public void setSelectable(Boolean selectable) {
        this.selectable = selectable;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @XmlTransient
    public Set<BundleComponent> getBundleComponentSet() {
        return bundleComponentSet;
    }

    public void setBundleComponentSet(Set<BundleComponent> bundleComponentSet) {
        this.bundleComponentSet = bundleComponentSet;
    }

    @XmlTransient
    public Set<Agent> getAgents() {
        return agents;
    }

    public void setAgents(Set<Agent> agents) {
        this.agents = agents;
    }

    @XmlTransient
    public Set<Directive> getDirectives() {
        return directives;
    }

    public void setDirectives(Set<Directive> directives) {
        this.directives = directives;
    }

    @XmlTransient
    public Set<Worker> getWorkers() {
        return workers;
    }

    public void setWorkers(Set<Worker> workers) {
        this.workers = workers;
    }

    @XmlTransient
    public Set<Configuration> getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Set<Configuration> configuration) {
        this.configuration = configuration;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Component)) {
            return false;
        }
        Component other = (Component) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mi.ea.pi.entities.Component[ id=" + id + " ]";
    }

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }    
}
