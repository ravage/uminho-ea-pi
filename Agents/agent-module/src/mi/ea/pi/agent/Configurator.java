/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.agent;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.Callable;
import mi.ea.pi.Utils;
import mi.ea.pi.entities.DirectiveItem;
import mi.ea.pi.entities.DirectiveItemCollection;
import org.apache.log4j.Logger;
import org.stringtemplate.v4.ST;

/**
 *
 * @author ravage
 */
public class Configurator implements Callable<Boolean> {

    private final DirectiveItemCollection directives;
    private final String command;
    private final String outputTo;
    private final String action;
    private final static Logger logger = Logger.getLogger("AgentLogger");

    // FIXME: output parameters should be externalized
    public Configurator(String outputTo, String command, String action, DirectiveItemCollection directives) {
        this.directives = directives;
        this.command = command;
        this.outputTo = outputTo;
        this.action = action;
    }

    @Override
    public Boolean call() throws Exception {
        String username = "unknown";
        ST template = new ST(directives.getTemplate());

        for (DirectiveItem directive : directives.getDirectives()) {
            template.add(directive.getName(), directive.getValue());
            
            if (directive.getName().equals("username"))
                username = (String)directive.getValue();
        }

        BufferedWriter output = null;
        
        File logsPath = new File(String.format("/mnt/storage/paas/services/%s/logs", username));
        File appsPath = new File(String.format("/mnt/storage/paas/services/%s/apps", username));
        
        if (!logsPath.exists())
            logsPath.mkdirs();
        
        if (!appsPath.exists())
            appsPath.mkdirs();
        
        try {
            output = new BufferedWriter(new FileWriter(String.format("%s/%s", 
                    outputTo, username)));
            
            output.write(template.render());
            output.close();
        } catch (IOException ex) {
            logger.error(ex, ex);
            return false;
        }
        
        logger.info("Restarting Daemon!");
        
        try {
            int returnCode = Utils.executeProcess(command, action);
            logger.info(String.format("Executed (%s) with exit code (%d)", command, returnCode));
        } catch (Exception e) {
            logger.error(e);
            return false;
        }
        
        return true;
    }
}