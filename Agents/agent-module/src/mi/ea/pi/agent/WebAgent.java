/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.agent;

import com.sun.jersey.api.container.grizzly2.GrizzlyWebContainerFactory;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import javax.ws.rs.core.UriBuilder;
import mi.ea.pi.agent.workers.CheckInWorker;
import org.glassfish.grizzly.http.server.HttpServer;

/**
 *
 * @author ravage
 */
public class WebAgent {

    private final int port;
    private final String resources;
    private final MediatorSetup setup;
    private HttpServer server;

    public WebAgent(MediatorSetup setup, String resources, int port) {
        this.port = port;
        this.resources = resources;
        this.setup = setup;
    }

    public WebAgent(String resources, int port) {
        this.port = port;
        this.resources = resources;
        this.setup = null;
    }
    
    public void start() throws IOException {
        Map<String, String> params = new HashMap<String, String>();
        params.put("com.sun.jersey.config.property.packages", resources);

        server = GrizzlyWebContainerFactory.create(
                UriBuilder.fromUri("http://0.0.0.0/").port(port).build(), params);

        server.start();
        
        if (setup != null) {
            Timer scheduler = new Timer("CHECK-IN");
            scheduler.schedule(new CheckInWorker(setup), 5000, 30000);
        }
        System.in.read();
    }

    public void stop() {
        if (server.isStarted()) {
            server.stop();
        }
    }
}