/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.agent.workers;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.representation.Form;
import com.sun.jersey.client.apache.ApacheHttpClient;
import java.util.TimerTask;
import javax.ws.rs.core.MultivaluedMap;
import mi.ea.pi.agent.MediatorSetup;
import org.apache.log4j.Logger;

/**
 *
 * @author ravage
 */
public class CheckInWorker extends TimerTask {
    private final WebResource mediatorResource;
    private final MediatorSetup setup;
    private static final Logger logger = Logger.getLogger("AgentLogger");
    
    public CheckInWorker(MediatorSetup setup) {
        Client client = ApacheHttpClient.create();
        mediatorResource = client.resource(setup.getMediatorAdress());
        this.setup = setup;
    }

    @Override
    public void run() {
        MultivaluedMap<String, String> params = new Form();
        params.add("key", setup.getKey());
        params.add("type", setup.getType());
        mediatorResource.post(params);
        logger.info("Check-In!");
    }
}