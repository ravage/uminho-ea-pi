/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.agent;

/**
 *
 * @author ravage
 */


public class MediatorSetup {
    private final String mediatorAdress;
    private final String key;
    private final String type;
    
    public MediatorSetup(String mediatorAddress, String key, String type) {
        this.mediatorAdress = mediatorAddress;
        this.key = key;
        this.type = type;
    }

    /**
     * @return the mediatorAdress
     */
    public String getMediatorAdress() {
        return mediatorAdress;
    }

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }
}