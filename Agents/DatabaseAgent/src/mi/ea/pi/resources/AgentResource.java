/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.resources;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import mi.ea.pi.async.AsynchronousTask;
import mi.ea.pi.async.IAsyncListener;
import mi.ea.pi.entities.DirectiveItemCollection;
import mi.ea.pi.workers.DatabaseWorker;
import org.apache.log4j.Logger;

/**
 *
 * @author ravage
 */
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class AgentResource {
    private final static Executor executor = Executors.newCachedThreadPool();
    
    @Context
    HttpServletRequest request;
    
    @GET @Path("/work-unit/{id}")
    public String workUnit(@PathParam("id") int id) {
        return String.format("got work unit %d", id);
    }
    
    @PUT @Path("/deploy")
    public void deploy(DirectiveItemCollection directives) {
        DatabaseWorker worker = new DatabaseWorker(directives);
        
        AsynchronousTask<Boolean> task = new AsynchronousTask<Boolean>(worker, 
                new ConfiguratorCallBack());
        
        executor.execute(task);
    }
    
    private class ConfiguratorCallBack implements IAsyncListener<Boolean> {

        @Override
        public void complete(AsynchronousTask<Boolean> result) {
            try {
                Logger.getLogger("AgentLogger").info(String.format("Deployment %s!", (result.get()) ? "Successful" : "Failed"));
            } catch (InterruptedException ex) {
                Logger.getLogger("AgentLogger").error(ex, ex);
            } catch (ExecutionException ex) {
                Logger.getLogger("AgentLogger").error(ex, ex);
            }
        }
    }
}