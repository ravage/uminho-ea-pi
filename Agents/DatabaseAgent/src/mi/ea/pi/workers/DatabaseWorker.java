package mi.ea.pi.workers;

import java.io.File;
import java.util.concurrent.Callable;
import mi.ea.pi.Utils;
import mi.ea.pi.entities.DirectiveItem;
import mi.ea.pi.entities.DirectiveItemCollection;
import org.apache.log4j.Logger;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ravage
 */
public class DatabaseWorker implements Callable<Boolean> {

    private static final String DB_COMMAND = "/usr/bin/psql";
    private static final String DB_INIT = "/usr/bin/initdb";
    private static final String SED = "/bin/sed";
    private static final String DB_CTL = "/usr/bin/pg_ctl";
    private static final String CHOWN = "/bin/chown";
    private static final String SU = "/bin/su";
    private final static String ADD_USER = "/usr/sbin/useradd";
    private final static String GPASSWD = "/usr/bin/gpasswd";
    private final DirectiveItemCollection directives;

    public DatabaseWorker(DirectiveItemCollection directives) {
        this.directives = directives;
    }

    @Override
    public Boolean call() throws Exception {
        ProcessBuilder processBuilder = new ProcessBuilder();

        String username = "";
        int serviceId = 0;

        for (DirectiveItem directive : directives.getDirectives()) {
            if (directive.getName().equals("username")) {
                username = directive.getValue();
            } else if (directive.getName().equals("service_id")) {
                serviceId = Integer.valueOf(directive.getValue());
            }
        }

        if (username.equals("") || serviceId == 0) {
            return false;
        }
        // FIXME: leads to port starvation
        int port = 5432 + serviceId;
        String command = "";

        try {
            // create database cluster
            String path = String.format("/mnt/storage/paas/services/%s/db", username);
            String postgresConfigPath = String.format("%s/postgresql.conf", path);
            String postgresHba = String.format("%s/pg_hba.conf", path);
            // set user permissions
            Utils.executeProcess(ADD_USER, username);
            Utils.executeProcess(GPASSWD, "-a", username, "workers");

            // create db directory
            Utils.createDirIfNotExists(path);
            // make it user property
            Utils.executeProcess(CHOWN, username, path);
            // initialize it in behalf of the users
            Logger.getLogger("AgentLogger").info("DB Init!");
            String suInit = String.format("%s -D %s", DB_INIT, path);
            Utils.executeProcess(SU, "-", username, "-c", suInit);

            // replace cluster port number
            Logger.getLogger("AgentLogger").info("DB Port Replace!");
            String portPattern = String.format("s/#port = 5432/port = %d/g", port);
            String listenPattern = "s/#listen_addresses = 'localhost'/listen_addresses = '*'/g";

            Utils.executeProcess(SED, "-i", portPattern, postgresConfigPath);
            Utils.executeProcess(SED, "-i", listenPattern, postgresConfigPath);
            //#listen_addresses = 'localhost'

            // change pg_hba
            Logger.getLogger("AgentLogger").info("DB Security Settings!");
            StringBuilder sb = new StringBuilder(Utils.readFile(postgresHba));
            sb.append("host all all 0.0.0.0/0 md5");
            Utils.writeFile(postgresHba, sb.toString());

            // start the cluster
            Logger.getLogger("AgentLogger").info("DB Start!");
            command = String.format("%s -D %s start", DB_CTL, path);
            Utils.executeProcess(SU, "-", username, "-c", command);

            String output = "";
            
            // create admin user
            command = String.format("ALTER USER %s WITH PASSWORD '%s' SUPERUSER",
                    username, "1234");
            do {
                Logger.getLogger("AgentLogger").info("Checking DB!");
                
                Thread.currentThread().sleep(5000);
                
                output = Utils.executeProcessOutput(DB_COMMAND, "-p", String.valueOf(port),
                        "-U", username, "-c", command, "postgres");
            } while (output.contains("could not connect"));

            
            Logger.getLogger("AgentLogger").info("DB Alter User!");
            
        } catch (Exception ex) {
            Logger.getLogger("AgentLogger").error(ex, ex);
            return false;
        }
        return true;
    }
}
