/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi;

import java.io.IOException;
import mi.ea.pi.agent.MediatorSetup;
import mi.ea.pi.agent.WebAgent;
import org.apache.log4j.Logger;

/**
 *
 * @author ravage
 */
public class App {
    public static void main(String[] args) {
        MediatorSetup setup = new MediatorSetup("http://10.2.0.3:8080/agents/check-in", "304ce9ea-aecd-11e0-8f4f-0017f2ceafb4", "1");
        WebAgent agent = new WebAgent(setup, "mi.ea.pi.resources", 9999);
        try {
            agent.start();
        } catch (IOException ex) {
            Logger.getLogger("AgentLogger").error(ex, ex);
        }
    }
}