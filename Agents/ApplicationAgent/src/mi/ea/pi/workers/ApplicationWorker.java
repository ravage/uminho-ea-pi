/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.workers;

import java.util.concurrent.Callable;
import mi.ea.pi.Utils;
import mi.ea.pi.agent.Configurator;
import mi.ea.pi.entities.DirectiveItem;
import mi.ea.pi.entities.DirectiveItemCollection;
import org.apache.log4j.Logger;

/**
 *
 * @author ravage
 */
public class ApplicationWorker implements Callable<Boolean> {
    private final static String ADD_USER = "/usr/sbin/useradd";
    private final static String GPASSWD = "/usr/bin/gpasswd";
    
    private final DirectiveItemCollection directives;
    
    public ApplicationWorker(DirectiveItemCollection directives) {
        this.directives = directives;
    }
    
    @Override
    public Boolean call() throws Exception {
        String username = getDirective("username");
        Utils.executeProcess(ADD_USER, username);
        Utils.executeProcess(GPASSWD, "-a", username, "workers");
        
        Configurator configurator = new Configurator("/mnt/storage/paas/pools", 
                "/etc/init.d/php-fpm", "restart", directives);
        
        configurator.call();
        
        return true;
    }
    
    private String getDirective(String key) {
        for (DirectiveItem item : directives.getDirectives()) {
            if (item.getName().equals(key))
                return item.getValue();
        }
        return "";
    }
}
