/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ravage
 */
public class Utils {

    private Utils() {}

    public static int executeProcess(String... args) throws InterruptedException, IOException {
        ProcessBuilder builder = new ProcessBuilder(args);
        Process process = builder.start();
        //debugOutput(process.getErrorStream());
        //debugOutput(process.getInputStream());
        return process.waitFor();
    }
    
    public static int executeProcessDebug(String... args) throws InterruptedException, IOException {
        ProcessBuilder builder = new ProcessBuilder(args);
        Process process = builder.start();
        debugOutput(process.getErrorStream());
        debugOutput(process.getInputStream());
        return process.waitFor();
    }
    
    public static String executeProcessOutput(String... args) throws InterruptedException, IOException {
        ProcessBuilder builder = new ProcessBuilder(args);
        Process process = builder.start();
        process.waitFor();
        return getProcessInput(process.getErrorStream());
    }
    
    public static String getProcessInput(InputStream stream) {
        InputStreamReader reader = new InputStreamReader(stream);
        BufferedReader bufferedReader = new BufferedReader(reader);
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sb.toString();
    }

    public static void debugOutput(InputStream stream) {
        InputStreamReader reader = new InputStreamReader(stream);
        BufferedReader bufferedReader = new BufferedReader(reader);
        String line;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                Logger.getLogger(Utils.class.getName()).info(line);
            }
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String readFile(String path) {

        FileReader file;
        StringBuilder sb = new StringBuilder();

        try {
            file = new FileReader(path);
            BufferedReader reader = new BufferedReader(file);
            String line;

            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append(System.getProperty("line.separator"));
                }
            } finally {
                reader.close();
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }

        return sb.toString();
    }

    public static void writeFile(String path, String content) {
        FileWriter file = null;
        try {

            file = new FileWriter(path);
            BufferedWriter writer = new BufferedWriter(file);

            try {
                writer.write(content);
            } finally {
                writer.close();
            }

        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static boolean createDirIfNotExists(String path) {
        File dir = new File(path);
        
        if (!dir.exists()) {
            return dir.mkdir();
        }
        
        return false;
    }
}
