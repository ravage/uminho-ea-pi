/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.entities;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Pedro
 */
public class MetricsCollector {
    private final int APPLICATION = 1;
    private final int DATABASE = 2;
    private final int STORAGE = 3;
    private final int DNS = 4;
    private final int FRONT_CONTROLLER = 5;
    //variables    
    private ArrayList<Stat> statistics;
    private String rootPath = "/mnt/storage/paas/services/";
    
    
    
    //constructors
    public MetricsCollector(){
    }
    
    //gets  
    public ArrayList<Stat> getStatistics(){
        return new ArrayList<Stat>(statistics);
    }
    //sets
    public void setStatistics(ArrayList<Stat> s){
        this.statistics = new ArrayList<Stat>(s);
    }
    

    //methods
    public static String executeShellCommand(String command){
        String[] executeArray = {
            "/bin/sh",
            "-c",
            command
        };
        Runtime r = Runtime.getRuntime();
        String output="";
        
        try{    
            Process p = r.exec(executeArray);

            InputStream in = p.getInputStream();
            BufferedInputStream buf = new BufferedInputStream(in);
            InputStreamReader inread = new InputStreamReader(buf);
            BufferedReader bufferedreader = new BufferedReader(inread);

            String line ="";
            while ((line= bufferedreader.readLine()) != null){
                output+= line + "\n";
            }
                

            // Check for command failure
            try {
                if (p.waitFor() != 0) {
                //System.err.println("exit value = " + p.exitValue());
                }
            } catch (InterruptedException e) {
                System.err.println(e);
            } finally {
                // Close the InputStream
                bufferedreader.close();
                inread.close();
                buf.close();
                in.close();
        }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        
        return output;

    }
    
    
    public static ArrayList<Integer> getPids(String user, String process_name){
        String cmd = " ps aux " +
                     " | grep " + process_name +
                     " | grep " + user +
                     " | grep -v grep " +
                     " | grep -v java" +
                     " | gawk '{print $2}'";
        String output = MetricsCollector.executeShellCommand(cmd);
        
        MetricsCollector.log(cmd, output);

        String[] pidsInText =  output.split("\n");
        ArrayList<Integer> pids = new ArrayList<Integer>();
        
        if(output.equals(""))
            return pids;
        else{
            try{
                for(int i=0;i<pidsInText.length;i++){
                    pids.add(Integer.parseInt(pidsInText[i]));
                }
            }catch(Exception e){
                System.err.println(e + "\n convertion from pgrep output failed \n"
                        +cmd+ "\n\n");
                return pids;
            }
            return pids;
        }
    }

    
    public long getMemory(Integer pid){
        String cmd = "gawk '/Private_Dirty/ {asum+=$2} "
                + "END{printf \"%d\\n\",asum}' /proc/" + pid + "/smaps | tail -1";
        String output = executeShellCommand(cmd);
 
        this.log(cmd, output);
        
        if(output.equals(""))
            return -1;
        else{
            try{
                return Math.round(Double.parseDouble(output));
            }catch(Exception e){
                System.err.println(e + "\nconvertion from awk output failed"
                        + cmd + "\n\n");
                return -1;
            }
            
        }
    }
    
    public long getAverageCPU(Integer pid, String process_name){
        if(pid==null)
            return -1;
        else{            
            String cmd = "ps aux |grep " + process_name + 
                         " |" + "grep " + pid +
                         " | grep -v grep" +
                         " | gawk '{print $3}'";
            String output = executeShellCommand(cmd);
            output = output.replaceAll("\n","");
            
            this.log(cmd, output);

            try{
                return Math.round(Double.parseDouble(output));
            }catch(Exception e){
                System.err.println(e + "\nCPUA: convertion from top/awk output"
                        + " failed.\ncomando:" + cmd +"\n\n"+
                        "output: " + output);
                return -1;
            }
        }
    }
    
    public long getInstantCPU(Integer pid){
        if(pid==null)
            return -1;
        else{
            String cmd = "top -n1 -p "+ pid +" -b "+
                         " | grep " + pid +
                         " | gawk '{print $9}'";
            String output = executeShellCommand(cmd);
            output = output.replaceAll("\n","");
            
            this.log(cmd, output);
            
            try{ 
                return Math.round(Double.parseDouble(output));
            }catch(Exception e){
                System.err.println(e + "\nCPUI\n command: "+cmd +
                        "output: "+ output);
                return -1;
            }
        }
    }
    
    public long getDataBaseUsage(String username){
        String path = this.rootPath + username;
        path+= "/db/";
        String cmd = "du -ks " + path +
                     " | gawk '{print $1}'";
        String output = executeShellCommand(cmd).replace("\n", "");
        
        this.log(cmd, output);
        
        if(output.equals(""))
            return -1;
        else{
            try{
                
                return Math.round(Long.parseLong(output)/1024);
            }catch(Exception e){
                System.err.println(e + "\n convertion failed in getDatabaseUsage\n"
                                     + cmd);
                return -1;
            }
        }
    }
    
    public long getStorageUsage(String username){
        String path = this.rootPath + username;
        path+= "/apps/";
        String cmd = "du -ks " + path +
                     " | gawk '{print $1}'";
        String output = executeShellCommand(cmd).replace("\n", "");
        
        this.log(cmd, output);
        
        if(output.equals(""))
            return -1;
        else{
            try{
                
                return Math.round(Long.parseLong(output)/1024);
                
            }catch(Exception e){
                System.err.println(e + "\n convertion failed in getStorage\n"
                                     + cmd);
                return -1;
            }
        }            
    }
    
    
    public StatCollection generateReportAppServer(String processName){
        //IMPORTANT SETS WORKERS; NEEDS CHANGE
        ArrayList<String> process_names = new ArrayList<String>();
        //process_names.add("jboss");
        //process_names.add("php-fpm");
        //process_names.add("firefox");
        process_names.add(processName);
        //get users in this server
        ArrayList<String> users = (ArrayList<String>)this.getUsers();
        //
        this.statistics = new ArrayList<Stat>();        
        
        Stat mem = null;
        Stat cpuA = null;
        Stat cpuI = null;
        Stat dbSpace = null;
        Stat appSpace = null;
        ArrayList<Integer> pids = null;
        int worker = APPLICATION;
        HashMap<String,Long> temp = null;

        //for each user
        for(String user : users){
            if(processName.equals("php-fpm")){
                //calculate dbspace
                dbSpace = new Stat(user,"space", DATABASE,this.getDataBaseUsage(user));
                //calculate appspace
                appSpace = new Stat(user,"space",STORAGE,this.getStorageUsage(user));

                //save quota if calculated correctly
                if(dbSpace.getValue()>=0)
                        this.statistics.add(dbSpace);
                if(appSpace.getValue()>=0)
                        this.statistics.add(appSpace);
            }
        

            //for each worker
            for(String pname : process_names){

                if(pname.equals("postgres"))
                    worker = DATABASE; 
                else 
                    if(pname.equals("php-fpm"))
                        worker = APPLICATION;

                temp = new HashMap<String, Long>();

                //get Pids
                pids = MetricsCollector.getPids(user, pname);
                for(Integer pid : pids){
                    long tempTotal = 0;
                    long actual = 0;

                    //para cada processo deste tipo, guarda o total de mem, cpuA e cpuI na hash
                    //mem
                    actual = this.getMemory(pid);
                    if(actual>=0)
                        if(temp.containsKey("mem")){
                            tempTotal = temp.get("mem") + actual;                              
                            temp.put("mem", tempTotal);
                        }else{
                            temp.put("mem", actual);
                        }

                    //cpuA
                    actual = this.getAverageCPU(pid,pname);
                    if(actual>=0)
                        if(temp.containsKey("cpua")){
                            tempTotal = temp.get("cpua") + actual;
                            temp.put("cpua", tempTotal);
                        }else{
                            temp.put("cpua", actual);
                        }

                    //cpuI
                    actual = this.getInstantCPU(pid);
                    if(actual>=0)
                        if(temp.containsKey("cpui")){
                            tempTotal = temp.get("cpui") + actual;
                            temp.put("cpui", tempTotal);
                        }else{
                            temp.put("cpui", actual);
                        }

                }
                //instance and save stats
                if(temp.containsKey("mem") && temp.get("mem")>=0){
                    mem = new Stat(user,"mem", worker, temp.get("mem"));
                    this.statistics.add(mem);
                }
                if(temp.containsKey("cpua") && temp.get("cpua")>=0){
                    cpuA = new Stat(user,"cpua", worker, temp.get("cpua"));
                    this.statistics.add(cpuA);
                }
                if(temp.containsKey("cpui") && temp.get("cpui")>=0){
                    cpuI = new Stat(user,"cpui", worker, temp.get("cpui"));    
                    this.statistics.add(cpuI);
                }


            }
        }
               
        return new StatCollection(statistics);
    }


    public List<String> getUsers(){
        //folder with user's folders
        File dir = new File(this.rootPath);
        File listDir[] = {};
        ArrayList<String> resDir = new ArrayList<String>();
        
        //create filter which filters hidden files
        FilenameFilter filter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return !name.startsWith(".");
            }
        };
        listDir = dir.listFiles(filter);
        
        for (int i = 0; i < listDir.length; i++) {
                if (listDir[i].isDirectory()) {
                       resDir.add(listDir[i].getName());
                       this.log("GetUSer", listDir[i].getName());
                }
        }
        return resDir;
    }


    public static void log(String cmd, String output){
        System.out.println("cmd: " + cmd);
        System.out.println("output: " + output);
    }
}




    /*public String[] getServerUsers(){
        String usrs = MetricsCollector.executeShellCommand("cat /etc/passwd | grep /home | cut -d: -f1");
        String[] usersList = usrs.split("\\n");
        return usersList;
    }*/



    /*public long getQuota(String user){
        String quotaOutTotal = executeShellCommand("quota -v "+ user);
        
        
        String[] lines = quotaOutTotal.split("\n");
        if(lines.length<3)
            return -1;
        else{
            String[] quotaOut = lines[2].split("\\s+");
            //Integer blocks=null;
            //Integer quota=null;


            try{

                return Long.parseLong(quotaOut[3]);
            }catch(Exception e){
                System.err.println(e + "convertion from quota output failed");
                return -1;
            }
   
        }
    }*/


    /*public static ArrayList<Integer> getPids(String user, String process_name){
        String cmd = "pgrep -U "+ user +" "+ process_name;
        String output = MetricsCollector.executeShellCommand(cmd);
        
        String[] pidsInText =  output.split("\n");
        ArrayList<Integer> pids = new ArrayList<Integer>();
        
        if(output.equals(""))
            return pids;
        else{
            try{
                for(int i=0;i<pidsInText.length;i++){
                    pids.add(Integer.parseInt(pidsInText[i]));
                }
            }catch(Exception e){
                System.err.println(e + "\n convertion from pgrep output failed \n"
                        +"pgrep -U "+ user +" "+ process_name + "\n\n");
                return pids;
            }
            return pids;
        }
    }*/