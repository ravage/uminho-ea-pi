/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.entities;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author ravage
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MediaStreamCollection", propOrder = {
    "directives",
    "template"
})

@XmlRootElement(name = "DirectiveCollection")
public class DirectiveItemCollection {
    @XmlElementWrapper
    private List<DirectiveItem> directives;
    
    @XmlElement(name = "template")
    private String template;
    
    public DirectiveItemCollection() {}
    
    public DirectiveItemCollection(String template, List<DirectiveItem> directives) {
        this.directives = directives;
        this.template = template;
    }

    /**
     * @return the directives
     */
    public List<DirectiveItem> getDirectives() {
        return directives;
    }

    /**
     * @param directives the directives to set
     */
    public void setDirectives(List<DirectiveItem> directives) {
        this.directives = directives;
    }

    /**
     * @return the template
     */
    public String getTemplate() {
        return template;
    }

    /**
     * @param template the template to set
     */
    public void setTemplate(String template) {
        this.template = template;
    }
}
