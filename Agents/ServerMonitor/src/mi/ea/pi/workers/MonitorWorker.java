/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.workers;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.client.apache.ApacheHttpClient;
import java.util.ArrayList;
import java.util.TimerTask;
import javax.management.timer.TimerMBean;
import javax.xml.bind.JAXBException;
import mi.ea.pi.entities.MetricsCollector;
import mi.ea.pi.entities.Stat;
import mi.ea.pi.entities.StatCollection;
import java.util.Random;
/**
 *
 * @author Pedro
 */
public class MonitorWorker extends TimerTask  {
    private final static String BASE_URL = "http://10.1.0.3:8080/";
    //private final static String BASE_URL = "http://localhost:8080/";

    private final WebResource mediatorResource;
    private final String processName;
    
    public MonitorWorker(String processName) {
        Client client = ApacheHttpClient.create();
        mediatorResource = client.resource(BASE_URL);
        this.processName = processName;
    }
    

    @Override
    public void run() {
        
        
        System.out.println("BASE_URL: " + BASE_URL);
        
        MetricsCollector m = new MetricsCollector();
        StatCollection sc = m.generateReportAppServer(processName);
        
        /*Random generator = new Random();
        
        Stat s = new Stat("user1", "cpui", 1, generator.nextInt(100)+1);
        Stat s1 = new Stat("user1", "cpua", 1, generator.nextInt(100)+1);
        Stat s2 = new Stat("user1", "mem", 1, generator.nextInt(400000)+1);
        
        Stat s3 = new Stat("user1", "cpui", 2, generator.nextInt(100)+1);
        Stat s4 = new Stat("user1", "cpua", 2, generator.nextInt(100)+1);
        Stat s5 = new Stat("user1", "mem", 2, generator.nextInt(400000)+1);
        Stat s6 = new Stat("user1", "space", 2, generator.nextInt(500)+1);
        
        Stat s7 = new Stat("user1", "space", 3, generator.nextInt(300)+1);
        
        ArrayList<Stat> sl = new ArrayList<Stat>();
        sl.add(s);
        sl.add(s1);
        sl.add(s2);
        sl.add(s3);
        sl.add(s4);
        sl.add(s5);
        sl.add(s6);
        sl.add(s7);        
        StatCollection sc = new StatCollection(sl);*/
        
        
        for(Stat s : sc.getStats()){
            System.out.print("username: "+ s.getUsername());
            System.out.println("worker: "+s.getWorker());
        }
        
        System.out.println("\n\n--------------------VOU MANDARI N: "+ sc.getStats().size() +"--------------------\n\n");
        
        mediatorResource.path("metrics").post(sc);
    }
}
