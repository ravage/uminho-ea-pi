package mi.ea.pi;

import java.util.Timer;
import javax.xml.bind.JAXBException;
import mi.ea.pi.workers.MonitorWorker;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Pedro
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws JAXBException {
        
        
        System.out.println("STARTED");
        
        if(args.length!=1){
            System.out.println("Apenas recebe um argumento! (nome da aplicação a monitorizar).");
            return;
        }
                
        Timer scheduler = new Timer("SEND-METRICS");
        //scheduler.schedule(new MonitorWorker(args[0]), 1000, 2000);
            
        scheduler.schedule(new MonitorWorker(args[0]), 1000, 120000);

    }
}
