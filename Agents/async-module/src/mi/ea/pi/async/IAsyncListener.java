/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.async;

/**
 *
 * @author ravage
 */
public interface IAsyncListener<T> {
    void complete(AsynchronousTask<T> result);
}