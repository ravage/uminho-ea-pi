/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.async;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/**
 *
 * @author ravage
 */
public class AsynchronousTask<T> extends FutureTask<T> {
    private final IAsyncListener<T> listener;
    
    
    public AsynchronousTask(Callable<T> task, IAsyncListener<T> listener) {
        super(task);
        this.listener = listener;
    }

    @Override
    protected void done() {
        listener.complete(this);
    }
}