/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.resources;

import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import mi.ea.pi.agent.Configurator;
import mi.ea.pi.async.AsynchronousTask;
import mi.ea.pi.async.IAsyncListener;
import mi.ea.pi.entities.DirectiveItemCollection;
import mi.ea.pi.workers.Deployer;
import org.apache.log4j.Logger;

/**
 *
 * @author Pedro
 */
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class AgentResource {
    private final static Logger logger = Logger.getLogger(AgentResource.class);
    private final static Executor executor = Executors.newCachedThreadPool();
    
    @Context
    HttpServletRequest request;
    
    
    @PUT @Path("/deploy")
    public void deploy(DirectiveItemCollection directives) {
        logger.info("Agente recebeu um pedido de deployment");
        System.out.println("RE: Agente recebeu um pedido de deployment");
        
        Deployer deployer = new Deployer(directives);
        
        AsynchronousTask<Boolean> deployTask = new AsynchronousTask<Boolean>(
                deployer, new ConfiguratorCallBack());
        
        executor.execute(deployTask);
    }
    
    private class ConfiguratorCallBack implements IAsyncListener<Boolean> {

        @Override
        public void complete(AsynchronousTask<Boolean> result) {
            try {
                logger.info(String.format("Deployment %s!", (result.get()) ? "Successful" : "Failed"));
            } catch (InterruptedException ex) {
                java.util.logging.Logger.getLogger(AgentResource.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ExecutionException ex) {
                java.util.logging.Logger.getLogger(AgentResource.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
