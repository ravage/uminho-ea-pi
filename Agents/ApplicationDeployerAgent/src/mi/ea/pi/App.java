/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import mi.ea.pi.agent.MediatorSetup;
import mi.ea.pi.agent.WebAgent;
/**
 *
 * @author ravage
 */
public class App {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MediatorSetup setup = new MediatorSetup("http://10.2.0.3:8080/agents/check-in", "AAB93A9D-76A6-4EF4-A9F0-BA89BA8FA449", "6");
        WebAgent agent = new WebAgent(setup, "mi.ea.pi.resources", 9936);
        
        System.out.println("AGENTE DEPLOYER STARTED");
        
        try {
            agent.start();
        } catch (IOException ex) {
            Logger.getLogger(WebAgent.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}