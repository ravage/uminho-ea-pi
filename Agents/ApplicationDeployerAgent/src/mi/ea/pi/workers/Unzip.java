/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.workers;

import java.io.OutputStream;
import java.util.zip.ZipException;
import java.util.Enumeration;
import java.util.zip.ZipFile;
import java.io.InputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;


/**
 * Example to read the elements of a Zip file sequentially.
 *
 * @author Roedy Green, Canadian Mind Products
 * @version 1.0 2008-06-06
 * @since 2008-06-06
 */
public class Unzip{

      public static void unzip(File file, File targetDir) throws ZipException,
        IOException {
          
          
        //File file = new File(file_path);
        //File targetDir = new File(targetDirPath);  
          
        targetDir.mkdirs();
        ZipFile zipFile = new ZipFile(file);
        try {
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                File targetFile = new File(targetDir, entry.getName());
                if (entry.isDirectory()) {
                    targetFile.mkdirs();
                } else {
                    InputStream input = zipFile.getInputStream(entry);
                    try {
                        OutputStream output = new FileOutputStream(targetFile);
                        try {
                            copy(input, output);
                        } finally {
                            output.close();
                        }
                    } finally {
                        input.close();
                    }
                }
            }
        } finally {
            zipFile.close();
        }
    }

    private static void copy(InputStream input, OutputStream output) 
            throws IOException {
        byte[] buffer = new byte[4096];
        int size;
        while ((size = input.read(buffer)) != -1)
            output.write(buffer, 0, size);
    }




}