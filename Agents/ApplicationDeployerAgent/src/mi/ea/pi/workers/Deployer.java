package mi.ea.pi.workers;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.zip.ZipException;
import mi.ea.pi.entities.DirectiveItem;
import mi.ea.pi.entities.DirectiveItemCollection;
import mi.ea.pi.entities.MetricsCollector;
import mi.ea.pi.workers.Unzip;
import org.apache.log4j.Logger;

/**
 *
 * @author Pedro
 */
public class Deployer implements Callable<Boolean>{
    private final String servicesRoot = "/mnt/storage/paas/services/";
    private DirectiveItemCollection dic;
    private final static Logger logger = Logger.getLogger(Deployer.class);
    
    public Deployer(DirectiveItemCollection dic){
        this.dic = dic;
    }
    

    @Override
    public Boolean call() throws ZipException, IOException{
        String username=null;
        String upload_path=null;
        
        //get usrname and upload path from directives
        for(DirectiveItem di : this.dic.getDirectives()){
            if(di.getName().equals("path"))
                upload_path = di.getValue();
            else
                if(di.getName().equals("username"))
                    username = di.getValue();
        }
        
        logger.info("Recebeu directivas!");
        logger.info("Username: " + username);
        System.out.println("Username: " + username);
        logger.info("Path para zip: "+ upload_path);
        System.out.println("Path para zip: "+ upload_path);
        
        //building path => path to unzip file
        StringBuilder sb = new StringBuilder();
        sb.append(this.servicesRoot);
        sb.append(username);
        sb.append("/apps/1/");
        String unzip_dir = sb.toString();
        //unzip_dir = "/Users/Pedro/Desktop/zona_teste/";

        System.out.println("Directorio de extracção: " + unzip_dir);
        logger.info("Directorio de extracção: " + unzip_dir);
        
        //create files for uploaded file and for unziping directory
        File upload_file = new File(upload_path);//File for uploaded zip
        File unzip_dir_file = new File(unzip_dir);//File unziped directory

        if(!unzip_dir_file.exists()){//se nao existir tenta criar
            try{
                
                if(unzip_dir_file.mkdir())
                    System.out.println("Directory Created");
                else
                    System.out.println("Directory is not created");
                }catch(Exception e){
            e.printStackTrace();
            }

        }else{//se existir apaga o conteudo
            //deleteDir(unzip_dir_file);
                logger.info("DEVIA APAGAR MAS ESTÁ COMENTADO PUNTZ PUNTZ");
        }
        Unzip.unzip(upload_file, unzip_dir_file);
        
        String cmd = "chown "+ username + " -R " + unzip_dir;
        logger.info("DEVOLVER PERMISSOES" + cmd);
        MetricsCollector.executeShellCommand(cmd);
        
        logger.info("Terminou processo de extracção");
        return true;
    }
    
    
    
    
    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        logger.info("Limpou a pasta destino!");
        // The directory is now empty so delete it
       // return dir.delete();
        return true;
    }
}




