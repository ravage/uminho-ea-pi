/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi.workers;

import java.util.concurrent.Callable;
import mi.ea.pi.Utils;
import mi.ea.pi.entities.DirectiveItem;
import mi.ea.pi.entities.DirectiveItemCollection;
import org.stringtemplate.v4.ST;

/**
 *
 * @author ravage
 */
public class DNSWorker implements Callable<Boolean> {

    private final DirectiveItemCollection directives;
    private final static String NAMED_CONF = "/var/named/chroot/etc/named.conf";
    private final static String NAMED_DATA_PATH = "/var/named/chroot/var/named/data";
    private final static String NAMED_INIT = "/etc/init.d/named";
    private final static Object lock = new Object();

    public DNSWorker(DirectiveItemCollection directives) {
        this.directives = directives;
    }

    @Override
    public Boolean call() throws Exception {
        String domain = "";

        // get templates by token
        String[] templates = directives.getTemplate().split("<-->");

        String named = templates[0];
        String zone = templates[1];

        // store zone reference
        ST templateNamed = new ST(named);
        ST templateZone = new ST(zone);

        for (DirectiveItem directive : directives.getDirectives()) {
            //store the username
            if (directive.getName().equals("domain")) {
                domain = directive.getValue();
            }

            templateNamed.add(directive.getName(), directive.getValue());
            templateZone.add(directive.getName(), directive.getValue());
        }

        // store named
        String namedFile = Utils.readFile(NAMED_CONF);
        if (!namedFile.contains(domain)) {
            StringBuilder sb = new StringBuilder(namedFile);
            sb.append(System.getProperty("line.separator"))
                    .append(templateNamed.render())
                    .append(System.getProperty("line.separator"));

            synchronized (lock) {
                Utils.writeFile(NAMED_CONF, sb.toString());
            }
        }

        String zonePath = String.format("%s/%s", NAMED_DATA_PATH, domain);
        Utils.writeFile(zonePath, templateZone.render());
        Utils.executeProcess(NAMED_INIT, "restart");

        return true;
    }
}
