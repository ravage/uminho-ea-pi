/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.ea.pi;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import mi.ea.pi.agent.MediatorSetup;
import mi.ea.pi.agent.WebAgent;
/**
 *
 * @author ravage
 */
public class App {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MediatorSetup setup = new MediatorSetup("http://10.2.0.3:8080/agents/check-in", "11f71ba6-afc1-11e0-a8d3-0017f2ceafb4", "4");
        WebAgent agent = new WebAgent(setup, "mi.ea.pi.resources", 9999);
        try {
            agent.start();
        } catch (IOException ex) {
            Logger.getLogger(WebAgent.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}