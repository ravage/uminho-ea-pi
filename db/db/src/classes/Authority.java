/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

public class Authority {
	public Authority() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == classes.ORMConstants.KEY_AUTHORITY_USER) {
			this.user = (classes.User) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private classes.User user;
	
	private String authority;
	
	private java.sql.Timestamp createdAt;
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setAuthority(String value) {
		this.authority = value;
	}
	
	public String getAuthority() {
		return authority;
	}
	
	public void setCreatedAt(java.sql.Timestamp value) {
		this.createdAt = value;
	}
	
	public java.sql.Timestamp getCreatedAt() {
		return createdAt;
	}
	
	public void setUser(classes.User value) {
		if (user != null) {
			user.authority.remove(this);
		}
		if (value != null) {
			value.authority.add(this);
		}
	}
	
	public classes.User getUser() {
		return user;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_User(classes.User value) {
		this.user = value;
	}
	
	private classes.User getORM_User() {
		return user;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
