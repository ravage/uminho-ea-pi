/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class UserCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression name;
	public final BooleanExpression gender;
	public final DateExpression birthdate;
	public final StringExpression username;
	public final StringExpression password;
	public final StringExpression email;
	public final StringExpression nif;
	public final StringExpression activationKey;
	public final BooleanExpression active;
	public final TimestampExpression createdAt;
	
	public UserCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		name = new StringExpression("name", this);
		gender = new BooleanExpression("gender", this);
		birthdate = new DateExpression("birthdate", this);
		username = new StringExpression("username", this);
		password = new StringExpression("password", this);
		email = new StringExpression("email", this);
		nif = new StringExpression("nif", this);
		activationKey = new StringExpression("activationKey", this);
		active = new BooleanExpression("active", this);
		createdAt = new TimestampExpression("createdAt", this);
	}
	
	public UserCriteria(PersistentSession session) {
		this(session.createCriteria(User.class));
	}
	
	public UserCriteria() throws PersistentException {
		this(orm.Hibernatev3PersistentManager.instance().getSession());
	}
	
	public ServiceCriteria createServiceCriteria() {
		return new ServiceCriteria(createCriteria("ORM_Service"));
	}
	
	public AuthorityCriteria createAuthorityCriteria() {
		return new AuthorityCriteria(createCriteria("ORM_Authority"));
	}
	
	public User uniqueUser() {
		return (User) super.uniqueResult();
	}
	
	public User[] listUser() {
		java.util.List list = super.list();
		return (User[]) list.toArray(new User[list.size()]);
	}
}

