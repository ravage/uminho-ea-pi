/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ConfigurationDirectiveCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression name;
	public final StringExpression value;
	public final TimestampExpression createdAt;
	
	public ConfigurationDirectiveCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		name = new StringExpression("name", this);
		value = new StringExpression("value", this);
		createdAt = new TimestampExpression("createdAt", this);
	}
	
	public ConfigurationDirectiveCriteria(PersistentSession session) {
		this(session.createCriteria(ConfigurationDirective.class));
	}
	
	public ConfigurationDirectiveCriteria() throws PersistentException {
		this(orm.Hibernatev3PersistentManager.instance().getSession());
	}
	
	public WorkerCriteria createWorkerCriteria() {
		return new WorkerCriteria(createCriteria("worker"));
	}
	
	public ConfigurationDirective uniqueConfigurationDirective() {
		return (ConfigurationDirective) super.uniqueResult();
	}
	
	public ConfigurationDirective[] listConfigurationDirective() {
		java.util.List list = super.list();
		return (ConfigurationDirective[]) list.toArray(new ConfigurationDirective[list.size()]);
	}
}

