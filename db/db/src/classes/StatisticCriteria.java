/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class StatisticCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression name;
	public final DateExpression statisticsDate;
	public final BigDecimalExpression percentageUse;
	public final StringExpression observations;
	public final TimestampExpression createdAt;
	
	public StatisticCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		name = new StringExpression("name", this);
		statisticsDate = new DateExpression("statisticsDate", this);
		percentageUse = new BigDecimalExpression("percentageUse", this);
		observations = new StringExpression("observations", this);
		createdAt = new TimestampExpression("createdAt", this);
	}
	
	public StatisticCriteria(PersistentSession session) {
		this(session.createCriteria(Statistic.class));
	}
	
	public StatisticCriteria() throws PersistentException {
		this(orm.Hibernatev3PersistentManager.instance().getSession());
	}
	
	public WorkerCriteria createWorkerCriteria() {
		return new WorkerCriteria(createCriteria("worker"));
	}
	
	public Statistic uniqueStatistic() {
		return (Statistic) super.uniqueResult();
	}
	
	public Statistic[] listStatistic() {
		java.util.List list = super.list();
		return (Statistic[]) list.toArray(new Statistic[list.size()]);
	}
}

