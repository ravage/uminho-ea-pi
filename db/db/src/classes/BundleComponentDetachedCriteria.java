/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class BundleComponentDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final IntegerExpression quantity;
	public final TimestampExpression createdAt;
	
	public BundleComponentDetachedCriteria() {
		super(classes.BundleComponent.class, classes.BundleComponentCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		quantity = new IntegerExpression("quantity", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
	}
	
	public BundleComponentDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, classes.BundleComponentCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		quantity = new IntegerExpression("quantity", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
	}
	
	public BundleDetachedCriteria createBundleCriteria() {
		return new BundleDetachedCriteria(createCriteria("bundle"));
	}
	
	public ComponentDetachedCriteria createComponentCriteria() {
		return new ComponentDetachedCriteria(createCriteria("component"));
	}
	
	public BundleComponent uniqueBundleComponent(PersistentSession session) {
		return (BundleComponent) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public BundleComponent[] listBundleComponent(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (BundleComponent[]) list.toArray(new BundleComponent[list.size()]);
	}
}

