/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class StatisticDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression name;
	public final DateExpression statisticsDate;
	public final BigDecimalExpression percentageUse;
	public final StringExpression observations;
	public final TimestampExpression createdAt;
	
	public StatisticDetachedCriteria() {
		super(classes.Statistic.class, classes.StatisticCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		statisticsDate = new DateExpression("statisticsDate", this.getDetachedCriteria());
		percentageUse = new BigDecimalExpression("percentageUse", this.getDetachedCriteria());
		observations = new StringExpression("observations", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
	}
	
	public StatisticDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, classes.StatisticCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		statisticsDate = new DateExpression("statisticsDate", this.getDetachedCriteria());
		percentageUse = new BigDecimalExpression("percentageUse", this.getDetachedCriteria());
		observations = new StringExpression("observations", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
	}
	
	public WorkerDetachedCriteria createWorkerCriteria() {
		return new WorkerDetachedCriteria(createCriteria("worker"));
	}
	
	public Statistic uniqueStatistic(PersistentSession session) {
		return (Statistic) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Statistic[] listStatistic(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Statistic[]) list.toArray(new Statistic[list.size()]);
	}
}

