/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ComponentCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression name;
	public final StringExpression description;
	public final BigDecimalExpression price;
	public final IntegerExpression selectable;
	public final TimestampExpression createdAt;
	
	public ComponentCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		name = new StringExpression("name", this);
		description = new StringExpression("description", this);
		price = new BigDecimalExpression("price", this);
		selectable = new IntegerExpression("selectable", this);
		createdAt = new TimestampExpression("createdAt", this);
	}
	
	public ComponentCriteria(PersistentSession session) {
		this(session.createCriteria(Component.class));
	}
	
	public ComponentCriteria() throws PersistentException {
		this(orm.Hibernatev3PersistentManager.instance().getSession());
	}
	
	public WorkerCriteria createWorkerCriteria() {
		return new WorkerCriteria(createCriteria("ORM_Worker"));
	}
	
	public BundleComponentCriteria createBundleComponentCriteria() {
		return new BundleComponentCriteria(createCriteria("ORM_BundleComponent"));
	}
	
	public ConfigurationCriteria createConfigurationCriteria() {
		return new ConfigurationCriteria(createCriteria("ORM_Configuration"));
	}
	
	public AgentCriteria createAgentCriteria() {
		return new AgentCriteria(createCriteria("ORM_Agent"));
	}
	
	public DirectiveCriteria createDirectiveCriteria() {
		return new DirectiveCriteria(createCriteria("ORM_Directive"));
	}
	
	public Component uniqueComponent() {
		return (Component) super.uniqueResult();
	}
	
	public Component[] listComponent() {
		java.util.List list = super.list();
		return (Component[]) list.toArray(new Component[list.size()]);
	}
}

