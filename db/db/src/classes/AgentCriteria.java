/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class AgentCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression name;
	public final StringExpression address;
	public final StringExpression status;
	public final TimestampExpression lastCheck;
	public final TimestampExpression createdAt;
	
	public AgentCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		name = new StringExpression("name", this);
		address = new StringExpression("address", this);
		status = new StringExpression("status", this);
		lastCheck = new TimestampExpression("lastCheck", this);
		createdAt = new TimestampExpression("createdAt", this);
	}
	
	public AgentCriteria(PersistentSession session) {
		this(session.createCriteria(Agent.class));
	}
	
	public AgentCriteria() throws PersistentException {
		this(orm.Hibernatev3PersistentManager.instance().getSession());
	}
	
	public ComponentCriteria createComponentCriteria() {
		return new ComponentCriteria(createCriteria("component"));
	}
	
	public Agent uniqueAgent() {
		return (Agent) super.uniqueResult();
	}
	
	public Agent[] listAgent() {
		java.util.List list = super.list();
		return (Agent[]) list.toArray(new Agent[list.size()]);
	}
}

