/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

public class Agent {
	public Agent() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == classes.ORMConstants.KEY_AGENT_COMPONENT) {
			this.component = (classes.Component) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private String name;
	
	private String address;
	
	private classes.Component component;
	
	private String status;
	
	private java.sql.Timestamp lastCheck;
	
	private java.sql.Timestamp createdAt;
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setName(String value) {
		this.name = value;
	}
	
	public String getName() {
		return name;
	}
	
	public void setAddress(String value) {
		this.address = value;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setStatus(String value) {
		this.status = value;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setLastCheck(java.sql.Timestamp value) {
		this.lastCheck = value;
	}
	
	public java.sql.Timestamp getLastCheck() {
		return lastCheck;
	}
	
	public void setCreatedAt(java.sql.Timestamp value) {
		this.createdAt = value;
	}
	
	public java.sql.Timestamp getCreatedAt() {
		return createdAt;
	}
	
	public void setComponent(classes.Component value) {
		if (component != null) {
			component.agent.remove(this);
		}
		if (value != null) {
			value.agent.add(this);
		}
	}
	
	public classes.Component getComponent() {
		return component;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Component(classes.Component value) {
		this.component = value;
	}
	
	private classes.Component getORM_Component() {
		return component;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
