/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class AuthorityCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression authority;
	public final TimestampExpression createdAt;
	
	public AuthorityCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		authority = new StringExpression("authority", this);
		createdAt = new TimestampExpression("createdAt", this);
	}
	
	public AuthorityCriteria(PersistentSession session) {
		this(session.createCriteria(Authority.class));
	}
	
	public AuthorityCriteria() throws PersistentException {
		this(orm.Hibernatev3PersistentManager.instance().getSession());
	}
	
	public UserCriteria createUserCriteria() {
		return new UserCriteria(createCriteria("user"));
	}
	
	public Authority uniqueAuthority() {
		return (Authority) super.uniqueResult();
	}
	
	public Authority[] listAuthority() {
		java.util.List list = super.list();
		return (Authority[]) list.toArray(new Authority[list.size()]);
	}
}

