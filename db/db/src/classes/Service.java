/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

public class Service {
	public Service() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == classes.ORMConstants.KEY_SERVICE_APPLICATION) {
			return ORM_application;
		}
		else if (key == classes.ORMConstants.KEY_SERVICE_WORKER) {
			return ORM_worker;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == classes.ORMConstants.KEY_SERVICE_USER) {
			this.user = (classes.User) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private java.util.Date initDate;
	
	private short duration;
	
	private String name;
	
	private String description;
	
	private java.math.BigDecimal price;
	
	private classes.User user;
	
	private java.sql.Timestamp createdAt;
	
	private java.util.Set ORM_application = new java.util.HashSet();
	
	private java.util.Set ORM_worker = new java.util.HashSet();
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setInitDate(java.util.Date value) {
		this.initDate = value;
	}
	
	public java.util.Date getInitDate() {
		return initDate;
	}
	
	public void setDuration(short value) {
		this.duration = value;
	}
	
	public short getDuration() {
		return duration;
	}
	
	public void setName(String value) {
		this.name = value;
	}
	
	public String getName() {
		return name;
	}
	
	public void setDescription(String value) {
		this.description = value;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setPrice(java.math.BigDecimal value) {
		this.price = value;
	}
	
	public java.math.BigDecimal getPrice() {
		return price;
	}
	
	public void setCreatedAt(java.sql.Timestamp value) {
		this.createdAt = value;
	}
	
	public java.sql.Timestamp getCreatedAt() {
		return createdAt;
	}
	
	public void setUser(classes.User value) {
		if (user != null) {
			user.service.remove(this);
		}
		if (value != null) {
			value.service.add(this);
		}
	}
	
	public classes.User getUser() {
		return user;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_User(classes.User value) {
		this.user = value;
	}
	
	private classes.User getORM_User() {
		return user;
	}
	
	private void setORM_Application(java.util.Set value) {
		this.ORM_application = value;
	}
	
	private java.util.Set getORM_Application() {
		return ORM_application;
	}
	
	public final classes.ApplicationSetCollection application = new classes.ApplicationSetCollection(this, _ormAdapter, classes.ORMConstants.KEY_SERVICE_APPLICATION, classes.ORMConstants.KEY_APPLICATION_SERVICE, classes.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Worker(java.util.Set value) {
		this.ORM_worker = value;
	}
	
	private java.util.Set getORM_Worker() {
		return ORM_worker;
	}
	
	public final classes.WorkerSetCollection worker = new classes.WorkerSetCollection(this, _ormAdapter, classes.ORMConstants.KEY_SERVICE_WORKER, classes.ORMConstants.KEY_WORKER_SERVICE, classes.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
