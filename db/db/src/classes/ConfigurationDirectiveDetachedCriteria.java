/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ConfigurationDirectiveDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression name;
	public final StringExpression value;
	public final TimestampExpression createdAt;
	
	public ConfigurationDirectiveDetachedCriteria() {
		super(classes.ConfigurationDirective.class, classes.ConfigurationDirectiveCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		value = new StringExpression("value", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
	}
	
	public ConfigurationDirectiveDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, classes.ConfigurationDirectiveCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		value = new StringExpression("value", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
	}
	
	public WorkerDetachedCriteria createWorkerCriteria() {
		return new WorkerDetachedCriteria(createCriteria("worker"));
	}
	
	public ConfigurationDirective uniqueConfigurationDirective(PersistentSession session) {
		return (ConfigurationDirective) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public ConfigurationDirective[] listConfigurationDirective(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (ConfigurationDirective[]) list.toArray(new ConfigurationDirective[list.size()]);
	}
}

