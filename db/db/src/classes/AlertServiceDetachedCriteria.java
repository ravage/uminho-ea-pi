/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class AlertServiceDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression name;
	public final StringExpression description;
	public final TimestampExpression createdAt;
	
	public AlertServiceDetachedCriteria() {
		super(classes.AlertService.class, classes.AlertServiceCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		description = new StringExpression("description", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
	}
	
	public AlertServiceDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, classes.AlertServiceCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		description = new StringExpression("description", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
	}
	
	public AlertService uniqueAlertService(PersistentSession session) {
		return (AlertService) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public AlertService[] listAlertService(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (AlertService[]) list.toArray(new AlertService[list.size()]);
	}
}

