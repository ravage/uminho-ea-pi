/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

public class Application {
	public Application() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == classes.ORMConstants.KEY_APPLICATION_SERVICE) {
			this.service = (classes.Service) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private String name;
	
	private String fileLocation;
	
	private String url;
	
	private classes.Service service;
	
	private java.sql.Timestamp createdAt;
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setName(String value) {
		this.name = value;
	}
	
	public String getName() {
		return name;
	}
	
	public void setFileLocation(String value) {
		this.fileLocation = value;
	}
	
	public String getFileLocation() {
		return fileLocation;
	}
	
	public void setUrl(String value) {
		this.url = value;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setCreatedAt(java.sql.Timestamp value) {
		this.createdAt = value;
	}
	
	public java.sql.Timestamp getCreatedAt() {
		return createdAt;
	}
	
	public void setService(classes.Service value) {
		if (service != null) {
			service.application.remove(this);
		}
		if (value != null) {
			value.application.add(this);
		}
	}
	
	public classes.Service getService() {
		return service;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Service(classes.Service value) {
		this.service = value;
	}
	
	private classes.Service getORM_Service() {
		return service;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
