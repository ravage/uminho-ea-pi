/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ConfigurationDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression name;
	public final StringExpression template;
	public final TimestampExpression createdAt;
	
	public ConfigurationDetachedCriteria() {
		super(classes.Configuration.class, classes.ConfigurationCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		template = new StringExpression("template", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
	}
	
	public ConfigurationDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, classes.ConfigurationCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		template = new StringExpression("template", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
	}
	
	public ComponentDetachedCriteria createComponentCriteria() {
		return new ComponentDetachedCriteria(createCriteria("component"));
	}
	
	public Configuration uniqueConfiguration(PersistentSession session) {
		return (Configuration) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Configuration[] listConfiguration(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Configuration[]) list.toArray(new Configuration[list.size()]);
	}
}

