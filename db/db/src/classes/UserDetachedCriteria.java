/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class UserDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression name;
	public final BooleanExpression gender;
	public final DateExpression birthdate;
	public final StringExpression username;
	public final StringExpression password;
	public final StringExpression email;
	public final StringExpression nif;
	public final StringExpression activationKey;
	public final BooleanExpression active;
	public final TimestampExpression createdAt;
	
	public UserDetachedCriteria() {
		super(classes.User.class, classes.UserCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		gender = new BooleanExpression("gender", this.getDetachedCriteria());
		birthdate = new DateExpression("birthdate", this.getDetachedCriteria());
		username = new StringExpression("username", this.getDetachedCriteria());
		password = new StringExpression("password", this.getDetachedCriteria());
		email = new StringExpression("email", this.getDetachedCriteria());
		nif = new StringExpression("nif", this.getDetachedCriteria());
		activationKey = new StringExpression("activationKey", this.getDetachedCriteria());
		active = new BooleanExpression("active", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
	}
	
	public UserDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, classes.UserCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		gender = new BooleanExpression("gender", this.getDetachedCriteria());
		birthdate = new DateExpression("birthdate", this.getDetachedCriteria());
		username = new StringExpression("username", this.getDetachedCriteria());
		password = new StringExpression("password", this.getDetachedCriteria());
		email = new StringExpression("email", this.getDetachedCriteria());
		nif = new StringExpression("nif", this.getDetachedCriteria());
		activationKey = new StringExpression("activationKey", this.getDetachedCriteria());
		active = new BooleanExpression("active", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
	}
	
	public ServiceDetachedCriteria createServiceCriteria() {
		return new ServiceDetachedCriteria(createCriteria("ORM_Service"));
	}
	
	public AuthorityDetachedCriteria createAuthorityCriteria() {
		return new AuthorityDetachedCriteria(createCriteria("ORM_Authority"));
	}
	
	public User uniqueUser(PersistentSession session) {
		return (User) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public User[] listUser(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (User[]) list.toArray(new User[list.size()]);
	}
}

