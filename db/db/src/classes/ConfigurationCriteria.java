/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ConfigurationCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression name;
	public final StringExpression template;
	public final TimestampExpression createdAt;
	
	public ConfigurationCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		name = new StringExpression("name", this);
		template = new StringExpression("template", this);
		createdAt = new TimestampExpression("createdAt", this);
	}
	
	public ConfigurationCriteria(PersistentSession session) {
		this(session.createCriteria(Configuration.class));
	}
	
	public ConfigurationCriteria() throws PersistentException {
		this(orm.Hibernatev3PersistentManager.instance().getSession());
	}
	
	public ComponentCriteria createComponentCriteria() {
		return new ComponentCriteria(createCriteria("component"));
	}
	
	public Configuration uniqueConfiguration() {
		return (Configuration) super.uniqueResult();
	}
	
	public Configuration[] listConfiguration() {
		java.util.List list = super.list();
		return (Configuration[]) list.toArray(new Configuration[list.size()]);
	}
}

