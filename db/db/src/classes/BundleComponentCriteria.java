/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class BundleComponentCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final IntegerExpression quantity;
	public final TimestampExpression createdAt;
	
	public BundleComponentCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		quantity = new IntegerExpression("quantity", this);
		createdAt = new TimestampExpression("createdAt", this);
	}
	
	public BundleComponentCriteria(PersistentSession session) {
		this(session.createCriteria(BundleComponent.class));
	}
	
	public BundleComponentCriteria() throws PersistentException {
		this(orm.Hibernatev3PersistentManager.instance().getSession());
	}
	
	public BundleCriteria createBundleCriteria() {
		return new BundleCriteria(createCriteria("bundle"));
	}
	
	public ComponentCriteria createComponentCriteria() {
		return new ComponentCriteria(createCriteria("component"));
	}
	
	public BundleComponent uniqueBundleComponent() {
		return (BundleComponent) super.uniqueResult();
	}
	
	public BundleComponent[] listBundleComponent() {
		java.util.List list = super.list();
		return (BundleComponent[]) list.toArray(new BundleComponent[list.size()]);
	}
}

