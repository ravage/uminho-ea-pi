/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class BundleCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression name;
	public final BigDecimalExpression price;
	public final ShortExpression duration;
	public final TimestampExpression createdAt;
	
	public BundleCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		name = new StringExpression("name", this);
		price = new BigDecimalExpression("price", this);
		duration = new ShortExpression("duration", this);
		createdAt = new TimestampExpression("createdAt", this);
	}
	
	public BundleCriteria(PersistentSession session) {
		this(session.createCriteria(Bundle.class));
	}
	
	public BundleCriteria() throws PersistentException {
		this(orm.Hibernatev3PersistentManager.instance().getSession());
	}
	
	public BundleComponentCriteria createBundleComponentCriteria() {
		return new BundleComponentCriteria(createCriteria("ORM_BundleComponent"));
	}
	
	public Bundle uniqueBundle() {
		return (Bundle) super.uniqueResult();
	}
	
	public Bundle[] listBundle() {
		java.util.List list = super.list();
		return (Bundle[]) list.toArray(new Bundle[list.size()]);
	}
}

