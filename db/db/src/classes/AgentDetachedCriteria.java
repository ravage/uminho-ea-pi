/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class AgentDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression name;
	public final StringExpression address;
	public final StringExpression status;
	public final TimestampExpression lastCheck;
	public final TimestampExpression createdAt;
	
	public AgentDetachedCriteria() {
		super(classes.Agent.class, classes.AgentCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		address = new StringExpression("address", this.getDetachedCriteria());
		status = new StringExpression("status", this.getDetachedCriteria());
		lastCheck = new TimestampExpression("lastCheck", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
	}
	
	public AgentDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, classes.AgentCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		address = new StringExpression("address", this.getDetachedCriteria());
		status = new StringExpression("status", this.getDetachedCriteria());
		lastCheck = new TimestampExpression("lastCheck", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
	}
	
	public ComponentDetachedCriteria createComponentCriteria() {
		return new ComponentDetachedCriteria(createCriteria("component"));
	}
	
	public Agent uniqueAgent(PersistentSession session) {
		return (Agent) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Agent[] listAgent(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Agent[]) list.toArray(new Agent[list.size()]);
	}
}

