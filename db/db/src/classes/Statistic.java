/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

public class Statistic {
	public Statistic() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == classes.ORMConstants.KEY_STATISTIC_WORKER) {
			this.worker = (classes.Worker) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private String name;
	
	private java.util.Date statisticsDate;
	
	private java.math.BigDecimal percentageUse;
	
	private String observations;
	
	private classes.Worker worker;
	
	private java.sql.Timestamp createdAt;
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setName(String value) {
		this.name = value;
	}
	
	public String getName() {
		return name;
	}
	
	public void setStatisticsDate(java.util.Date value) {
		this.statisticsDate = value;
	}
	
	public java.util.Date getStatisticsDate() {
		return statisticsDate;
	}
	
	public void setPercentageUse(java.math.BigDecimal value) {
		this.percentageUse = value;
	}
	
	public java.math.BigDecimal getPercentageUse() {
		return percentageUse;
	}
	
	public void setObservations(String value) {
		this.observations = value;
	}
	
	public String getObservations() {
		return observations;
	}
	
	public void setCreatedAt(java.sql.Timestamp value) {
		this.createdAt = value;
	}
	
	public java.sql.Timestamp getCreatedAt() {
		return createdAt;
	}
	
	public void setWorker(classes.Worker value) {
		if (worker != null) {
			worker.statistic.remove(this);
		}
		if (value != null) {
			value.statistic.add(this);
		}
	}
	
	public classes.Worker getWorker() {
		return worker;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Worker(classes.Worker value) {
		this.worker = value;
	}
	
	private classes.Worker getORM_Worker() {
		return worker;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
