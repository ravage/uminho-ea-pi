/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

public class User {
	public User() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == classes.ORMConstants.KEY_USER_SERVICE) {
			return ORM_service;
		}
		else if (key == classes.ORMConstants.KEY_USER_AUTHORITY) {
			return ORM_authority;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int id;
	
	private String name;
	
	private Boolean gender;
	
	private java.util.Date birthdate;
	
	private String username;
	
	private String password;
	
	private String email;
	
	private String nif;
	
	private String activationKey;
	
	private Boolean active;
	
	private java.sql.Timestamp createdAt;
	
	private java.util.Set ORM_service = new java.util.HashSet();
	
	private java.util.Set ORM_authority = new java.util.HashSet();
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setName(String value) {
		this.name = value;
	}
	
	public String getName() {
		return name;
	}
	
	public void setGender(boolean value) {
		setGender(new Boolean(value));
	}
	
	public void setGender(Boolean value) {
		this.gender = value;
	}
	
	public Boolean getGender() {
		return gender;
	}
	
	public void setBirthdate(java.util.Date value) {
		this.birthdate = value;
	}
	
	public java.util.Date getBirthdate() {
		return birthdate;
	}
	
	public void setUsername(String value) {
		this.username = value;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setPassword(String value) {
		this.password = value;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setEmail(String value) {
		this.email = value;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setNif(String value) {
		this.nif = value;
	}
	
	public String getNif() {
		return nif;
	}
	
	public void setActivationKey(String value) {
		this.activationKey = value;
	}
	
	public String getActivationKey() {
		return activationKey;
	}
	
	public void setActive(boolean value) {
		setActive(new Boolean(value));
	}
	
	public void setActive(Boolean value) {
		this.active = value;
	}
	
	public Boolean getActive() {
		return active;
	}
	
	public void setCreatedAt(java.sql.Timestamp value) {
		this.createdAt = value;
	}
	
	public java.sql.Timestamp getCreatedAt() {
		return createdAt;
	}
	
	private void setORM_Service(java.util.Set value) {
		this.ORM_service = value;
	}
	
	private java.util.Set getORM_Service() {
		return ORM_service;
	}
	
	public final classes.ServiceSetCollection service = new classes.ServiceSetCollection(this, _ormAdapter, classes.ORMConstants.KEY_USER_SERVICE, classes.ORMConstants.KEY_SERVICE_USER, classes.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Authority(java.util.Set value) {
		this.ORM_authority = value;
	}
	
	private java.util.Set getORM_Authority() {
		return ORM_authority;
	}
	
	public final classes.AuthoritySetCollection authority = new classes.AuthoritySetCollection(this, _ormAdapter, classes.ORMConstants.KEY_USER_AUTHORITY, classes.ORMConstants.KEY_AUTHORITY_USER, classes.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
