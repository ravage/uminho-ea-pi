/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ServiceDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final DateExpression initDate;
	public final ShortExpression duration;
	public final StringExpression name;
	public final StringExpression description;
	public final BigDecimalExpression price;
	public final TimestampExpression createdAt;
	
	public ServiceDetachedCriteria() {
		super(classes.Service.class, classes.ServiceCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		initDate = new DateExpression("initDate", this.getDetachedCriteria());
		duration = new ShortExpression("duration", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		description = new StringExpression("description", this.getDetachedCriteria());
		price = new BigDecimalExpression("price", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
	}
	
	public ServiceDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, classes.ServiceCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		initDate = new DateExpression("initDate", this.getDetachedCriteria());
		duration = new ShortExpression("duration", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		description = new StringExpression("description", this.getDetachedCriteria());
		price = new BigDecimalExpression("price", this.getDetachedCriteria());
		createdAt = new TimestampExpression("createdAt", this.getDetachedCriteria());
	}
	
	public UserDetachedCriteria createUserCriteria() {
		return new UserDetachedCriteria(createCriteria("user"));
	}
	
	public ApplicationDetachedCriteria createApplicationCriteria() {
		return new ApplicationDetachedCriteria(createCriteria("ORM_Application"));
	}
	
	public WorkerDetachedCriteria createWorkerCriteria() {
		return new WorkerDetachedCriteria(createCriteria("ORM_Worker"));
	}
	
	public Service uniqueService(PersistentSession session) {
		return (Service) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Service[] listService(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Service[]) list.toArray(new Service[list.size()]);
	}
}

