/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class WorkerCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final LongExpression quantity;
	public final LongExpression percentageLimit;
	public final TimestampExpression createdAt;
	
	public WorkerCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		quantity = new LongExpression("quantity", this);
		percentageLimit = new LongExpression("percentageLimit", this);
		createdAt = new TimestampExpression("createdAt", this);
	}
	
	public WorkerCriteria(PersistentSession session) {
		this(session.createCriteria(Worker.class));
	}
	
	public WorkerCriteria() throws PersistentException {
		this(orm.Hibernatev3PersistentManager.instance().getSession());
	}
	
	public ServiceCriteria createServiceCriteria() {
		return new ServiceCriteria(createCriteria("service"));
	}
	
	public ComponentCriteria createComponentCriteria() {
		return new ComponentCriteria(createCriteria("component"));
	}
	
	public StatisticCriteria createStatisticCriteria() {
		return new StatisticCriteria(createCriteria("ORM_Statistic"));
	}
	
	public ConfigurationDirectiveCriteria createConfigurationDirectiveCriteria() {
		return new ConfigurationDirectiveCriteria(createCriteria("ORM_ConfigurationDirective"));
	}
	
	public Worker uniqueWorker() {
		return (Worker) super.uniqueResult();
	}
	
	public Worker[] listWorker() {
		java.util.List list = super.list();
		return (Worker[]) list.toArray(new Worker[list.size()]);
	}
}

