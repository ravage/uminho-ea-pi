/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DirectiveCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression name;
	public final StringExpression value;
	public final TimestampExpression createdAt;
	
	public DirectiveCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		name = new StringExpression("name", this);
		value = new StringExpression("value", this);
		createdAt = new TimestampExpression("createdAt", this);
	}
	
	public DirectiveCriteria(PersistentSession session) {
		this(session.createCriteria(Directive.class));
	}
	
	public DirectiveCriteria() throws PersistentException {
		this(orm.Hibernatev3PersistentManager.instance().getSession());
	}
	
	public ComponentCriteria createComponentCriteria() {
		return new ComponentCriteria(createCriteria("component"));
	}
	
	public Directive uniqueDirective() {
		return (Directive) super.uniqueResult();
	}
	
	public Directive[] listDirective() {
		java.util.List list = super.list();
		return (Directive[]) list.toArray(new Directive[list.size()]);
	}
}

