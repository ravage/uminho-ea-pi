/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

public class Directive {
	public Directive() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == classes.ORMConstants.KEY_DIRECTIVE_COMPONENT) {
			this.component = (classes.Component) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private String name;
	
	private String value;
	
	private classes.Component component;
	
	private java.sql.Timestamp createdAt;
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setName(String value) {
		this.name = value;
	}
	
	public String getName() {
		return name;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setCreatedAt(java.sql.Timestamp value) {
		this.createdAt = value;
	}
	
	public java.sql.Timestamp getCreatedAt() {
		return createdAt;
	}
	
	public void setComponent(classes.Component value) {
		if (component != null) {
			component.directive.remove(this);
		}
		if (value != null) {
			value.directive.add(this);
		}
	}
	
	public classes.Component getComponent() {
		return component;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Component(classes.Component value) {
		this.component = value;
	}
	
	private classes.Component getORM_Component() {
		return component;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
