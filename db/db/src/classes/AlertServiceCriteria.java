/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package classes;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class AlertServiceCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression name;
	public final StringExpression description;
	public final TimestampExpression createdAt;
	
	public AlertServiceCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		name = new StringExpression("name", this);
		description = new StringExpression("description", this);
		createdAt = new TimestampExpression("createdAt", this);
	}
	
	public AlertServiceCriteria(PersistentSession session) {
		this(session.createCriteria(AlertService.class));
	}
	
	public AlertServiceCriteria() throws PersistentException {
		this(orm.Hibernatev3PersistentManager.instance().getSession());
	}
	
	public AlertService uniqueAlertService() {
		return (AlertService) super.uniqueResult();
	}
	
	public AlertService[] listAlertService() {
		java.util.List list = super.list();
		return (AlertService[]) list.toArray(new AlertService[list.size()]);
	}
}

