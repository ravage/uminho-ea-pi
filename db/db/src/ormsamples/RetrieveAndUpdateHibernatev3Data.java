/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package ormsamples;

import org.orm.*;
public class RetrieveAndUpdateHibernatev3Data {
	public void retrieveAndUpdateTestData() throws PersistentException {
		PersistentTransaction t = orm.Hibernatev3PersistentManager.instance().getSession().beginTransaction();
		try {
			classes.User lclassesUser= (classes.User)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.User").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			orm.Hibernatev3PersistentManager.instance().getSession().update(lclassesUser);
			
			classes.Component lclassesComponent= (classes.Component)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Component").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			orm.Hibernatev3PersistentManager.instance().getSession().update(lclassesComponent);
			
			classes.Application lclassesApplication= (classes.Application)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Application").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			orm.Hibernatev3PersistentManager.instance().getSession().update(lclassesApplication);
			
			classes.Service lclassesService= (classes.Service)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Service").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			orm.Hibernatev3PersistentManager.instance().getSession().update(lclassesService);
			
			classes.Worker lclassesWorker= (classes.Worker)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Worker").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			orm.Hibernatev3PersistentManager.instance().getSession().update(lclassesWorker);
			
			classes.Statistic lclassesStatistic= (classes.Statistic)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Statistic").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			orm.Hibernatev3PersistentManager.instance().getSession().update(lclassesStatistic);
			
			classes.AlertService lclassesAlertService= (classes.AlertService)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.AlertService").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			orm.Hibernatev3PersistentManager.instance().getSession().update(lclassesAlertService);
			
			classes.Bundle lclassesBundle= (classes.Bundle)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Bundle").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			orm.Hibernatev3PersistentManager.instance().getSession().update(lclassesBundle);
			
			classes.BundleComponent lclassesBundleComponent= (classes.BundleComponent)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.BundleComponent").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			orm.Hibernatev3PersistentManager.instance().getSession().update(lclassesBundleComponent);
			
			classes.Configuration lclassesConfiguration= (classes.Configuration)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Configuration").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			orm.Hibernatev3PersistentManager.instance().getSession().update(lclassesConfiguration);
			
			classes.ConfigurationDirective lclassesConfigurationDirective= (classes.ConfigurationDirective)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.ConfigurationDirective").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			orm.Hibernatev3PersistentManager.instance().getSession().update(lclassesConfigurationDirective);
			
			classes.Agent lclassesAgent= (classes.Agent)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Agent").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			orm.Hibernatev3PersistentManager.instance().getSession().update(lclassesAgent);
			
			classes.Directive lclassesDirective= (classes.Directive)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Directive").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			orm.Hibernatev3PersistentManager.instance().getSession().update(lclassesDirective);
			
			classes.Authority lclassesAuthority= (classes.Authority)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Authority").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			orm.Hibernatev3PersistentManager.instance().getSession().update(lclassesAuthority);
			
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}
	
	public void retrieveByCriteria() throws PersistentException {
		System.out.println("Retrieving User by UserCriteria");
		classes.UserCriteria userCriteria = new classes.UserCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//userCriteria.id.eq();
		System.out.println(userCriteria.uniqueUser());
		
		System.out.println("Retrieving Component by ComponentCriteria");
		classes.ComponentCriteria componentCriteria = new classes.ComponentCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//componentCriteria.id.eq();
		System.out.println(componentCriteria.uniqueComponent());
		
		System.out.println("Retrieving Application by ApplicationCriteria");
		classes.ApplicationCriteria applicationCriteria = new classes.ApplicationCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//applicationCriteria.id.eq();
		System.out.println(applicationCriteria.uniqueApplication());
		
		System.out.println("Retrieving Service by ServiceCriteria");
		classes.ServiceCriteria serviceCriteria = new classes.ServiceCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//serviceCriteria.id.eq();
		System.out.println(serviceCriteria.uniqueService());
		
		System.out.println("Retrieving Worker by WorkerCriteria");
		classes.WorkerCriteria workerCriteria = new classes.WorkerCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//workerCriteria.id.eq();
		System.out.println(workerCriteria.uniqueWorker());
		
		System.out.println("Retrieving Statistic by StatisticCriteria");
		classes.StatisticCriteria statisticCriteria = new classes.StatisticCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//statisticCriteria.id.eq();
		System.out.println(statisticCriteria.uniqueStatistic());
		
		System.out.println("Retrieving AlertService by AlertServiceCriteria");
		classes.AlertServiceCriteria alertServiceCriteria = new classes.AlertServiceCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//alertServiceCriteria.id.eq();
		System.out.println(alertServiceCriteria.uniqueAlertService());
		
		System.out.println("Retrieving Bundle by BundleCriteria");
		classes.BundleCriteria bundleCriteria = new classes.BundleCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//bundleCriteria.id.eq();
		System.out.println(bundleCriteria.uniqueBundle());
		
		System.out.println("Retrieving BundleComponent by BundleComponentCriteria");
		classes.BundleComponentCriteria bundleComponentCriteria = new classes.BundleComponentCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//bundleComponentCriteria.id.eq();
		System.out.println(bundleComponentCriteria.uniqueBundleComponent());
		
		System.out.println("Retrieving Configuration by ConfigurationCriteria");
		classes.ConfigurationCriteria configurationCriteria = new classes.ConfigurationCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//configurationCriteria.id.eq();
		System.out.println(configurationCriteria.uniqueConfiguration());
		
		System.out.println("Retrieving ConfigurationDirective by ConfigurationDirectiveCriteria");
		classes.ConfigurationDirectiveCriteria configurationDirectiveCriteria = new classes.ConfigurationDirectiveCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//configurationDirectiveCriteria.id.eq();
		System.out.println(configurationDirectiveCriteria.uniqueConfigurationDirective());
		
		System.out.println("Retrieving Agent by AgentCriteria");
		classes.AgentCriteria agentCriteria = new classes.AgentCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//agentCriteria.id.eq();
		System.out.println(agentCriteria.uniqueAgent());
		
		System.out.println("Retrieving Directive by DirectiveCriteria");
		classes.DirectiveCriteria directiveCriteria = new classes.DirectiveCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//directiveCriteria.id.eq();
		System.out.println(directiveCriteria.uniqueDirective());
		
		System.out.println("Retrieving Authority by AuthorityCriteria");
		classes.AuthorityCriteria authorityCriteria = new classes.AuthorityCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//authorityCriteria.id.eq();
		System.out.println(authorityCriteria.uniqueAuthority());
		
	}
	
	
	public static void main(String[] args) {
		try {
			RetrieveAndUpdateHibernatev3Data retrieveAndUpdateHibernatev3Data = new RetrieveAndUpdateHibernatev3Data();
			try {
				retrieveAndUpdateHibernatev3Data.retrieveAndUpdateTestData();
				//retrieveAndUpdateHibernatev3Data.retrieveByCriteria();
			}
			finally {
				orm.Hibernatev3PersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
