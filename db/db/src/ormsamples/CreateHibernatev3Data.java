/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package ormsamples;

import org.orm.*;
public class CreateHibernatev3Data {
	public void createTestData() throws PersistentException {
		PersistentTransaction t = orm.Hibernatev3PersistentManager.instance().getSession().beginTransaction();
		try {
			classes.User lclassesUser = orm.classes.UserDAO.createUser();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : authority, service, email, password, username, name
			orm.Hibernatev3PersistentManager.instance().getSession().save(lclassesUser);
			
			classes.Component lclassesComponent = orm.classes.ComponentDAO.createComponent();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : directive, agent, configuration, bundleComponent, worker, name
			orm.Hibernatev3PersistentManager.instance().getSession().save(lclassesComponent);
			
			classes.Application lclassesApplication = orm.classes.ApplicationDAO.createApplication();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : service, url, fileLocation
			orm.Hibernatev3PersistentManager.instance().getSession().save(lclassesApplication);
			
			classes.Service lclassesService = orm.classes.ServiceDAO.createService();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : worker, application, user, price, name, duration, initDate
			orm.Hibernatev3PersistentManager.instance().getSession().save(lclassesService);
			
			classes.Worker lclassesWorker = orm.classes.WorkerDAO.createWorker();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : statistic, percentageLimit, quantity, component, service
			orm.Hibernatev3PersistentManager.instance().getSession().save(lclassesWorker);
			
			classes.Statistic lclassesStatistic = orm.classes.StatisticDAO.createStatistic();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : worker, percentageUse, statisticsDate, name
			orm.Hibernatev3PersistentManager.instance().getSession().save(lclassesStatistic);
			
			classes.AlertService lclassesAlertService = orm.classes.AlertServiceDAO.createAlertService();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : description, name
			orm.Hibernatev3PersistentManager.instance().getSession().save(lclassesAlertService);
			
			classes.Bundle lclassesBundle = orm.classes.BundleDAO.createBundle();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : bundleComponent, duration, price, name
			orm.Hibernatev3PersistentManager.instance().getSession().save(lclassesBundle);
			
			classes.BundleComponent lclassesBundleComponent = orm.classes.BundleComponentDAO.createBundleComponent();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : component, bundle
			orm.Hibernatev3PersistentManager.instance().getSession().save(lclassesBundleComponent);
			
			classes.Configuration lclassesConfiguration = orm.classes.ConfigurationDAO.createConfiguration();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : component, name
			orm.Hibernatev3PersistentManager.instance().getSession().save(lclassesConfiguration);
			
			classes.ConfigurationDirective lclassesConfigurationDirective = orm.classes.ConfigurationDirectiveDAO.createConfigurationDirective();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : value, name
			orm.Hibernatev3PersistentManager.instance().getSession().save(lclassesConfigurationDirective);
			
			classes.Agent lclassesAgent = orm.classes.AgentDAO.createAgent();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : lastCheck, component, name
			orm.Hibernatev3PersistentManager.instance().getSession().save(lclassesAgent);
			
			classes.Directive lclassesDirective = orm.classes.DirectiveDAO.createDirective();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : component, value, name
			orm.Hibernatev3PersistentManager.instance().getSession().save(lclassesDirective);
			
			classes.Authority lclassesAuthority = orm.classes.AuthorityDAO.createAuthority();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : user
			orm.Hibernatev3PersistentManager.instance().getSession().save(lclassesAuthority);
			
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			CreateHibernatev3Data createHibernatev3Data = new CreateHibernatev3Data();
			try {
				createHibernatev3Data.createTestData();
			}
			finally {
				orm.Hibernatev3PersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
