/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package ormsamples;

import org.orm.*;
public class DeleteHibernatev3Data {
	public void deleteTestData() throws PersistentException {
		PersistentTransaction t = orm.Hibernatev3PersistentManager.instance().getSession().beginTransaction();
		try {
			classes.User lclassesUser= (classes.User)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.User").setMaxResults(1).uniqueResult();
			orm.Hibernatev3PersistentManager.instance().getSession().delete(lclassesUser);
			
			classes.Component lclassesComponent= (classes.Component)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Component").setMaxResults(1).uniqueResult();
			orm.Hibernatev3PersistentManager.instance().getSession().delete(lclassesComponent);
			
			classes.Application lclassesApplication= (classes.Application)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Application").setMaxResults(1).uniqueResult();
			orm.Hibernatev3PersistentManager.instance().getSession().delete(lclassesApplication);
			
			classes.Service lclassesService= (classes.Service)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Service").setMaxResults(1).uniqueResult();
			orm.Hibernatev3PersistentManager.instance().getSession().delete(lclassesService);
			
			classes.Worker lclassesWorker= (classes.Worker)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Worker").setMaxResults(1).uniqueResult();
			orm.Hibernatev3PersistentManager.instance().getSession().delete(lclassesWorker);
			
			classes.Statistic lclassesStatistic= (classes.Statistic)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Statistic").setMaxResults(1).uniqueResult();
			orm.Hibernatev3PersistentManager.instance().getSession().delete(lclassesStatistic);
			
			classes.AlertService lclassesAlertService= (classes.AlertService)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.AlertService").setMaxResults(1).uniqueResult();
			orm.Hibernatev3PersistentManager.instance().getSession().delete(lclassesAlertService);
			
			classes.Bundle lclassesBundle= (classes.Bundle)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Bundle").setMaxResults(1).uniqueResult();
			orm.Hibernatev3PersistentManager.instance().getSession().delete(lclassesBundle);
			
			classes.BundleComponent lclassesBundleComponent= (classes.BundleComponent)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.BundleComponent").setMaxResults(1).uniqueResult();
			orm.Hibernatev3PersistentManager.instance().getSession().delete(lclassesBundleComponent);
			
			classes.Configuration lclassesConfiguration= (classes.Configuration)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Configuration").setMaxResults(1).uniqueResult();
			orm.Hibernatev3PersistentManager.instance().getSession().delete(lclassesConfiguration);
			
			classes.ConfigurationDirective lclassesConfigurationDirective= (classes.ConfigurationDirective)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.ConfigurationDirective").setMaxResults(1).uniqueResult();
			orm.Hibernatev3PersistentManager.instance().getSession().delete(lclassesConfigurationDirective);
			
			classes.Agent lclassesAgent= (classes.Agent)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Agent").setMaxResults(1).uniqueResult();
			orm.Hibernatev3PersistentManager.instance().getSession().delete(lclassesAgent);
			
			classes.Directive lclassesDirective= (classes.Directive)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Directive").setMaxResults(1).uniqueResult();
			orm.Hibernatev3PersistentManager.instance().getSession().delete(lclassesDirective);
			
			classes.Authority lclassesAuthority= (classes.Authority)orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Authority").setMaxResults(1).uniqueResult();
			orm.Hibernatev3PersistentManager.instance().getSession().delete(lclassesAuthority);
			
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}
	
	public static void main(String[] args) {
		try {
			DeleteHibernatev3Data deleteHibernatev3Data = new DeleteHibernatev3Data();
			try {
				deleteHibernatev3Data.deleteTestData();
			}
			finally {
				orm.Hibernatev3PersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
