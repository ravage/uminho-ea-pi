/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package ormsamples;

import org.orm.*;
public class ListHibernatev3Data {
	private static final int ROW_COUNT = 100;
	
	public void listTestData() throws PersistentException {
		System.out.println("Listing User...");
		java.util.List lUserList = orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.User").setMaxResults(ROW_COUNT).list();
		classes.User[] lclassesUsers = (classes.User[]) lUserList.toArray(new classes.User[lUserList.size()]);
		int length = Math.min(lclassesUsers.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lclassesUsers[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Component...");
		java.util.List lComponentList = orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Component").setMaxResults(ROW_COUNT).list();
		classes.Component[] lclassesComponents = (classes.Component[]) lComponentList.toArray(new classes.Component[lComponentList.size()]);
		length = Math.min(lclassesComponents.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lclassesComponents[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Application...");
		java.util.List lApplicationList = orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Application").setMaxResults(ROW_COUNT).list();
		classes.Application[] lclassesApplications = (classes.Application[]) lApplicationList.toArray(new classes.Application[lApplicationList.size()]);
		length = Math.min(lclassesApplications.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lclassesApplications[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Service...");
		java.util.List lServiceList = orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Service").setMaxResults(ROW_COUNT).list();
		classes.Service[] lclassesServices = (classes.Service[]) lServiceList.toArray(new classes.Service[lServiceList.size()]);
		length = Math.min(lclassesServices.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lclassesServices[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Worker...");
		java.util.List lWorkerList = orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Worker").setMaxResults(ROW_COUNT).list();
		classes.Worker[] lclassesWorkers = (classes.Worker[]) lWorkerList.toArray(new classes.Worker[lWorkerList.size()]);
		length = Math.min(lclassesWorkers.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lclassesWorkers[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Statistic...");
		java.util.List lStatisticList = orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Statistic").setMaxResults(ROW_COUNT).list();
		classes.Statistic[] lclassesStatistics = (classes.Statistic[]) lStatisticList.toArray(new classes.Statistic[lStatisticList.size()]);
		length = Math.min(lclassesStatistics.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lclassesStatistics[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing AlertService...");
		java.util.List lAlertServiceList = orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.AlertService").setMaxResults(ROW_COUNT).list();
		classes.AlertService[] lclassesAlertServices = (classes.AlertService[]) lAlertServiceList.toArray(new classes.AlertService[lAlertServiceList.size()]);
		length = Math.min(lclassesAlertServices.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lclassesAlertServices[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Bundle...");
		java.util.List lBundleList = orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Bundle").setMaxResults(ROW_COUNT).list();
		classes.Bundle[] lclassesBundles = (classes.Bundle[]) lBundleList.toArray(new classes.Bundle[lBundleList.size()]);
		length = Math.min(lclassesBundles.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lclassesBundles[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing BundleComponent...");
		java.util.List lBundleComponentList = orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.BundleComponent").setMaxResults(ROW_COUNT).list();
		classes.BundleComponent[] lclassesBundleComponents = (classes.BundleComponent[]) lBundleComponentList.toArray(new classes.BundleComponent[lBundleComponentList.size()]);
		length = Math.min(lclassesBundleComponents.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lclassesBundleComponents[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Configuration...");
		java.util.List lConfigurationList = orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Configuration").setMaxResults(ROW_COUNT).list();
		classes.Configuration[] lclassesConfigurations = (classes.Configuration[]) lConfigurationList.toArray(new classes.Configuration[lConfigurationList.size()]);
		length = Math.min(lclassesConfigurations.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lclassesConfigurations[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing ConfigurationDirective...");
		java.util.List lConfigurationDirectiveList = orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.ConfigurationDirective").setMaxResults(ROW_COUNT).list();
		classes.ConfigurationDirective[] lclassesConfigurationDirectives = (classes.ConfigurationDirective[]) lConfigurationDirectiveList.toArray(new classes.ConfigurationDirective[lConfigurationDirectiveList.size()]);
		length = Math.min(lclassesConfigurationDirectives.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lclassesConfigurationDirectives[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Agent...");
		java.util.List lAgentList = orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Agent").setMaxResults(ROW_COUNT).list();
		classes.Agent[] lclassesAgents = (classes.Agent[]) lAgentList.toArray(new classes.Agent[lAgentList.size()]);
		length = Math.min(lclassesAgents.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lclassesAgents[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Directive...");
		java.util.List lDirectiveList = orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Directive").setMaxResults(ROW_COUNT).list();
		classes.Directive[] lclassesDirectives = (classes.Directive[]) lDirectiveList.toArray(new classes.Directive[lDirectiveList.size()]);
		length = Math.min(lclassesDirectives.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lclassesDirectives[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Authority...");
		java.util.List lAuthorityList = orm.Hibernatev3PersistentManager.instance().getSession().createQuery("From classes.Authority").setMaxResults(ROW_COUNT).list();
		classes.Authority[] lclassesAuthoritys = (classes.Authority[]) lAuthorityList.toArray(new classes.Authority[lAuthorityList.size()]);
		length = Math.min(lclassesAuthoritys.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lclassesAuthoritys[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
	}
	
	public void listByCriteria() throws PersistentException  {
		System.out.println("Listing User by Criteria...");
		classes.UserCriteria userCriteria = new classes.UserCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//userCriteria.id.eq();
		userCriteria.setMaxResults(ROW_COUNT);
		classes.User[] classesUsers = userCriteria.listUser();
		int length =classesUsers== null ? 0 : Math.min(classesUsers.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(classesUsers[i]);
		}
		System.out.println(length + " User record(s) retrieved."); 
		
		System.out.println("Listing Component by Criteria...");
		classes.ComponentCriteria componentCriteria = new classes.ComponentCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//componentCriteria.id.eq();
		componentCriteria.setMaxResults(ROW_COUNT);
		classes.Component[] classesComponents = componentCriteria.listComponent();
		length =classesComponents== null ? 0 : Math.min(classesComponents.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(classesComponents[i]);
		}
		System.out.println(length + " Component record(s) retrieved."); 
		
		System.out.println("Listing Application by Criteria...");
		classes.ApplicationCriteria applicationCriteria = new classes.ApplicationCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//applicationCriteria.id.eq();
		applicationCriteria.setMaxResults(ROW_COUNT);
		classes.Application[] classesApplications = applicationCriteria.listApplication();
		length =classesApplications== null ? 0 : Math.min(classesApplications.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(classesApplications[i]);
		}
		System.out.println(length + " Application record(s) retrieved."); 
		
		System.out.println("Listing Service by Criteria...");
		classes.ServiceCriteria serviceCriteria = new classes.ServiceCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//serviceCriteria.id.eq();
		serviceCriteria.setMaxResults(ROW_COUNT);
		classes.Service[] classesServices = serviceCriteria.listService();
		length =classesServices== null ? 0 : Math.min(classesServices.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(classesServices[i]);
		}
		System.out.println(length + " Service record(s) retrieved."); 
		
		System.out.println("Listing Worker by Criteria...");
		classes.WorkerCriteria workerCriteria = new classes.WorkerCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//workerCriteria.id.eq();
		workerCriteria.setMaxResults(ROW_COUNT);
		classes.Worker[] classesWorkers = workerCriteria.listWorker();
		length =classesWorkers== null ? 0 : Math.min(classesWorkers.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(classesWorkers[i]);
		}
		System.out.println(length + " Worker record(s) retrieved."); 
		
		System.out.println("Listing Statistic by Criteria...");
		classes.StatisticCriteria statisticCriteria = new classes.StatisticCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//statisticCriteria.id.eq();
		statisticCriteria.setMaxResults(ROW_COUNT);
		classes.Statistic[] classesStatistics = statisticCriteria.listStatistic();
		length =classesStatistics== null ? 0 : Math.min(classesStatistics.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(classesStatistics[i]);
		}
		System.out.println(length + " Statistic record(s) retrieved."); 
		
		System.out.println("Listing AlertService by Criteria...");
		classes.AlertServiceCriteria alertServiceCriteria = new classes.AlertServiceCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//alertServiceCriteria.id.eq();
		alertServiceCriteria.setMaxResults(ROW_COUNT);
		classes.AlertService[] classesAlertServices = alertServiceCriteria.listAlertService();
		length =classesAlertServices== null ? 0 : Math.min(classesAlertServices.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(classesAlertServices[i]);
		}
		System.out.println(length + " AlertService record(s) retrieved."); 
		
		System.out.println("Listing Bundle by Criteria...");
		classes.BundleCriteria bundleCriteria = new classes.BundleCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//bundleCriteria.id.eq();
		bundleCriteria.setMaxResults(ROW_COUNT);
		classes.Bundle[] classesBundles = bundleCriteria.listBundle();
		length =classesBundles== null ? 0 : Math.min(classesBundles.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(classesBundles[i]);
		}
		System.out.println(length + " Bundle record(s) retrieved."); 
		
		System.out.println("Listing BundleComponent by Criteria...");
		classes.BundleComponentCriteria bundleComponentCriteria = new classes.BundleComponentCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//bundleComponentCriteria.id.eq();
		bundleComponentCriteria.setMaxResults(ROW_COUNT);
		classes.BundleComponent[] classesBundleComponents = bundleComponentCriteria.listBundleComponent();
		length =classesBundleComponents== null ? 0 : Math.min(classesBundleComponents.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(classesBundleComponents[i]);
		}
		System.out.println(length + " BundleComponent record(s) retrieved."); 
		
		System.out.println("Listing Configuration by Criteria...");
		classes.ConfigurationCriteria configurationCriteria = new classes.ConfigurationCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//configurationCriteria.id.eq();
		configurationCriteria.setMaxResults(ROW_COUNT);
		classes.Configuration[] classesConfigurations = configurationCriteria.listConfiguration();
		length =classesConfigurations== null ? 0 : Math.min(classesConfigurations.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(classesConfigurations[i]);
		}
		System.out.println(length + " Configuration record(s) retrieved."); 
		
		System.out.println("Listing ConfigurationDirective by Criteria...");
		classes.ConfigurationDirectiveCriteria configurationDirectiveCriteria = new classes.ConfigurationDirectiveCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//configurationDirectiveCriteria.id.eq();
		configurationDirectiveCriteria.setMaxResults(ROW_COUNT);
		classes.ConfigurationDirective[] classesConfigurationDirectives = configurationDirectiveCriteria.listConfigurationDirective();
		length =classesConfigurationDirectives== null ? 0 : Math.min(classesConfigurationDirectives.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(classesConfigurationDirectives[i]);
		}
		System.out.println(length + " ConfigurationDirective record(s) retrieved."); 
		
		System.out.println("Listing Agent by Criteria...");
		classes.AgentCriteria agentCriteria = new classes.AgentCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//agentCriteria.id.eq();
		agentCriteria.setMaxResults(ROW_COUNT);
		classes.Agent[] classesAgents = agentCriteria.listAgent();
		length =classesAgents== null ? 0 : Math.min(classesAgents.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(classesAgents[i]);
		}
		System.out.println(length + " Agent record(s) retrieved."); 
		
		System.out.println("Listing Directive by Criteria...");
		classes.DirectiveCriteria directiveCriteria = new classes.DirectiveCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//directiveCriteria.id.eq();
		directiveCriteria.setMaxResults(ROW_COUNT);
		classes.Directive[] classesDirectives = directiveCriteria.listDirective();
		length =classesDirectives== null ? 0 : Math.min(classesDirectives.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(classesDirectives[i]);
		}
		System.out.println(length + " Directive record(s) retrieved."); 
		
		System.out.println("Listing Authority by Criteria...");
		classes.AuthorityCriteria authorityCriteria = new classes.AuthorityCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//authorityCriteria.id.eq();
		authorityCriteria.setMaxResults(ROW_COUNT);
		classes.Authority[] classesAuthoritys = authorityCriteria.listAuthority();
		length =classesAuthoritys== null ? 0 : Math.min(classesAuthoritys.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(classesAuthoritys[i]);
		}
		System.out.println(length + " Authority record(s) retrieved."); 
		
	}
	
	public static void main(String[] args) {
		try {
			ListHibernatev3Data listHibernatev3Data = new ListHibernatev3Data();
			try {
				listHibernatev3Data.listTestData();
				//listHibernatev3Data.listByCriteria();
			}
			finally {
				orm.Hibernatev3PersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
