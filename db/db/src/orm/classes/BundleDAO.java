/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package orm.classes;

import org.orm.*;
import org.hibernate.Query;
import java.util.List;
import classes.*;

public class BundleDAO {
	private static final org.apache.log4j.Logger _logger = org.apache.log4j.Logger.getLogger(BundleDAO.class);
	public static Bundle loadBundleByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadBundleByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("loadBundleByORMID(int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Bundle getBundleByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getBundleByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("getBundleByORMID(int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Bundle loadBundleByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadBundleByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadBundleByORMID(int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Bundle getBundleByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getBundleByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("getBundleByORMID(int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Bundle loadBundleByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Bundle) session.load(classes.Bundle.class, new Integer(id));
		}
		catch (Exception e) {
			_logger.error("loadBundleByORMID(PersistentSession session, int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Bundle getBundleByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Bundle) session.get(classes.Bundle.class, new Integer(id));
		}
		catch (Exception e) {
			_logger.error("getBundleByORMID(PersistentSession session, int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Bundle loadBundleByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Bundle) session.load(classes.Bundle.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("loadBundleByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Bundle getBundleByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Bundle) session.get(classes.Bundle.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("getBundleByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Bundle[] listBundleByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listBundleByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("listBundleByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Bundle[] listBundleByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listBundleByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("listBundleByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Bundle[] listBundleByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Bundle as Bundle");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			List list = query.list();
			return (Bundle[]) list.toArray(new Bundle[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listBundleByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Bundle[] listBundleByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Bundle as Bundle");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			List list = query.list();
			return (Bundle[]) list.toArray(new Bundle[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listBundleByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Bundle loadBundleByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadBundleByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("loadBundleByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Bundle loadBundleByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadBundleByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadBundleByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Bundle loadBundleByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Bundle[] bundles = listBundleByQuery(session, condition, orderBy);
		if (bundles != null && bundles.length > 0)
			return bundles[0];
		else
			return null;
	}
	
	public static Bundle loadBundleByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Bundle[] bundles = listBundleByQuery(session, condition, orderBy, lockMode);
		if (bundles != null && bundles.length > 0)
			return bundles[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateBundleByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateBundleByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("iterateBundleByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateBundleByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateBundleByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("iterateBundleByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateBundleByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Bundle as Bundle");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateBundleByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateBundleByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Bundle as Bundle");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateBundleByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Bundle createBundle() {
		return new classes.Bundle();
	}
	
	public static boolean save(classes.Bundle bundle) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().saveObject(bundle);
			return true;
		}
		catch (Exception e) {
			_logger.error("save(classes.Bundle bundle)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(classes.Bundle bundle) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().deleteObject(bundle);
			return true;
		}
		catch (Exception e) {
			_logger.error("delete(classes.Bundle bundle)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.Bundle bundle)throws PersistentException {
		try {
			classes.BundleComponent[] lBundleComponents = bundle.bundleComponent.toArray();
			for(int i = 0; i < lBundleComponents.length; i++) {
				lBundleComponents[i].setBundle(null);
			}
			return delete(bundle);
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.Bundle bundle, org.orm.PersistentSession session)throws PersistentException {
		try {
			classes.BundleComponent[] lBundleComponents = bundle.bundleComponent.toArray();
			for(int i = 0; i < lBundleComponents.length; i++) {
				lBundleComponents[i].setBundle(null);
			}
			try {
				session.delete(bundle);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(classes.Bundle bundle) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().refresh(bundle);
			return true;
		}
		catch (Exception e) {
			_logger.error("refresh(classes.Bundle bundle)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(classes.Bundle bundle) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().evict(bundle);
			return true;
		}
		catch (Exception e) {
			_logger.error("evict(classes.Bundle bundle)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Bundle loadBundleByCriteria(BundleCriteria bundleCriteria) {
		Bundle[] bundles = listBundleByCriteria(bundleCriteria);
		if(bundles == null || bundles.length == 0) {
			return null;
		}
		return bundles[0];
	}
	
	public static Bundle[] listBundleByCriteria(BundleCriteria bundleCriteria) {
		return bundleCriteria.listBundle();
	}
}
