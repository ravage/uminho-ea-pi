/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package orm.classes;

import org.orm.*;
import org.hibernate.Query;
import java.util.List;
import classes.*;

public class AlertServiceDAO {
	private static final org.apache.log4j.Logger _logger = org.apache.log4j.Logger.getLogger(AlertServiceDAO.class);
	public static AlertService loadAlertServiceByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadAlertServiceByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("loadAlertServiceByORMID(int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static AlertService getAlertServiceByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getAlertServiceByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("getAlertServiceByORMID(int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static AlertService loadAlertServiceByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadAlertServiceByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadAlertServiceByORMID(int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static AlertService getAlertServiceByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getAlertServiceByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("getAlertServiceByORMID(int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static AlertService loadAlertServiceByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (AlertService) session.load(classes.AlertService.class, new Integer(id));
		}
		catch (Exception e) {
			_logger.error("loadAlertServiceByORMID(PersistentSession session, int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static AlertService getAlertServiceByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (AlertService) session.get(classes.AlertService.class, new Integer(id));
		}
		catch (Exception e) {
			_logger.error("getAlertServiceByORMID(PersistentSession session, int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static AlertService loadAlertServiceByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (AlertService) session.load(classes.AlertService.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("loadAlertServiceByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static AlertService getAlertServiceByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (AlertService) session.get(classes.AlertService.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("getAlertServiceByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static AlertService[] listAlertServiceByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listAlertServiceByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("listAlertServiceByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static AlertService[] listAlertServiceByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listAlertServiceByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("listAlertServiceByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static AlertService[] listAlertServiceByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.AlertService as AlertService");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			List list = query.list();
			return (AlertService[]) list.toArray(new AlertService[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listAlertServiceByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static AlertService[] listAlertServiceByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.AlertService as AlertService");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			List list = query.list();
			return (AlertService[]) list.toArray(new AlertService[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listAlertServiceByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static AlertService loadAlertServiceByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadAlertServiceByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("loadAlertServiceByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static AlertService loadAlertServiceByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadAlertServiceByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadAlertServiceByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static AlertService loadAlertServiceByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		AlertService[] alertServices = listAlertServiceByQuery(session, condition, orderBy);
		if (alertServices != null && alertServices.length > 0)
			return alertServices[0];
		else
			return null;
	}
	
	public static AlertService loadAlertServiceByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		AlertService[] alertServices = listAlertServiceByQuery(session, condition, orderBy, lockMode);
		if (alertServices != null && alertServices.length > 0)
			return alertServices[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateAlertServiceByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateAlertServiceByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("iterateAlertServiceByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateAlertServiceByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateAlertServiceByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("iterateAlertServiceByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateAlertServiceByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.AlertService as AlertService");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateAlertServiceByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateAlertServiceByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.AlertService as AlertService");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateAlertServiceByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static AlertService createAlertService() {
		return new classes.AlertService();
	}
	
	public static boolean save(classes.AlertService alertService) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().saveObject(alertService);
			return true;
		}
		catch (Exception e) {
			_logger.error("save(classes.AlertService alertService)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(classes.AlertService alertService) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().deleteObject(alertService);
			return true;
		}
		catch (Exception e) {
			_logger.error("delete(classes.AlertService alertService)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(classes.AlertService alertService) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().refresh(alertService);
			return true;
		}
		catch (Exception e) {
			_logger.error("refresh(classes.AlertService alertService)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(classes.AlertService alertService) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().evict(alertService);
			return true;
		}
		catch (Exception e) {
			_logger.error("evict(classes.AlertService alertService)", e);
			throw new PersistentException(e);
		}
	}
	
	public static AlertService loadAlertServiceByCriteria(AlertServiceCriteria alertServiceCriteria) {
		AlertService[] alertServices = listAlertServiceByCriteria(alertServiceCriteria);
		if(alertServices == null || alertServices.length == 0) {
			return null;
		}
		return alertServices[0];
	}
	
	public static AlertService[] listAlertServiceByCriteria(AlertServiceCriteria alertServiceCriteria) {
		return alertServiceCriteria.listAlertService();
	}
}
