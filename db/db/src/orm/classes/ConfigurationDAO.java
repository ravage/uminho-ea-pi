/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package orm.classes;

import org.orm.*;
import org.hibernate.Query;
import java.util.List;
import classes.*;

public class ConfigurationDAO {
	private static final org.apache.log4j.Logger _logger = org.apache.log4j.Logger.getLogger(ConfigurationDAO.class);
	public static Configuration loadConfigurationByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadConfigurationByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("loadConfigurationByORMID(int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Configuration getConfigurationByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getConfigurationByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("getConfigurationByORMID(int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Configuration loadConfigurationByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadConfigurationByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadConfigurationByORMID(int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Configuration getConfigurationByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getConfigurationByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("getConfigurationByORMID(int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Configuration loadConfigurationByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Configuration) session.load(classes.Configuration.class, new Integer(id));
		}
		catch (Exception e) {
			_logger.error("loadConfigurationByORMID(PersistentSession session, int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Configuration getConfigurationByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Configuration) session.get(classes.Configuration.class, new Integer(id));
		}
		catch (Exception e) {
			_logger.error("getConfigurationByORMID(PersistentSession session, int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Configuration loadConfigurationByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Configuration) session.load(classes.Configuration.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("loadConfigurationByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Configuration getConfigurationByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Configuration) session.get(classes.Configuration.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("getConfigurationByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Configuration[] listConfigurationByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listConfigurationByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("listConfigurationByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Configuration[] listConfigurationByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listConfigurationByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("listConfigurationByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Configuration[] listConfigurationByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Configuration as Configuration");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			List list = query.list();
			return (Configuration[]) list.toArray(new Configuration[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listConfigurationByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Configuration[] listConfigurationByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Configuration as Configuration");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			List list = query.list();
			return (Configuration[]) list.toArray(new Configuration[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listConfigurationByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Configuration loadConfigurationByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadConfigurationByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("loadConfigurationByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Configuration loadConfigurationByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadConfigurationByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadConfigurationByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Configuration loadConfigurationByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Configuration[] configurations = listConfigurationByQuery(session, condition, orderBy);
		if (configurations != null && configurations.length > 0)
			return configurations[0];
		else
			return null;
	}
	
	public static Configuration loadConfigurationByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Configuration[] configurations = listConfigurationByQuery(session, condition, orderBy, lockMode);
		if (configurations != null && configurations.length > 0)
			return configurations[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateConfigurationByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateConfigurationByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("iterateConfigurationByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateConfigurationByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateConfigurationByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("iterateConfigurationByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateConfigurationByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Configuration as Configuration");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateConfigurationByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateConfigurationByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Configuration as Configuration");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateConfigurationByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Configuration createConfiguration() {
		return new classes.Configuration();
	}
	
	public static boolean save(classes.Configuration configuration) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().saveObject(configuration);
			return true;
		}
		catch (Exception e) {
			_logger.error("save(classes.Configuration configuration)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(classes.Configuration configuration) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().deleteObject(configuration);
			return true;
		}
		catch (Exception e) {
			_logger.error("delete(classes.Configuration configuration)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.Configuration configuration)throws PersistentException {
		try {
			if(configuration.getComponent() != null) {
				configuration.getComponent().configuration.remove(configuration);
			}
			
			return delete(configuration);
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.Configuration configuration, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(configuration.getComponent() != null) {
				configuration.getComponent().configuration.remove(configuration);
			}
			
			try {
				session.delete(configuration);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(classes.Configuration configuration) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().refresh(configuration);
			return true;
		}
		catch (Exception e) {
			_logger.error("refresh(classes.Configuration configuration)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(classes.Configuration configuration) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().evict(configuration);
			return true;
		}
		catch (Exception e) {
			_logger.error("evict(classes.Configuration configuration)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Configuration loadConfigurationByCriteria(ConfigurationCriteria configurationCriteria) {
		Configuration[] configurations = listConfigurationByCriteria(configurationCriteria);
		if(configurations == null || configurations.length == 0) {
			return null;
		}
		return configurations[0];
	}
	
	public static Configuration[] listConfigurationByCriteria(ConfigurationCriteria configurationCriteria) {
		return configurationCriteria.listConfiguration();
	}
}
