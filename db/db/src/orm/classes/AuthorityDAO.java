/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package orm.classes;

import org.orm.*;
import org.hibernate.Query;
import java.util.List;
import classes.*;

public class AuthorityDAO {
	private static final org.apache.log4j.Logger _logger = org.apache.log4j.Logger.getLogger(AuthorityDAO.class);
	public static Authority loadAuthorityByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadAuthorityByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("loadAuthorityByORMID(int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Authority getAuthorityByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getAuthorityByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("getAuthorityByORMID(int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Authority loadAuthorityByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadAuthorityByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadAuthorityByORMID(int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Authority getAuthorityByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getAuthorityByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("getAuthorityByORMID(int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Authority loadAuthorityByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Authority) session.load(classes.Authority.class, new Integer(id));
		}
		catch (Exception e) {
			_logger.error("loadAuthorityByORMID(PersistentSession session, int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Authority getAuthorityByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Authority) session.get(classes.Authority.class, new Integer(id));
		}
		catch (Exception e) {
			_logger.error("getAuthorityByORMID(PersistentSession session, int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Authority loadAuthorityByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Authority) session.load(classes.Authority.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("loadAuthorityByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Authority getAuthorityByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Authority) session.get(classes.Authority.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("getAuthorityByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Authority[] listAuthorityByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listAuthorityByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("listAuthorityByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Authority[] listAuthorityByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listAuthorityByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("listAuthorityByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Authority[] listAuthorityByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Authority as Authority");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			List list = query.list();
			return (Authority[]) list.toArray(new Authority[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listAuthorityByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Authority[] listAuthorityByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Authority as Authority");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			List list = query.list();
			return (Authority[]) list.toArray(new Authority[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listAuthorityByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Authority loadAuthorityByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadAuthorityByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("loadAuthorityByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Authority loadAuthorityByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadAuthorityByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadAuthorityByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Authority loadAuthorityByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Authority[] authoritys = listAuthorityByQuery(session, condition, orderBy);
		if (authoritys != null && authoritys.length > 0)
			return authoritys[0];
		else
			return null;
	}
	
	public static Authority loadAuthorityByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Authority[] authoritys = listAuthorityByQuery(session, condition, orderBy, lockMode);
		if (authoritys != null && authoritys.length > 0)
			return authoritys[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateAuthorityByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateAuthorityByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("iterateAuthorityByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateAuthorityByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateAuthorityByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("iterateAuthorityByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateAuthorityByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Authority as Authority");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateAuthorityByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateAuthorityByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Authority as Authority");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateAuthorityByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Authority createAuthority() {
		return new classes.Authority();
	}
	
	public static boolean save(classes.Authority authority) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().saveObject(authority);
			return true;
		}
		catch (Exception e) {
			_logger.error("save(classes.Authority authority)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(classes.Authority authority) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().deleteObject(authority);
			return true;
		}
		catch (Exception e) {
			_logger.error("delete(classes.Authority authority)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.Authority authority)throws PersistentException {
		try {
			if(authority.getUser() != null) {
				authority.getUser().authority.remove(authority);
			}
			
			return delete(authority);
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.Authority authority, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(authority.getUser() != null) {
				authority.getUser().authority.remove(authority);
			}
			
			try {
				session.delete(authority);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(classes.Authority authority) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().refresh(authority);
			return true;
		}
		catch (Exception e) {
			_logger.error("refresh(classes.Authority authority)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(classes.Authority authority) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().evict(authority);
			return true;
		}
		catch (Exception e) {
			_logger.error("evict(classes.Authority authority)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Authority loadAuthorityByCriteria(AuthorityCriteria authorityCriteria) {
		Authority[] authoritys = listAuthorityByCriteria(authorityCriteria);
		if(authoritys == null || authoritys.length == 0) {
			return null;
		}
		return authoritys[0];
	}
	
	public static Authority[] listAuthorityByCriteria(AuthorityCriteria authorityCriteria) {
		return authorityCriteria.listAuthority();
	}
}
