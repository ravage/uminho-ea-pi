/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package orm.classes;

import org.orm.*;
import org.hibernate.Query;
import java.util.List;
import classes.*;

public class WorkerDAO {
	private static final org.apache.log4j.Logger _logger = org.apache.log4j.Logger.getLogger(WorkerDAO.class);
	public static Worker loadWorkerByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadWorkerByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("loadWorkerByORMID(int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Worker getWorkerByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getWorkerByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("getWorkerByORMID(int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Worker loadWorkerByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadWorkerByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadWorkerByORMID(int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Worker getWorkerByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getWorkerByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("getWorkerByORMID(int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Worker loadWorkerByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Worker) session.load(classes.Worker.class, new Integer(id));
		}
		catch (Exception e) {
			_logger.error("loadWorkerByORMID(PersistentSession session, int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Worker getWorkerByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Worker) session.get(classes.Worker.class, new Integer(id));
		}
		catch (Exception e) {
			_logger.error("getWorkerByORMID(PersistentSession session, int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Worker loadWorkerByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Worker) session.load(classes.Worker.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("loadWorkerByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Worker getWorkerByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Worker) session.get(classes.Worker.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("getWorkerByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Worker[] listWorkerByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listWorkerByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("listWorkerByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Worker[] listWorkerByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listWorkerByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("listWorkerByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Worker[] listWorkerByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Worker as Worker");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			List list = query.list();
			return (Worker[]) list.toArray(new Worker[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listWorkerByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Worker[] listWorkerByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Worker as Worker");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			List list = query.list();
			return (Worker[]) list.toArray(new Worker[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listWorkerByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Worker loadWorkerByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadWorkerByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("loadWorkerByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Worker loadWorkerByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadWorkerByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadWorkerByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Worker loadWorkerByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Worker[] workers = listWorkerByQuery(session, condition, orderBy);
		if (workers != null && workers.length > 0)
			return workers[0];
		else
			return null;
	}
	
	public static Worker loadWorkerByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Worker[] workers = listWorkerByQuery(session, condition, orderBy, lockMode);
		if (workers != null && workers.length > 0)
			return workers[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateWorkerByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateWorkerByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("iterateWorkerByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateWorkerByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateWorkerByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("iterateWorkerByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateWorkerByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Worker as Worker");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateWorkerByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateWorkerByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Worker as Worker");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateWorkerByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Worker createWorker() {
		return new classes.Worker();
	}
	
	public static boolean save(classes.Worker worker) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().saveObject(worker);
			return true;
		}
		catch (Exception e) {
			_logger.error("save(classes.Worker worker)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(classes.Worker worker) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().deleteObject(worker);
			return true;
		}
		catch (Exception e) {
			_logger.error("delete(classes.Worker worker)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.Worker worker)throws PersistentException {
		try {
			if(worker.getService() != null) {
				worker.getService().worker.remove(worker);
			}
			
			if(worker.getComponent() != null) {
				worker.getComponent().worker.remove(worker);
			}
			
			classes.Statistic[] lStatistics = worker.statistic.toArray();
			for(int i = 0; i < lStatistics.length; i++) {
				lStatistics[i].setWorker(null);
			}
			classes.ConfigurationDirective[] lConfigurationDirectives = worker.configurationDirective.toArray();
			for(int i = 0; i < lConfigurationDirectives.length; i++) {
				lConfigurationDirectives[i].setWorker(null);
			}
			return delete(worker);
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.Worker worker, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(worker.getService() != null) {
				worker.getService().worker.remove(worker);
			}
			
			if(worker.getComponent() != null) {
				worker.getComponent().worker.remove(worker);
			}
			
			classes.Statistic[] lStatistics = worker.statistic.toArray();
			for(int i = 0; i < lStatistics.length; i++) {
				lStatistics[i].setWorker(null);
			}
			classes.ConfigurationDirective[] lConfigurationDirectives = worker.configurationDirective.toArray();
			for(int i = 0; i < lConfigurationDirectives.length; i++) {
				lConfigurationDirectives[i].setWorker(null);
			}
			try {
				session.delete(worker);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(classes.Worker worker) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().refresh(worker);
			return true;
		}
		catch (Exception e) {
			_logger.error("refresh(classes.Worker worker)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(classes.Worker worker) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().evict(worker);
			return true;
		}
		catch (Exception e) {
			_logger.error("evict(classes.Worker worker)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Worker loadWorkerByCriteria(WorkerCriteria workerCriteria) {
		Worker[] workers = listWorkerByCriteria(workerCriteria);
		if(workers == null || workers.length == 0) {
			return null;
		}
		return workers[0];
	}
	
	public static Worker[] listWorkerByCriteria(WorkerCriteria workerCriteria) {
		return workerCriteria.listWorker();
	}
}
