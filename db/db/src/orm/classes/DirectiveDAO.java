/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package orm.classes;

import org.orm.*;
import org.hibernate.Query;
import java.util.List;
import classes.*;

public class DirectiveDAO {
	private static final org.apache.log4j.Logger _logger = org.apache.log4j.Logger.getLogger(DirectiveDAO.class);
	public static Directive loadDirectiveByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadDirectiveByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("loadDirectiveByORMID(int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Directive getDirectiveByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getDirectiveByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("getDirectiveByORMID(int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Directive loadDirectiveByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadDirectiveByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadDirectiveByORMID(int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Directive getDirectiveByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getDirectiveByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("getDirectiveByORMID(int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Directive loadDirectiveByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Directive) session.load(classes.Directive.class, new Integer(id));
		}
		catch (Exception e) {
			_logger.error("loadDirectiveByORMID(PersistentSession session, int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Directive getDirectiveByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Directive) session.get(classes.Directive.class, new Integer(id));
		}
		catch (Exception e) {
			_logger.error("getDirectiveByORMID(PersistentSession session, int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Directive loadDirectiveByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Directive) session.load(classes.Directive.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("loadDirectiveByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Directive getDirectiveByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Directive) session.get(classes.Directive.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("getDirectiveByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Directive[] listDirectiveByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listDirectiveByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("listDirectiveByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Directive[] listDirectiveByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listDirectiveByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("listDirectiveByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Directive[] listDirectiveByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Directive as Directive");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			List list = query.list();
			return (Directive[]) list.toArray(new Directive[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listDirectiveByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Directive[] listDirectiveByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Directive as Directive");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			List list = query.list();
			return (Directive[]) list.toArray(new Directive[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listDirectiveByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Directive loadDirectiveByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadDirectiveByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("loadDirectiveByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Directive loadDirectiveByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadDirectiveByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadDirectiveByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Directive loadDirectiveByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Directive[] directives = listDirectiveByQuery(session, condition, orderBy);
		if (directives != null && directives.length > 0)
			return directives[0];
		else
			return null;
	}
	
	public static Directive loadDirectiveByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Directive[] directives = listDirectiveByQuery(session, condition, orderBy, lockMode);
		if (directives != null && directives.length > 0)
			return directives[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateDirectiveByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateDirectiveByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("iterateDirectiveByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateDirectiveByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateDirectiveByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("iterateDirectiveByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateDirectiveByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Directive as Directive");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateDirectiveByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateDirectiveByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Directive as Directive");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateDirectiveByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Directive createDirective() {
		return new classes.Directive();
	}
	
	public static boolean save(classes.Directive directive) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().saveObject(directive);
			return true;
		}
		catch (Exception e) {
			_logger.error("save(classes.Directive directive)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(classes.Directive directive) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().deleteObject(directive);
			return true;
		}
		catch (Exception e) {
			_logger.error("delete(classes.Directive directive)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.Directive directive)throws PersistentException {
		try {
			if(directive.getComponent() != null) {
				directive.getComponent().directive.remove(directive);
			}
			
			return delete(directive);
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.Directive directive, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(directive.getComponent() != null) {
				directive.getComponent().directive.remove(directive);
			}
			
			try {
				session.delete(directive);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(classes.Directive directive) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().refresh(directive);
			return true;
		}
		catch (Exception e) {
			_logger.error("refresh(classes.Directive directive)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(classes.Directive directive) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().evict(directive);
			return true;
		}
		catch (Exception e) {
			_logger.error("evict(classes.Directive directive)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Directive loadDirectiveByCriteria(DirectiveCriteria directiveCriteria) {
		Directive[] directives = listDirectiveByCriteria(directiveCriteria);
		if(directives == null || directives.length == 0) {
			return null;
		}
		return directives[0];
	}
	
	public static Directive[] listDirectiveByCriteria(DirectiveCriteria directiveCriteria) {
		return directiveCriteria.listDirective();
	}
}
