/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package orm.classes;

import org.orm.*;
import org.hibernate.Query;
import java.util.List;
import classes.*;

public class ApplicationDAO {
	private static final org.apache.log4j.Logger _logger = org.apache.log4j.Logger.getLogger(ApplicationDAO.class);
	public static Application loadApplicationByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadApplicationByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("loadApplicationByORMID(int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Application getApplicationByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getApplicationByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("getApplicationByORMID(int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Application loadApplicationByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadApplicationByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadApplicationByORMID(int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Application getApplicationByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getApplicationByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("getApplicationByORMID(int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Application loadApplicationByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Application) session.load(classes.Application.class, new Integer(id));
		}
		catch (Exception e) {
			_logger.error("loadApplicationByORMID(PersistentSession session, int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Application getApplicationByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Application) session.get(classes.Application.class, new Integer(id));
		}
		catch (Exception e) {
			_logger.error("getApplicationByORMID(PersistentSession session, int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Application loadApplicationByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Application) session.load(classes.Application.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("loadApplicationByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Application getApplicationByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Application) session.get(classes.Application.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("getApplicationByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Application[] listApplicationByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listApplicationByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("listApplicationByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Application[] listApplicationByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listApplicationByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("listApplicationByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Application[] listApplicationByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Application as Application");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			List list = query.list();
			return (Application[]) list.toArray(new Application[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listApplicationByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Application[] listApplicationByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Application as Application");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			List list = query.list();
			return (Application[]) list.toArray(new Application[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listApplicationByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Application loadApplicationByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadApplicationByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("loadApplicationByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Application loadApplicationByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadApplicationByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadApplicationByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Application loadApplicationByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Application[] applications = listApplicationByQuery(session, condition, orderBy);
		if (applications != null && applications.length > 0)
			return applications[0];
		else
			return null;
	}
	
	public static Application loadApplicationByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Application[] applications = listApplicationByQuery(session, condition, orderBy, lockMode);
		if (applications != null && applications.length > 0)
			return applications[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateApplicationByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateApplicationByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("iterateApplicationByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateApplicationByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateApplicationByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("iterateApplicationByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateApplicationByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Application as Application");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateApplicationByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateApplicationByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Application as Application");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateApplicationByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Application createApplication() {
		return new classes.Application();
	}
	
	public static boolean save(classes.Application application) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().saveObject(application);
			return true;
		}
		catch (Exception e) {
			_logger.error("save(classes.Application application)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(classes.Application application) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().deleteObject(application);
			return true;
		}
		catch (Exception e) {
			_logger.error("delete(classes.Application application)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.Application application)throws PersistentException {
		try {
			if(application.getService() != null) {
				application.getService().application.remove(application);
			}
			
			return delete(application);
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.Application application, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(application.getService() != null) {
				application.getService().application.remove(application);
			}
			
			try {
				session.delete(application);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(classes.Application application) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().refresh(application);
			return true;
		}
		catch (Exception e) {
			_logger.error("refresh(classes.Application application)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(classes.Application application) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().evict(application);
			return true;
		}
		catch (Exception e) {
			_logger.error("evict(classes.Application application)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Application loadApplicationByCriteria(ApplicationCriteria applicationCriteria) {
		Application[] applications = listApplicationByCriteria(applicationCriteria);
		if(applications == null || applications.length == 0) {
			return null;
		}
		return applications[0];
	}
	
	public static Application[] listApplicationByCriteria(ApplicationCriteria applicationCriteria) {
		return applicationCriteria.listApplication();
	}
}
