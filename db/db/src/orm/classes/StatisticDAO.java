/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: DuKe TeAm
 * License Type: Purchased
 */
package orm.classes;

import org.orm.*;
import org.hibernate.Query;
import java.util.List;
import classes.*;

public class StatisticDAO {
	private static final org.apache.log4j.Logger _logger = org.apache.log4j.Logger.getLogger(StatisticDAO.class);
	public static Statistic loadStatisticByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadStatisticByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("loadStatisticByORMID(int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Statistic getStatisticByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getStatisticByORMID(session, id);
		}
		catch (Exception e) {
			_logger.error("getStatisticByORMID(int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Statistic loadStatisticByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadStatisticByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadStatisticByORMID(int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Statistic getStatisticByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return getStatisticByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			_logger.error("getStatisticByORMID(int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Statistic loadStatisticByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Statistic) session.load(classes.Statistic.class, new Integer(id));
		}
		catch (Exception e) {
			_logger.error("loadStatisticByORMID(PersistentSession session, int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Statistic getStatisticByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Statistic) session.get(classes.Statistic.class, new Integer(id));
		}
		catch (Exception e) {
			_logger.error("getStatisticByORMID(PersistentSession session, int id)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Statistic loadStatisticByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Statistic) session.load(classes.Statistic.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("loadStatisticByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Statistic getStatisticByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Statistic) session.get(classes.Statistic.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			_logger.error("getStatisticByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Statistic[] listStatisticByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listStatisticByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("listStatisticByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Statistic[] listStatisticByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return listStatisticByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("listStatisticByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Statistic[] listStatisticByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Statistic as Statistic");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			List list = query.list();
			return (Statistic[]) list.toArray(new Statistic[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listStatisticByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Statistic[] listStatisticByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Statistic as Statistic");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			List list = query.list();
			return (Statistic[]) list.toArray(new Statistic[list.size()]);
		}
		catch (Exception e) {
			_logger.error("listStatisticByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Statistic loadStatisticByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadStatisticByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("loadStatisticByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Statistic loadStatisticByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return loadStatisticByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("loadStatisticByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Statistic loadStatisticByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Statistic[] statistics = listStatisticByQuery(session, condition, orderBy);
		if (statistics != null && statistics.length > 0)
			return statistics[0];
		else
			return null;
	}
	
	public static Statistic loadStatisticByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Statistic[] statistics = listStatisticByQuery(session, condition, orderBy, lockMode);
		if (statistics != null && statistics.length > 0)
			return statistics[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateStatisticByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateStatisticByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			_logger.error("iterateStatisticByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateStatisticByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.Hibernatev3PersistentManager.instance().getSession();
			return iterateStatisticByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			_logger.error("iterateStatisticByQuery(String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateStatisticByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Statistic as Statistic");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateStatisticByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateStatisticByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From classes.Statistic as Statistic");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("this", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			_logger.error("iterateStatisticByQuery(PersistentSession session, String condition, String orderBy)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Statistic createStatistic() {
		return new classes.Statistic();
	}
	
	public static boolean save(classes.Statistic statistic) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().saveObject(statistic);
			return true;
		}
		catch (Exception e) {
			_logger.error("save(classes.Statistic statistic)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(classes.Statistic statistic) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().deleteObject(statistic);
			return true;
		}
		catch (Exception e) {
			_logger.error("delete(classes.Statistic statistic)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.Statistic statistic)throws PersistentException {
		try {
			if(statistic.getWorker() != null) {
				statistic.getWorker().statistic.remove(statistic);
			}
			
			return delete(statistic);
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(classes.Statistic statistic, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(statistic.getWorker() != null) {
				statistic.getWorker().statistic.remove(statistic);
			}
			
			try {
				session.delete(statistic);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			_logger.error("deleteAndDissociate()", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(classes.Statistic statistic) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().refresh(statistic);
			return true;
		}
		catch (Exception e) {
			_logger.error("refresh(classes.Statistic statistic)", e);
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(classes.Statistic statistic) throws PersistentException {
		try {
			orm.Hibernatev3PersistentManager.instance().getSession().evict(statistic);
			return true;
		}
		catch (Exception e) {
			_logger.error("evict(classes.Statistic statistic)", e);
			throw new PersistentException(e);
		}
	}
	
	public static Statistic loadStatisticByCriteria(StatisticCriteria statisticCriteria) {
		Statistic[] statistics = listStatisticByCriteria(statisticCriteria);
		if(statistics == null || statistics.length == 0) {
			return null;
		}
		return statistics[0];
	}
	
	public static Statistic[] listStatisticByCriteria(StatisticCriteria statisticCriteria) {
		return statisticCriteria.listStatistic();
	}
}
