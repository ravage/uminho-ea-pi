﻿CREATE FUNCTION changeUpdateAt() RETURNS trigger AS $$
    BEGIN
	NEW.updated_at:= current_timestamp;
	RETURN NEW;
    END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER users_triggers BEFORE UPDATE ON users
    FOR EACH ROW EXECUTE PROCEDURE changeUpdateAt();

CREATE TRIGGER authorities_triggers BEFORE UPDATE ON authorities
	FOR EACH ROW EXECUTE PROCEDURE changeUpdateAt();

CREATE TRIGGER agents_triggers BEFORE UPDATE ON agents
	FOR EACH ROW EXECUTE PROCEDURE changeUpdateAt();

CREATE TRIGGER alert_services_triggers BEFORE UPDATE ON alert_services
	FOR EACH ROW EXECUTE PROCEDURE changeUpdateAt();

CREATE TRIGGER applications_triggers BEFORE UPDATE ON applications
	FOR EACH ROW EXECUTE PROCEDURE changeUpdateAt();

CREATE TRIGGER bundle_components_triggers BEFORE UPDATE ON bundle_components
	FOR EACH ROW EXECUTE PROCEDURE changeUpdateAt();

CREATE TRIGGER bundles_triggers BEFORE UPDATE ON bundles
	FOR EACH ROW EXECUTE PROCEDURE changeUpdateAt();

CREATE TRIGGER components_triggers BEFORE UPDATE ON components
	FOR EACH ROW EXECUTE PROCEDURE changeUpdateAt();

CREATE TRIGGER configuration_directives_triggers BEFORE UPDATE ON configuration_directives
	FOR EACH ROW EXECUTE PROCEDURE changeUpdateAt();

CREATE TRIGGER configuration_triggers BEFORE UPDATE ON configuration
	FOR EACH ROW EXECUTE PROCEDURE changeUpdateAt();

CREATE TRIGGER directives_triggers BEFORE UPDATE ON directives
	FOR EACH ROW EXECUTE PROCEDURE changeUpdateAt();

CREATE TRIGGER services_triggers BEFORE UPDATE ON services
	FOR EACH ROW EXECUTE PROCEDURE changeUpdateAt();

CREATE TRIGGER statistics_triggers BEFORE UPDATE ON statistics
	FOR EACH ROW EXECUTE PROCEDURE changeUpdateAt();

CREATE TRIGGER workes_triggers BEFORE UPDATE ON workers
	FOR EACH ROW EXECUTE PROCEDURE changeUpdateAt();

