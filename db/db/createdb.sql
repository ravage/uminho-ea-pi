ALTER TABLE applications DROP CONSTRAINT fk_application_service;		
ALTER TABLE services DROP CONSTRAINT fk_service_user;
ALTER TABLE workers DROP CONSTRAINT fk_worker_service;
ALTER TABLE workers DROP CONSTRAINT fk_worker_component;
ALTER TABLE statistics DROP CONSTRAINT fk_statistic_worker;
ALTER TABLE bundle_components DROP CONSTRAINT fk_bundle_component_bundle;
ALTER TABLE bundle_components DROP CONSTRAINT fk_bundle_component_component;
ALTER TABLE configurations DROP CONSTRAINT fk_configuration_component;
ALTER TABLE configuration_directives DROP CONSTRAINT fk_configuration_worker;
ALTER TABLE agents DROP CONSTRAINT fk_agent_component;
ALTER TABLE directives DROP CONSTRAINT fk_directive_component;
ALTER TABLE authorities DROP CONSTRAINT fk_user_authority;

DROP TABLE users;
DROP TABLE components;
DROP TABLE applications;
DROP TABLE services;
DROP TABLE workers;
DROP TABLE statistics;
DROP TABLE alert_services;
DROP TABLE bundles;
DROP TABLE bundle_components;
DROP TABLE configurations;
DROP TABLE configuration_directives;
DROP TABLE agents;		
DROP TABLE directives;
DROP TABLE authorities;

CREATE TABLE users (
	id BIGSERIAL NOT NULL,
	name varchar(128) NOT NULL, 
	username varchar(32) NOT NULL UNIQUE, 
	password varchar(128) NOT NULL, 
	email varchar(128) NOT NULL, 
	nif varchar(16), 
	activation_key varchar(128), 
	active bool DEFAULT 'false', 
	created_at timestamp DEFAULT LOCALTIMESTAMP, 
	updated_at timestamp DEFAULT LOCALTIMESTAMP, 
	PRIMARY KEY (id));

CREATE TABLE components (
	id BIGSERIAL NOT NULL, 
	name varchar(128) NOT NULL,
	description varchar(512),
	price numeric(7, 2), 
	selectable BOOLEAN,
	"key" varchar(16) NOT NULL,
	created_at timestamp DEFAULT LOCALTIMESTAMP,
	updated_at timestamp DEFAULT LOCALTIMESTAMP, 
	PRIMARY KEY (id));

CREATE TABLE applications (
	id BIGSERIAL NOT NULL, 
	name varchar(128),
	file_location varchar(256) NOT NULL,
	url varchar(256) NOT NULL,
	service_id BIGSERIAL NOT NULL,
	created_at timestamp DEFAULT LOCALTIMESTAMP,
	updated_at timestamp DEFAULT LOCALTIMESTAMP, 
	PRIMARY KEY (id));

CREATE TABLE services (
	id BIGSERIAL NOT NULL, 
	init_date date NOT NULL, 
	duration int2 NOT NULL,
	name varchar(128) NOT NULL,
	description varchar(512),
	price numeric(7, 2) NOT NULL,
	user_id int8 NOT NULL,
	created_at timestamp DEFAULT LOCALTIMESTAMP,
	updated_at timestamp DEFAULT LOCALTIMESTAMP, 
	PRIMARY KEY (id));

CREATE TABLE workers (
	id BIGSERIAL NOT NULL, 
	service_id BIGSERIAL NOT NULL,
	component_id BIGSERIAL NOT NULL,
	quantity int8 NOT NULL, 
	percentage_limit int8 NOT NULL, 
	created_at timestamp DEFAULT LOCALTIMESTAMP,
	updated_at timestamp DEFAULT LOCALTIMESTAMP,
	deploy_to varchar(16) NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT serv_comp_uniq UNIQUE (service_id, component_id));

CREATE TABLE statistics (
	id BIGSERIAL NOT NULL,
	name varchar(128) NOT NULL,
	statistics_date date NOT NULL, 
	usage BIGINT NOT NULL, 
	observations varchar(512),
	worker_id BIGSERIAL NOT NULL,
	created_at timestamp DEFAULT LOCALTIMESTAMP,
	updated_at timestamp DEFAULT LOCALTIMESTAMP, 
	PRIMARY KEY (id));

CREATE TABLE alert_services (
	id BIGSERIAL NOT NULL, 
	name varchar(128) NOT NULL,
	description varchar(512) NOT NULL, 
	created_at timestamp DEFAULT LOCALTIMESTAMP,
	updated_at timestamp DEFAULT LOCALTIMESTAMP,
	PRIMARY KEY (id));

CREATE TABLE bundles (
	id BIGSERIAL NOT NULL,
	name varchar(40) NOT NULL, 
	price numeric(7, 2) NOT NULL, 
	duration int2 NOT NULL, 
	created_at timestamp DEFAULT LOCALTIMESTAMP, 
	updated_at timestamp DEFAULT LOCALTIMESTAMP, 
	PRIMARY KEY (id));

CREATE TABLE bundle_components (
	id BIGSERIAL NOT NULL,
	bundle_id BIGSERIAL NOT NULL,
	component_id BIGSERIAL NOT NULL,
	quantity INT, 
	created_at timestamp DEFAULT LOCALTIMESTAMP, 
	updated_at timestamp DEFAULT LOCALTIMESTAMP,
	PRIMARY KEY (id),
	CONSTRAINT bund_comp_uniq UNIQUE (bundle_id, component_id));

CREATE TABLE configurations (
	id BIGSERIAL NOT NULL,
	name varchar(128) NOT NULL,
	template text,
	component_id BIGSERIAL NOT NULL,
	created_at timestamp DEFAULT LOCALTIMESTAMP,
	updated_at timestamp DEFAULT LOCALTIMESTAMP, 
	PRIMARY KEY (id));

CREATE TABLE configuration_directives (
	id BIGSERIAL NOT NULL, 
	name varchar(128) NOT NULL,
	value varchar(128) NOT NULL,
	worker_id BIGSERIAL,
	created_at timestamp DEFAULT LOCALTIMESTAMP, 
	updated_at timestamp DEFAULT LOCALTIMESTAMP,
	PRIMARY KEY (id));

CREATE TABLE agents (
	id BIGSERIAL NOT NULL, 
	address varchar(16),
	component_id BIGSERIAL NOT NULL,
	status varchar(16) CHECK(status IN ('UP', 'DOWN', 'MAINTENANCE')), 
	"key" varchar(64) NOT NULL,
	last_check timestamp NOT NULL,
	created_at timestamp DEFAULT LOCALTIMESTAMP, 
	updated_at timestamp DEFAULT LOCALTIMESTAMP,
	PRIMARY KEY (id));

CREATE TABLE directives (
	id BIGSERIAL NOT NULL, 
	name varchar(128) NOT NULL,
	value varchar(128) NOT NULL,
	component_id BIGSERIAL NOT NULL, 
	created_at timestamp DEFAULT LOCALTIMESTAMP,
	updated_at timestamp DEFAULT LOCALTIMESTAMP,
	PRIMARY KEY (id));

CREATE TABLE authorities (
	user_id BIGSERIAL NOT NULL,
	authority varchar(20) NOT NULL,
	created_at timestamp DEFAULT LOCALTIMESTAMP, 
	updated_at timestamp DEFAULT LOCALTIMESTAMP,
	PRIMARY KEY (user_id, authority), 
	CONSTRAINT user_auth_uniq UNIQUE (user_id, authority));

ALTER TABLE applications ADD CONSTRAINT fk_application_service FOREIGN KEY (service_id) REFERENCES services (id) ON UPDATE Cascade ON DELETE Cascade;
ALTER TABLE services ADD CONSTRAINT fk_service_user FOREIGN KEY (user_id) REFERENCES users (id) ON UPDATE Cascade ON DELETE Cascade;
ALTER TABLE workers ADD CONSTRAINT fk_worker_service FOREIGN KEY (service_id) REFERENCES services (id) ON UPDATE Cascade ON DELETE Cascade;
ALTER TABLE workers ADD CONSTRAINT fk_worker_component FOREIGN KEY (component_id) REFERENCES components (id) ON UPDATE Cascade ON DELETE Cascade;
ALTER TABLE statistics ADD CONSTRAINT fk_statistic_worker FOREIGN KEY (worker_id) REFERENCES workers (id) ON UPDATE Cascade ON DELETE Cascade;
ALTER TABLE bundle_components ADD CONSTRAINT fk_bundle_component_bundle FOREIGN KEY (bundle_id) REFERENCES bundles (id) ON UPDATE Cascade ON DELETE Cascade;
ALTER TABLE bundle_components ADD CONSTRAINT fk_bundle_component_component FOREIGN KEY (component_id) REFERENCES components (id) ON UPDATE Cascade ON DELETE Cascade;
ALTER TABLE configurations ADD CONSTRAINT fk_configuration_component FOREIGN KEY (component_id) REFERENCES components (id) ON UPDATE Cascade ON DELETE Cascade;
ALTER TABLE configuration_directives ADD CONSTRAINT fk_configuration_worker FOREIGN KEY (worker_id) REFERENCES workers (id) ON UPDATE Cascade ON DELETE Cascade;
ALTER TABLE agents ADD CONSTRAINT fk_agent_component FOREIGN KEY (component_id) REFERENCES components (id) ON UPDATE Cascade ON DELETE Cascade;
ALTER TABLE directives ADD CONSTRAINT fk_directive_component FOREIGN KEY (component_id) REFERENCES components (id) ON UPDATE Cascade ON DELETE Cascade;
ALTER TABLE authorities ADD CONSTRAINT fk_user_authority FOREIGN KEY (user_id) REFERENCES users (id);
CREATE INDEX serv_comp_ind ON workers (service_id, component_id);
CREATE INDEX bund_comp_ind ON bundle_components (bundle_id, component_id);
CREATE INDEX user_auth_ind ON authorities (user_id, authority);