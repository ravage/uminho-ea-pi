CREATE TABLE users (
	id            	BIGSERIAL PRIMARY KEY,
	name          	VARCHAR(128) NOT NULL,
	username      	VARCHAR(32) NOT NULL,
	password      	VARCHAR(128) NOT NULL,
	email         	VARCHAR(128) NOT NULL,
	nif           	VARCHAR(16) NOT NULL,
	activation_key	VARCHAR(128),
	active			BIT,
	created_at		TIMESTAMP DEFAULT LOCALTIMESTAMP
);

CREATE TABLE components (
	id          BIGSERIAL PRIMARY KEY,
	name        VARCHAR(128) NOT NULL,
	description VARCHAR(512),
	price       NUMERIC(7, 2),
	selectable	BOOLEAN,
	key					VARCHAR(16) NOT NULL,
	created_at	TIMESTAMP DEFAULT LOCALTIMESTAMP
);

CREATE TABLE applications (
	id            BIGSERIAL PRIMARY KEY,
	name          VARCHAR(128),
	file_location VARCHAR(256) NOT NULL,
	url           VARCHAR(256) NOT NULL,
	service_id	  BIGINT NOT NULL,
	created_at    TIMESTAMP DEFAULT LOCALTIMESTAMP
);

CREATE TABLE services (
	id          BIGSERIAL PRIMARY KEY,
	init_date   DATE NOT NULL,
	duration    SMALLINT NOT NULL,
	name        VARCHAR(128) NOT NULL,
	description VARCHAR(512),
	price       NUMERIC(7, 2) NOT NULL,
	user_id     BIGINT NOT NULL,
	created_at	TIMESTAMP DEFAULT LOCALTIMESTAMP
);

CREATE TABLE workers (
	id								BIGSERIAL	PRIMARY KEY,
	service_id      	BIGINT NOT NULL,
	component_id     	BIGINT NOT NULL,
	quantity         	SMALLINT NOT NULL,
	percentage_limit 	SMALLINT NOT NULL,
	deploy_to					VARCHAR(16) NOT NULL,
	created_at				TIMESTAMP DEFAULT LOCALTIMESTAMP,
	UNIQUE (service_id, component_id)
);

CREATE TABLE statistics (
	id              BIGSERIAL PRIMARY KEY,
	name            VARCHAR(128) NOT NULL,
	usage					  BIGINT NOT NULL,
	observations    VARCHAR(512),
	worker_id      	BIGINT NOT NULL,
	created_at			TIMESTAMP DEFAULT LOCALTIMESTAMP
);

CREATE TABLE alert_services (
	id          BIGSERIAL PRIMARY KEY,
	name        VARCHAR(128) NOT NULL,
	description VARCHAR(512) NOT NULL,
	created_at	TIMESTAMP DEFAULT LOCALTIMESTAMP
);

CREATE TABLE bundles (
	id       		BIGSERIAL PRIMARY KEY,
	name     		VARCHAR(40) NOT NULL,
	price   		NUMERIC(7, 2) NOT NULL,
	duration 		SMALLINT NOT NULL,
	created_at	TIMESTAMP DEFAULT LOCALTIMESTAMP
);

CREATE TABLE bundle_components (
	id				BIGSERIAL PRIMARY KEY,
	bundle_id 		BIGINT NOT NULL,
	component_id  	BIGINT NOT NULL,
	quantity		INT,
	created_at		TIMESTAMP DEFAULT LOCALTIMESTAMP,
	UNIQUE (bundle_id, component_id)
);

CREATE TABLE configurations (
	id           	BIGSERIAL PRIMARY KEY,
	name         	VARCHAR(128) NOT NULL,
	template		TEXT,
	component_id 	BIGINT NOT NULL,
	created_at		TIMESTAMP DEFAULT LOCALTIMESTAMP
);

CREATE TABLE configuration_directives (
	id				BIGSERIAL PRIMARY KEY,
	name			VARCHAR(128) NOT NULL,
	value     		VARCHAR(128) NOT NULL,
	worker_id		BIGINT,
	created_at		TIMESTAMP DEFAULT LOCALTIMESTAMP
);

CREATE TABLE agents (
  id            BIGSERIAL PRIMARY KEY,
  address       VARCHAR(16),
  component_id  BIGINT NOT NULL,
  status        VARCHAR(16) CHECK (status IN ('UP', 'DOWN', 'MAINTENANCE')),
  key           VARCHAR(64) NOT NULL,
  last_check    TIMESTAMP,
  created_at    TIMESTAMP DEFAULT LOCALTIMESTAMP
);

CREATE TABLE directives (
	id				BIGSERIAL PRIMARY KEY,
	name			VARCHAR(128) NOT NULL,
	value 			VARCHAR(128) NOT NULL,
	component_id	BIGINT NOT NULL,
	created_at		TIMESTAMP DEFAULT LOCALTIMESTAMP
);

CREATE TABLE authorities (
	id         		BIGSERIAL PRIMARY KEY,
	user_id       	BIGINT NOT NULL,
	authority		VARCHAR(20),
	created_at		TIMESTAMP DEFAULT LOCALTIMESTAMP,
	UNIQUE (user_id, authority)
);

ALTER TABLE authorities 
	ADD CONSTRAINT fk_authority_user FOREIGN KEY (user_id) REFERENCES users (id);

ALTER TABLE directives
	ADD CONSTRAINT fk_directive_component FOREIGN KEY (component_id) REFERENCES components (id);

ALTER TABLE applications 
	ADD CONSTRAINT fk_application_service FOREIGN KEY (service_id) REFERENCES services (id);

ALTER TABLE workers
	ADD CONSTRAINT fk_worker_service FOREIGN KEY (service_id) REFERENCES services (id),
	ADD CONSTRAINT fk_worker_component FOREIGN KEY (component_id) REFERENCES components (id);

ALTER TABLE services 
	ADD CONSTRAINT fk_service_user FOREIGN KEY (user_id) REFERENCES users (id);

ALTER TABLE statistics
	ADD CONSTRAINT fk_statistic_worker FOREIGN KEY (worker_id) REFERENCES workers (id);

ALTER TABLE bundle_components
	ADD CONSTRAINT fk_bundle_component_component FOREIGN KEY (component_id) REFERENCES components (id),
	ADD CONSTRAINT fk_bundle_component_bundle FOREIGN KEY (bundle_id) REFERENCES bundles (id);

ALTER TABLE configurations 
	ADD CONSTRAINT fk_configuration_component FOREIGN KEY (component_id) REFERENCES components (id);

ALTER TABLE agents 
	ADD CONSTRAINT fk_agent_component FOREIGN KEY (component_id) REFERENCES components (id);

ALTER TABLE configuration_directives
	ADD CONSTRAINT fk_configuration_worker FOREIGN KEY (worker_id) REFERENCES workers (id);
