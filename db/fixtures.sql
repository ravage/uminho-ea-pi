-- users

-- user1/password
INSERT INTO users (name,email,username,password,active,nif) 
	values ('User1','teste@teste.com','user1','2870c94d126472de54b0a8da0137495b5fc4212a',true,'000000');
-- user2/password
INSERT INTO users (name,email,username,password,active,nif)  
	values ('User2','teste@teste.com','user2','0a5c8ac28b2848571eaf1445dba28009aaf496ae',true,'000000');
-- user3/password
INSERT INTO users (name,email,username,password,active,nif) 
	values ('User3','teste@teste.com','user3','0c06b55c036e2939ea9abd3cfb44d9904a27315e',true,'000000');
-- user4/password
INSERT INTO users (name,email,username,password,active,nif)  
	values ('User4','teste@teste.com','user4','3305d8274b207f1e6be2312f42b387a07cf63615',true,'000000');
-- admin/admin
INSERT INTO users (name,email,username,password,active,nif)  
	values ('Admin','teste@teste.com','admin','a40546cc4fd6a12572828bb803380888ad1bfdab',true,'000000');

--authorities
INSERT INTO authorities (user_id,authority) 
	values (1,'ROLE_USER');
INSERT INTO authorities (user_id,authority) 
	values (2,'ROLE_USER');
INSERT INTO authorities (user_id,authority) 
	values (3,'ROLE_USER');
INSERT INTO authorities (user_id,authority) 
	values (4,'ROLE_USER');
INSERT INTO authorities (user_id,authority) 
	values (5,'ROLE_USER');
INSERT INTO authorities (user_id,authority) 
	values (5,'ROLE_ADMIN');

-- components
INSERT INTO components (id, name, description, price, selectable, key) 
	VALUES (1, 'Application', 'Application Server', 0, true, 'Application');
INSERT INTO components (id, name, description, price, selectable, key) 
	VALUES (2, 'Database', 'Database Server', 0, true, 'Database');
INSERT INTO components (id, name, description, price, selectable, key) 
	VALUES (3, 'Storage', 'Storage Server', 0, true, 'Storage');
INSERT INTO components (id, name, description, price, selectable, key) 
	VALUES (4, 'DNS', 'DNS Server (Bind)', 0, false, 'DNS');
INSERT INTO components (id, name, description, price, selectable, key) 
	VALUES (5, 'Front Controller', 'Front Controller (NGINX)', 0, false, 'FrontController');
INSERT INTO components (id, name, description, price, selectable, key) 
	VALUES (6, 'Deployer', 'Application Deployer Unit', 0, false, 'Deployer');
	
-- agents
INSERT INTO agents (component_id, last_check, key) 
	VALUES (5, LOCALTIMESTAMP, 'ebac0b78-a28c-11e0-aea2-0017f2ceafb4');
	
-- users
INSERT INTO users (name, username, password, email, nif) 
	VALUES ('Rui', 'Ravage', '0000', 'ravage@fragmentized.net', '00000000');
	
-- services
INSERT INTO services (init_date, duration, name, price, user_id)
	VALUES ('2011-05-20', 1, 'Service Blargh!', 0.0, 1);

-- workers
INSERT INTO workers (service_id, component_id, quantity, percentage_limit, deploy_to)
	VALUES (1, 1, 3, 0, 'localhost:9999');

INSERT INTO workers (service_id, component_id, quantity, percentage_limit, deploy_to)
	VALUES (1, 2, 2, 0, 'localhost:9999');

INSERT INTO workers (service_id, component_id, quantity, percentage_limit, deploy_to)
	VALUES (1, 3, 10000, 0, 'localhost:9999');

INSERT INTO workers (service_id, component_id, quantity, percentage_limit, deploy_to)
	VALUES (1, 4, 0, 0, 'localhost:9999');


INSERT INTO workers (service_id, component_id, quantity, percentage_limit, deploy_to)
	VALUES (1, 5, 3, 0, 'localhost:9999');
	
-- directives Application Server
INSERT INTO directives (component_id, name, value)
	VALUES (1, 'listen_backlog', '-1');
	
INSERT INTO directives (component_id, name, value)
	VALUES (1, 'group', 'workers');
	
INSERT INTO directives (component_id, name, value)
	VALUES (1, 'pm', 'static');
	
INSERT INTO directives (component_id, name, value)
	VALUES (1, 'pm_max_requests', '500');
	
INSERT INTO directives (component_id, name, value)
	VALUES (1, 'pm_status_path', '/status');

INSERT INTO directives (component_id, name, value)
	VALUES (1, 'ping_path', '/bin/ping');
	
-- directives Database Server
INSERT INTO directives (component_id, name, value)
	VALUES (2, 'listen_addresses', '*');

INSERT INTO directives (component_id, name, value)
	VALUES (2, 'port', '5432');

INSERT INTO directives (component_id, name, value)
	VALUES (2, 'max_connection', '100');

INSERT INTO directives (component_id, name, value)
	VALUES (2, 'shared_buffers', '24MB');
	
-- configuration templates
-- openssl base64  -in front_controller.str -out front_controller.b64 

insert into configurations (name, template, component_id) values ('fc',
'
server {
        server_name <domain> *.<domain>;

        access_log /mnt/storage/paas/services/<username>/logs/access.log;
        error_log /mnt/storage/paas/services/<username>/logs/error.log;

        root /mnt/storage/paas/services/<username>/apps;

        location / {
                try_files  $uri/index.html $uri.html $uri @backend;
                expires 30d;
                access_log off;
        }

        location ~* \.php$ {
                fastcgi_pass   <username>;
                fastcgi_index  index.php;
                fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
                include        fastcgi_params;
        }

        location ~ /\.ht {
                deny all;
        }

        location @backend {
                fastcgi_pass   <username>;
                fastcgi_index  index.php;
                fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
                include        fastcgi_params;
        }
}

upstream <username> {
        <port>
}
', 5);

insert into configurations (name, template, component_id) values ('ap', 
'
[<username>]
	listen = <listen>
	listen.backlog = <listen_backlog>
	user = <user>
	group = <group>
	pm = <pm>
	pm.max_children = <pm_max_children>
	pm.max_requests = <pm_max_requests>
	pm.status_path = <pm_status_path>
	ping.path = <ping_path>
	slowlog = <slowlog>
', 1);


--	listen = <listen>
--	listen.backlog = <listen_backlog>
--	listen.allowed_clients = <listen_allowed_clients>
--	listen.owner = nobody
--	listen.group = nobody
--	user = <user>
--	group = <group>
--	pm = <pm>
--	pm.max_children = <pm_max_children>
--	pm.max_requests = <pm_max_requests>
--	pm.status_path = <pm_status_path>
--	ping.path = <ping_path>
--	slowlog = <slowlog>

insert into configurations (name, template, component_id) values ('db', '', 2);
	
insert into configurations (name, template, component_id) values ('dns','
zone "<domain>" {
	type master;
	file "/var/named/data/<domain>";
	allow-update { key "rndckey"; };
};
<-->
$TTL 86400
$ORIGIN <domain>.

@       IN      SOA     ns.paas.lsd.di.uminho.pt. root.<domain>. (
                20110522        ; Serial
                3H              ; Refresh after 3 hours
                1H              ; Retry after 1 hour
                1W              ; Expire after 1 week
                1D )            ; Minimum TTL of 1 day

@               NS      ns.paas.lsd.di.uminho.pt.

@               A				10.1.0.5
www							A				10.1.0.5
', 4);

-- Front Controller - No directives necessary
		
-- directives Front Controller
--INSERT INTO directives (component_id, name, value)
--	VALUES (1, 'server_name', '');

--bundles
INSERT INTO bundles (name,price,duration) 
	values ('Standard',100,30);
INSERT INTO bundles (name,price,duration) 
	values ('Advanced',200,30);
INSERT INTO bundles (name,price,duration) 
	values ('Ultra',500,30);

--valores das bundles
INSERT INTO bundle_components (bundle_id,component_id,quantity) 
	values (1,1,1);
INSERT INTO bundle_components (bundle_id,component_id,quantity) 
	values (1,2,200);
INSERT INTO bundle_components (bundle_id,component_id,quantity) 
	values (1,3,1);
INSERT INTO bundle_components (bundle_id,component_id,quantity) 
	values (1,4,1);
INSERT INTO bundle_components (bundle_id,component_id,quantity) 
	values (1,5,1);
INSERT INTO bundle_components (bundle_id,component_id,quantity) 
	values (2,1,2);
INSERT INTO bundle_components (bundle_id,component_id,quantity) 
	values (2,2,500);
INSERT INTO bundle_components (bundle_id,component_id,quantity) 
	values (2,3,2);
INSERT INTO bundle_components (bundle_id,component_id,quantity) 
	values (2,4,2);
INSERT INTO bundle_components (bundle_id,component_id,quantity) 
	values (2,5,2);
INSERT INTO bundle_components (bundle_id,component_id,quantity) 
	values (3,1,5);
INSERT INTO bundle_components (bundle_id,component_id,quantity) 
	values (3,2,1000);
INSERT INTO bundle_components (bundle_id,component_id,quantity) 
	values (3,3,4);
INSERT INTO bundle_components (bundle_id,component_id,quantity) 
	values (3,4,4);
INSERT INTO bundle_components (bundle_id,component_id,quantity) 
	values (3,5,5);


